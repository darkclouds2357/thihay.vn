﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ST.Library.Common.EncryptedPassword
{
    public static class HashEncrypted
    {
        private static readonly UTF8Encoding Encoding = new UTF8Encoding();

        public static string EncryptedHash(string password, string salt)
        {
            byte[] bSalt = Encoding.GetBytes(salt);
            return HashSHA(password, bSalt);
        }

        public static bool IsVerifyStringMatch(string original, string hashString, string salt)
        {
            byte[] byteSalts = Encoding.GetBytes(salt);
            string hashedOriginal = HashSHA(original, byteSalts);
            return string.Equals(hashedOriginal, hashString);
        }

        private static string HashSHA(string message, byte[] salt)
        {
            byte[] messageBytes = Encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(salt))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        public static string GenerateSalt(int size)
        {
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                byte[] buff = new byte[size];
                rng.GetBytes(buff);
                return Convert.ToBase64String(buff);
            }
        }

        public static byte[] GenerateSaltBytes(int size)
        {
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                byte[] buff = new byte[size];
                rng.GetBytes(buff);
                return buff;
            }
        }

        private static readonly char[] Punctuations = "!@#$%^&*()_-+=[{]};:>|./?".ToCharArray();

        public static string GenerateStrongPassword(int length, int numberOfNonAlphanumericCharacters)
        {
            if (length < 1 || length > 128)
            {
                throw new ArgumentException(nameof(length));
            }

            if (numberOfNonAlphanumericCharacters > length || numberOfNonAlphanumericCharacters < 0)
            {
                throw new ArgumentException(nameof(numberOfNonAlphanumericCharacters));
            }

            using (var rng = RandomNumberGenerator.Create())
            {
                var byteBuffer = new byte[length];

                rng.GetBytes(byteBuffer);

                var count = 0;
                var characterBuffer = new char[length];

                for (var iter = 0; iter < length; iter++)
                {
                    var i = byteBuffer[iter] % 87;

                    if (i < 10)
                    {
                        characterBuffer[iter] = (char)('0' + i);
                    }
                    else if (i < 36)
                    {
                        characterBuffer[iter] = (char)('A' + i - 10);
                    }
                    else if (i < 62)
                    {
                        characterBuffer[iter] = (char)('a' + i - 36);
                    }
                    else
                    {
                        characterBuffer[iter] = Punctuations[i - 62];
                        count++;
                    }
                }

                if (count >= numberOfNonAlphanumericCharacters)
                {
                    return new string(characterBuffer);
                }

                int j;
                var rand = new Random();

                for (j = 0; j < numberOfNonAlphanumericCharacters - count; j++)
                {
                    int k;
                    do
                    {
                        k = rand.Next(0, length);
                    }
                    while (!char.IsLetterOrDigit(characterBuffer[k]));

                    characterBuffer[k] = Punctuations[rand.Next(0, Punctuations.Length)];
                }

                return new string(characterBuffer);
            }
        }
    }

}