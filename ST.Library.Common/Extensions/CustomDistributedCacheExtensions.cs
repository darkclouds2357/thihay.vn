﻿using BinaryFormatter;
using Newtonsoft.Json;
using ST.Library.Common.Caching;
using System;
using System.Threading.Tasks;

namespace Microsoft.Extensions.Caching.Distributed
{
    public static class CustomDistributedCacheExtensions
    {
        public static T GetCache<T>(this IDistributedCache cacheManager, string key, int resetCacheTime, CacheDataType dataType)
        {
            key = CacheOption.CacheKeyRule(key, dataType);
            switch (dataType)
            {
                case CacheDataType.ByteArray:
                //var data = cacheManager.Get(key);
                //if (data != null && data.Length > 0)
                //{
                //    if (resetCacheTime > 0)
                //        cacheManager.Set(key, data, new DistributedCacheEntryOptions()
                //        {
                //            AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(resetCacheTime)
                //        });
                //    return DeserializeBinaryData<T>(data);
                //}
                //break;

                case CacheDataType.Json:
                    var jsonData = cacheManager.GetString(key);
                    if (!string.IsNullOrEmpty(jsonData))
                    {
                        if (resetCacheTime > 0)
                            cacheManager.SetString(key, jsonData, new DistributedCacheEntryOptions()
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(resetCacheTime)
                            });
                        return DeserializeJsonData<T>(jsonData);
                    }
                    break;
            }

            return default(T);
        }

        public static async Task<T> GetCacheAsync<T>(this IDistributedCache cacheManager, string key, int resetCacheTime, CacheDataType dataType)
        {
            key = CacheOption.CacheKeyRule(key, dataType);
            switch (dataType)
            {
                case CacheDataType.ByteArray:
                    var data = await cacheManager.GetAsync(key);
                    if (data != null && data.Length > 0)
                    {
                        if (resetCacheTime > 0)
                        {
                            await cacheManager.SetAsync(key, data, new DistributedCacheEntryOptions()
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(resetCacheTime)
                            });
                        }
                        return DeserializeBinaryData<T>(data);
                    }
                    break;

                case CacheDataType.Json:
                    var jsonData = await cacheManager.GetStringAsync(key);
                    if (!string.IsNullOrEmpty(jsonData))
                    {
                        if (resetCacheTime > 0)
                        {
                            await cacheManager.SetStringAsync(key, jsonData.ToString(), new DistributedCacheEntryOptions()
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(resetCacheTime)
                            });
                        }
                        return DeserializeJsonData<T>(jsonData);
                    }
                    break;
            }

            return default(T);
        }

        public static void SetCache(this IDistributedCache cacheManager, string key, object data, int cacheTime, CacheDataType dataType)
        {
            key = CacheOption.CacheKeyRule(key, dataType);
            switch (dataType)
            {
                case CacheDataType.ByteArray:
                    cacheManager.Set(key, SerializeBinaryData(data), new DistributedCacheEntryOptions()
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cacheTime)
                    });

                    break;

                case CacheDataType.Json:
                    cacheManager.SetString(key, SerializeJsonData(data), new DistributedCacheEntryOptions()
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cacheTime)
                    });
                    break;
            }
        }

        public static async void SetCacheAsync(this IDistributedCache cacheManager, string key, object data, int cacheTime, CacheDataType dataType)
        {
            key = CacheOption.CacheKeyRule(key, dataType);
            switch (dataType)
            {
                case CacheDataType.ByteArray:
                    await cacheManager.SetAsync(key, SerializeBinaryData(data), new DistributedCacheEntryOptions()
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cacheTime)
                    });
                    break;

                case CacheDataType.Json:
                    await cacheManager.SetStringAsync(key, SerializeJsonData(data), new DistributedCacheEntryOptions()
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cacheTime)
                    });
                    break;
            }
        }

        private static T DeserializeBinaryData<T>(byte[] data)
        {
            var bc = new BinaryConverter();
            return bc.Deserialize<T>(data);
        }

        private static byte[] SerializeBinaryData(object data)
        {
            BinaryConverter bc = new BinaryConverter();
            return bc.Serialize(data);
        }

        private static string SerializeJsonData(object data)
        {
            return JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        private static T DeserializeJsonData<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}