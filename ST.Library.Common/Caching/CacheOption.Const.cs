﻿namespace ST.Library.Common.Caching
{
    public partial class CacheOption
    {
        public const int INVALID_CACHE_TIME = -1;
        public const int DEFAULT_CACHE_TIME = 2; //If no setting of cache in config then get 2 mins
        public const int DEFAULT_CACHE_RESET_TIME = 0;

        public const string CACHE_KEY_CONFIG = "CacheConfig";
        public const string CACHE_TIME_CONFIG = "CacheTime";
        public const string CACHE_DATA_TYPE = "CacheDataType";
    }

    public enum CacheDataType
    {
        ByteArray = -1, // Can convert to byte array
        Json,
    }
}