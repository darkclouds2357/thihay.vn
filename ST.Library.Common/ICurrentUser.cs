﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ST.Library.Common
{
    public interface ICurrentUser
    {
        void Init(Action<ICurrentUser> acquire);

        //List<long> RoleIds { get; set; }
        //List<string> RoleCodes { get; set; }
        //List<string> Permissions { get; set; }
        Guid Id { get; set; }

        string UserName { get; set; }
        string FullName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }

        bool IsDeleted { get; set; }

        void KeepHistory(object entity, bool saveInBaseRepository = false);

        void KeepHistory(object entity, string tableName, bool saveInBaseRepository = false);

        void CommitHistory(DbContext dbContext);

        void CommitHistory();

        bool IsSystemUser { get; }
        int FrontEndGMTMinutes { get; set; }

        bool IsEndpointAuthorized(string endpoint, string method);
    }
}