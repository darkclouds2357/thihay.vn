/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'eqneditor';
    config.skin = 'moono-lisa';
    config.toolbar = [
        { name: 'basicstyles', items: ['Font', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike'] },
        { name: 'eqn', items: ['EqnEditor'] },
        { name: 'insert', items: ['Table'] },
    ];
    config.filebrowserImageBrowseUrl = '/uploader/brower';
    //config.filebrowserImageUploadUrl = '/uploader/upload';
};