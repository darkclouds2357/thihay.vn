IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'Gmail' AND Object_ID = Object_ID(N'dbo.tbl_User')
)
BEGIN
ALTER TABLE 
	dbo.tbl_User
ADD 
	Gmail VARCHAR(100)  NULL;
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'GoogleId' AND Object_ID = Object_ID(N'dbo.tbl_User')
)
BEGIN
ALTER TABLE 
	dbo.tbl_User
ADD 
	GoogleId varchar(50)  NULL;
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'FacebookId' AND Object_ID = Object_ID(N'dbo.tbl_User')
)
BEGIN
ALTER TABLE 
	dbo.tbl_User
ADD 
	FacebookId varchar(50)  NULL;
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'Salt' AND Object_ID = Object_ID(N'dbo.tbl_User')
)
BEGIN
ALTER TABLE 
	dbo.tbl_User
ADD 
	Salt nvarchar(100) NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'IsDeleted' AND Object_ID = Object_ID(N'dbo.tbl_User')
)
BEGIN
ALTER TABLE 
	dbo.tbl_User
ADD 
	IsDeleted bit NOT NULL CONSTRAINT DF_tbl_User_IsDeleted DEFAULT(0);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_IsDeleted' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE INDEX IX_tbl_User_IsDeleted
ON dbo.tbl_User (IsDeleted);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_GoogleId' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE INDEX IX_tbl_User_GoogleId
ON dbo.tbl_User (GoogleId);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_Gmail' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE INDEX IX_tbl_User_Gmail
ON dbo.tbl_User (Gmail);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_FacebookId' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE INDEX IX_tbl_User_FacebookId
ON dbo.tbl_User (FacebookId);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_username' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE UNIQUE INDEX IX_tbl_User_username 
ON dbo.tbl_User (s_username);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_username_GoogleId' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE UNIQUE INDEX IX_tbl_User_username_GoogleId
ON dbo.tbl_User (s_username, GoogleId);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_username_FacebookId' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE UNIQUE INDEX IX_tbl_User_username_FacebookId
ON dbo.tbl_User (s_username, FacebookId);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_tbl_User_username_GoogleId_FacebookId' AND object_id = OBJECT_ID('dbo.tbl_User')
)
BEGIN
CREATE UNIQUE INDEX IX_tbl_User_username_GoogleId_FacebookId
ON dbo.tbl_User (s_username,GoogleId, FacebookId);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_tbl_User_School'
	AND parent_object_id = OBJECT_ID(N'dbo.tbl_User')
)
BEGIN
	ALTER TABLE dbo.tbl_User
	ADD CONSTRAINT FK_tbl_User_School
	FOREIGN KEY (fk_school_id) REFERENCES School(pk_ID)
	ON DELETE SET NULL;
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_tbl_User_Village'
	AND parent_object_id = OBJECT_ID(N'dbo.tbl_User')
)
BEGIN
	ALTER TABLE dbo.tbl_User
	ADD CONSTRAINT FK_tbl_User_Village
	FOREIGN KEY (fk_village_id) REFERENCES Village(pk_ID);
END

IF EXISTS (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'tbl_User' AND 
     COLUMN_NAME = 'fk_province_id' AND
	 DATA_TYPE = 'tinyint')
BEGIN
ALTER TABLE tbl_User
ALTER COLUMN fk_province_id INT NULL
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_tbl_User_Province'
	AND parent_object_id = OBJECT_ID(N'dbo.tbl_User')
)
BEGIN
	ALTER TABLE dbo.tbl_User
	ADD CONSTRAINT FK_tbl_User_Province
	FOREIGN KEY (fk_province_id) REFERENCES Province(pk_ID);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_School_Village'
	AND parent_object_id = OBJECT_ID(N'dbo.School')
)
BEGIN
update School set fk_VilageID = null
where fk_VilageID not in (select pk_ID from Village)
	ALTER TABLE dbo.School
	ADD CONSTRAINT FK_School_Village
	FOREIGN KEY (fk_VilageID) REFERENCES Village(pk_ID)
	ON Delete SET NULL;
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_Village_Province'
	AND parent_object_id = OBJECT_ID(N'dbo.Village')
)
BEGIN
	ALTER TABLE dbo.Village
	ADD CONSTRAINT FK_Village_Province
	FOREIGN KEY (i_ProvinceID) REFERENCES Province(pk_ID);
END