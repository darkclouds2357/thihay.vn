﻿using System;

namespace EventBus.Event
{
    public abstract class IntegrationEvent
    {
        public IntegrationEvent()
        {
            CorrelationId = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }

        public Guid CorrelationId { get; }
        public DateTime CreationDate { get; }
        public string ActionBy { get; protected set; }
    }
}