IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'AuthUserId' AND Object_ID = Object_ID(N'dbo.Users')
)
BEGIN
ALTER TABLE 
	dbo.Users
ADD 
	AuthUserId int  NULL;
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'AuthUserId' AND Object_ID = Object_ID(N'dbo.Users')
)
BEGIN
ALTER TABLE 
	dbo.Users
ADD 
	AuthUserId int  NULL;
END

ALTER TABLE dbo.Users ALTER COLUMN d_birth datetime;


IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_Gmail' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE INDEX IX_Users_Gmail
ON dbo.Users (s_gmail);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_GoogleId' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE INDEX IX_Users_GoogleId
ON dbo.Users (s_google_id);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_FacebookId' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE INDEX IX_Users_FacebookId
ON dbo.Users (s_facebook_id);
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_email' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE INDEX IX_Users_email
ON dbo.Users (s_email);
END


IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_AuthUserId' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE INDEX IX_Users_AuthUserId
ON dbo.Users (AuthUserId);
END


--IF NOT EXISTS(
--	SELECT * FROM sys.indexes 
--	WHERE name='IX_Users_Gmail_Emai' AND object_id = OBJECT_ID('dbo.Users')
--)
--BEGIN
--CREATE UNIQUE INDEX IX_Users_Gmail_Emai
--ON dbo.Users (s_gmail, s_email);
--END

--IF NOT EXISTS(
--	SELECT * FROM sys.indexes 
--	WHERE name='IX_Users_AuthUserId_GoogleId' AND object_id = OBJECT_ID('dbo.Users')
--)
--BEGIN
--CREATE UNIQUE INDEX IX_Users_Emai_GoogleId_AuthUserId
--ON dbo.Users (s_gmail, AuthUserId, s_google_id);
--END

--IF NOT EXISTS(
--	SELECT * FROM sys.indexes 
--	WHERE name='IX_Users_AuthUserId_FacebookId' AND object_id = OBJECT_ID('dbo.Users')
--)
--BEGIN
--CREATE UNIQUE INDEX IX_Users_Emai_AuthUserId_FacebookId
--ON dbo.Users (s_email, AuthUserId, s_facebook_id);
--END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Users_AuthUserId_GoogleId_FacebookId' AND object_id = OBJECT_ID('dbo.Users')
)
BEGIN
CREATE UNIQUE INDEX IX_Users_AuthUserId_GoogleId_FacebookId
ON dbo.Users (AuthUserId, s_google_id, s_facebook_id);
END
