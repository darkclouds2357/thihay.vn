IF NOT EXISTS (
	SELECT * FROM sys.tables
	WHERE name='Subject' AND object_id = OBJECT_ID('dbo.Subject')
)
BEGIN
CREATE TABLE [dbo].[Subject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] bit NOT NULL CONSTRAINT DF_Subject_IsDeleted DEFAULT 0,
	[CreatedOn] datetime NULL,
	[UpdatedOn] datetime NULL,
	[SubjectName] nvarchar(100) NOT NULL CONSTRAINT DF_Subject_Name DEFAULT '',
 CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE UNIQUE INDEX IX_Subject_SubjectName ON dbo.Subject (SubjectName);

Update MathTopic set iCategory = 6 where sCategory = N'Tiếng Việt'
Update MathUnit set iSubject = 6 where iTopic IN (Select pk_ID from MathTopic where sCategory = N'Tiếng Việt')

SET IDENTITY_INSERT [dbo].[Subject] ON;
Insert into [dbo].[Subject] (Id, IsDeleted, CreatedOn, UpdatedOn, SubjectName)
select iCategory, 0, getdate(), null, sCategory
from MathTopic
Group by iCategory, sCategory;
SET IDENTITY_INSERT [dbo].[Subject] OFF;

END

IF NOT EXISTS (
	SELECT * FROM sys.tables
	WHERE name='Class' AND object_id = OBJECT_ID('dbo.Class')
)
BEGIN
CREATE TABLE [dbo].[Class](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] bit NOT NULL CONSTRAINT DF_Class_IsDeleted DEFAULT 0,
	[CreatedOn] datetime NULL,
	[UpdatedOn] datetime NULL,
	[ClassName] nvarchar(100) NOT NULL CONSTRAINT DF_Class_Name DEFAULT '',
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE UNIQUE INDEX IX_Class_ClassName ON dbo.Class (ClassName);
SET IDENTITY_INSERT [dbo].[Class] ON;
Insert into [dbo].[Class] (Id, IsDeleted, CreatedOn, UpdatedOn, ClassName)
select iClass, 0 , getdate(), null, CONCAT(N'Lớp ', iClass)
from MathTopic
Group by iClass

SET IDENTITY_INSERT [dbo].[Class] OFF;
END

IF NOT EXISTS (
	SELECT * FROM sys.tables
	WHERE name='QuestionDetail' AND object_id = OBJECT_ID('dbo.QuestionDetail')
)
BEGIN
CREATE TABLE [dbo].[QuestionDetail](
	[Id] uniqueidentifier NOT NULL,
	[IsDeleted] bit NOT NULL CONSTRAINT DF_QuestionDetail_IsDeleted DEFAULT 0,
	[CreatedOn] datetime NULL,
	[UpdatedOn] datetime NULL,
	[QuestionId] int NOT NULL,
	[QuestionContent] nvarchar(1000) NOT NULL CONSTRAINT DF_QuestionDetail_QuestionContent DEFAULT '',
 CONSTRAINT [PK_QuestionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],

CONSTRAINT FK_QuestionDetail_JQuestion FOREIGN KEY (QuestionId)     
    REFERENCES dbo.JQuestion (pk_ID)     
    ON DELETE CASCADE    

) ON [PRIMARY]

CREATE INDEX IX_QuestionDetailx_QuestionId ON dbo.QuestionDetail (QuestionId);
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'Order' AND Object_ID = Object_ID(N'dbo.QuestionDetail')
)
BEGIN
ALTER TABLE 
	dbo.QuestionDetail
ADD 
	[Order] int  NOT NULL CONSTRAINT DF_QuestionDetail_Order DEFAULT 0;
END

IF NOT EXISTS (
	SELECT * FROM sys.tables
	WHERE name='AnswerIndex' AND object_id = OBJECT_ID('dbo.AnswerIndex')
)
BEGIN
CREATE TABLE [dbo].[AnswerIndex](
	[Id] uniqueidentifier NOT NULL,
	[IsDeleted] bit NOT NULL CONSTRAINT DF_AnswerIndex_IsDeleted DEFAULT 0,
	[CreatedOn] datetime NULL,
	[UpdatedOn] datetime NULL,
	[QuestionDetailId] uniqueidentifier NOT NULL,
	[Index] int NOT NULL CONSTRAINT DF_AnswerIndex_Index DEFAULT 0,
 CONSTRAINT [PK_AnswerIndex] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],

CONSTRAINT FK_AnswerIndex_QuestionDetail FOREIGN KEY (QuestionDetailId)     
    REFERENCES dbo.QuestionDetail (Id)     
    ON DELETE CASCADE    

) ON [PRIMARY]

CREATE INDEX IX_AnswerIndex_QuestionId ON dbo.AnswerIndex (QuestionDetailId);
END

IF NOT EXISTS (
	SELECT * FROM sys.tables
	WHERE name='AnswerContent' AND object_id = OBJECT_ID('dbo.AnswerContent')
)
BEGIN
CREATE TABLE [dbo].[AnswerContent](
	[Id] uniqueidentifier NOT NULL,
	[IsDeleted] bit NOT NULL CONSTRAINT DF_AnswerContent_IsDeleted DEFAULT 0,
	[CreatedOn] datetime NULL,
	[UpdatedOn] datetime NULL,
	[QuestionDetailId] uniqueidentifier NOT NULL,
	[Content] nvarchar(500) NOT NULL CONSTRAINT DF_AnswerContent_Content DEFAULT '',
	[Index] int NOT NULL CONSTRAINT DF_AnswerContentx_Index DEFAULT 0,
 CONSTRAINT [PK_AnswerContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],

CONSTRAINT FK_AnswerContent_QuestionDetail FOREIGN KEY (QuestionDetailId)     
    REFERENCES dbo.QuestionDetail (Id)     
    ON DELETE CASCADE    

) ON [PRIMARY]

CREATE INDEX IX_AnswerContent_QuestionId ON dbo.AnswerContent (QuestionDetailId);
END



IF EXISTS (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'JQuestion' AND 
     COLUMN_NAME = 'iType' AND
	 DATA_TYPE = 'tinyint')
BEGIN
ALTER TABLE JQuestion
ALTER COLUMN iType INT NULL
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_JQuestion_iType' AND object_id = OBJECT_ID('dbo.JQuestion')
)
BEGIN
CREATE INDEX IX_JQuestion_iType
ON dbo.JQuestion (iType);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_JQuestion_MathUnit'
	AND parent_object_id = OBJECT_ID(N'dbo.JQuestion')
)
BEGIN

Update JQuestion set iUnit = null
where iUnit not in (select pk_ID from MathUnit)

	ALTER TABLE dbo.JQuestion
	ADD CONSTRAINT FK_JQuestion_MathUnit
	FOREIGN KEY (iUnit) REFERENCES MathUnit (pk_ID)
	ON DELETE SET NULL;
END

IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_JQuestion_iUnit' AND object_id = OBJECT_ID('dbo.JQuestion')
)
BEGIN
CREATE INDEX IX_JQuestion_iUnit
ON dbo.JQuestion (iUnit);
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'Content' AND Object_ID = Object_ID(N'dbo.JQuestion')
)
BEGIN
ALTER TABLE 
	dbo.JQuestion
ADD 
	Content VARCHAR(max)  NOT NULL CONSTRAINT DF_JQuestion_Content DEFAULT '';
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
	WHERE Name = N'Explanation' AND Object_ID = Object_ID(N'dbo.JQuestion')
)
BEGIN
ALTER TABLE 
	dbo.JQuestion
ADD 
	Explanation VARCHAR(500)  NOT NULL CONSTRAINT DF_JQuestion_Explanation DEFAULT '';
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_MathTopic_Class'
	AND parent_object_id = OBJECT_ID(N'dbo.MathTopic')
)
BEGIN
	ALTER TABLE dbo.MathTopic
	ADD CONSTRAINT FK_MathTopic_Class
	FOREIGN KEY (iClass) REFERENCES Class (Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_MathTopic_ClassId' AND object_id = OBJECT_ID('dbo.MathTopic')
)
BEGIN
CREATE INDEX IX_MathTopic_ClassId
ON dbo.MathTopic (iClass);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_MathTopic_Subject'
	AND parent_object_id = OBJECT_ID(N'dbo.MathTopic')
)
BEGIN
	ALTER TABLE dbo.MathTopic
	ADD CONSTRAINT FK_MathTopic_Subject
	FOREIGN KEY (iCategory) REFERENCES [Subject](Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_MathTopic_SubjectId' AND object_id = OBJECT_ID('dbo.MathTopic')
)
BEGIN
CREATE INDEX IX_MathTopic_SubjectId
ON dbo.MathTopic (iCategory);
END


IF EXISTS (SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'MathUnit' AND 
     COLUMN_NAME = 'iSubject' AND
	 DATA_TYPE = 'tinyint')
BEGIN
ALTER TABLE MathUnit
ALTER COLUMN iSubject INT NULL
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_MathUnit_Subject'
	AND parent_object_id = OBJECT_ID(N'dbo.MathUnit')
)
BEGIN
	ALTER TABLE dbo.MathUnit
	ADD CONSTRAINT FK_MathUnit_Subject
	FOREIGN KEY (iSubject) REFERENCES [Subject](Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_MathUnit_SubjectId' AND object_id = OBJECT_ID('dbo.MathUnit')
)
BEGIN
CREATE INDEX IX_MathUnit_SubjectId
ON dbo.MathUnit (iSubject);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_MathUnit_iClass'
	AND parent_object_id = OBJECT_ID(N'dbo.MathUnit')
)
BEGIN
	ALTER TABLE dbo.MathUnit
	ADD CONSTRAINT FK_MathUnit_iClass
	FOREIGN KEY (iClass) REFERENCES [Class](Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_MathUnit_iClass' AND object_id = OBJECT_ID('dbo.MathUnit')
)
BEGIN
CREATE INDEX IX_MathUnit_iClass
ON dbo.MathUnit (iClass);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_MathUnit_iTopic'
	AND parent_object_id = OBJECT_ID(N'dbo.MathUnit')
)
BEGIN
	ALTER TABLE dbo.MathUnit
	ADD CONSTRAINT FK_MathUnit_iTopic
	FOREIGN KEY (iTopic) REFERENCES [MathTopic](pk_ID)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_MathUnit_iTopic' AND object_id = OBJECT_ID('dbo.MathUnit')
)
BEGIN
CREATE INDEX IX_MathUnit_iTopic
ON dbo.MathUnit (iTopic);
END


IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_Marks_MathUnit'
	AND parent_object_id = OBJECT_ID(N'dbo.Marks')
)
BEGIN

Update dbo.Marks set fk_UnitID = null
where fk_UnitID not in (select pk_ID from MathUnit)

	ALTER TABLE dbo.Marks
	ADD CONSTRAINT FK_Marks_MathUnit
	FOREIGN KEY (fk_UnitID) REFERENCES MathUnit(pk_ID)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Marks_MathUnit' AND object_id = OBJECT_ID('dbo.Marks')
)
BEGIN
CREATE INDEX IX_Marks_MathUnit
ON dbo.Marks (fk_UnitID);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_Marks_Subject'
	AND parent_object_id = OBJECT_ID(N'dbo.Marks')
)
BEGIN
	ALTER TABLE dbo.Marks
	ADD CONSTRAINT FK_Marks_Subject
	FOREIGN KEY (fk_SubjectID) REFERENCES Subject(Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Marks_Subject' AND object_id = OBJECT_ID('dbo.Marks')
)
BEGIN
CREATE INDEX IX_Marks_Subject
ON dbo.Marks (fk_SubjectID);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_Marks_Class'
	AND parent_object_id = OBJECT_ID(N'dbo.Marks')
)
BEGIN
	ALTER TABLE dbo.Marks
	ADD CONSTRAINT FK_Marks_Class
	FOREIGN KEY (fk_GradeID) REFERENCES Class(Id)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Marks_Class' AND object_id = OBJECT_ID('dbo.Marks')
)
BEGIN
CREATE INDEX IX_Marks_Class
ON dbo.Marks (fk_GradeID);
END

IF NOT EXISTS (
	SELECT * FROM sys.foreign_keys 
	WHERE name ='FK_Marks_User'
	AND parent_object_id = OBJECT_ID(N'dbo.Marks')
)
BEGIN
	ALTER TABLE dbo.Marks
	ADD CONSTRAINT FK_Marks_User
	FOREIGN KEY (fk_UserID) REFERENCES Users(pk_ID)
	ON DELETE SET NULL;
END
IF NOT EXISTS(
	SELECT * FROM sys.indexes 
	WHERE name='IX_Marks_Class' AND object_id = OBJECT_ID('dbo.Marks')
)
BEGIN
CREATE INDEX IX_Marks_Class
ON dbo.Marks (fk_UserID);
END