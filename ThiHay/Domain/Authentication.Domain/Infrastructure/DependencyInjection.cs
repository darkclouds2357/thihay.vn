﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Authentication.Domain.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddAuthenticationContext(this IServiceCollection services, string connectionString)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<AuthenticationContext>(options => options.UseSqlServer(connectionString),
                ServiceLifetime.Scoped);

            return services;
        }
    }
}