﻿using Authentication.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Authentication.Domain
{
    public class AuthenticationContext : DbContext
    {
        public AuthenticationContext(DbContextOptions<AuthenticationContext> options) : base(options)
        {
        }

        public DbSet<AuthUser> AuthUsers { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Village> Village { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthUser>(AuthUserConfigure);
            modelBuilder.Entity<Province>(ProvinceConfigure);
            modelBuilder.Entity<School>(SchoolConfigure);
            modelBuilder.Entity<Village>(VillageConfigure);
        }

        private void AuthUserConfigure(EntityTypeBuilder<AuthUser> builder)
        {
            builder.ToTable("tbl_User");
            builder.HasKey(p => p.AuthUserId);
            builder.Property(p => p.AuthUserId).IsRequired().HasColumnName("pk_user_id");

            builder.Property(p => p.UserName).IsRequired().HasColumnName("s_username");
            builder.Property(p => p.Password).IsRequired().HasColumnName("s_password");
            builder.Property(p => p.FirstName).IsRequired(false).HasColumnName("s_first_name");
            builder.Property(p => p.LastName).IsRequired(false).HasColumnName("s_last_name");
            builder.Property(p => p.ClassGrade).IsRequired(false).HasColumnName("b_class");
            builder.Property(p => p.FullName).IsRequired().HasColumnName("s_fullname");
            builder.Property(p => p.DateOfBirth).IsRequired().HasColumnName("d_bith");
            builder.Property(p => p.Phone).IsRequired().HasColumnName("s_phone");
            builder.Property(p => p.Address).IsRequired().HasColumnName("s_address");
            builder.Property(p => p.Email).IsRequired().HasColumnName("s_email");

            builder.Property(p => p.SchoolId).IsRequired().HasColumnName("fk_school_id");
            builder.HasOne(p => p.School).WithMany(s => s.Users).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.SetNull);
            builder.Property(p => p.SchoolName).IsRequired().HasColumnName("s_school_name");

            builder.Property(p => p.VillageId).IsRequired().HasColumnName("fk_village_id");
            builder.HasOne(p => p.Village).WithMany(v => v.Users).HasForeignKey(p => p.VillageId);
            builder.Property(p => p.VillageName).IsRequired().HasColumnName("s_village_name");

            builder.Property(p => p.ProvinceId).IsRequired().HasColumnName("fk_province_id");
            builder.HasOne(p => p.Province).WithMany(p => p.Users).HasForeignKey(p => p.ProvinceId);
            builder.Property(p => p.ProvinceName).IsRequired().HasColumnName("s_province_name");

            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("d_create_date");

            builder.Property(p => p.UserType).IsRequired(false).HasColumnName("b_user_type");
            builder.Property(p => p.Gmail).IsRequired(false).HasColumnName("Gmail");
            builder.HasIndex(p => p.Gmail).IsUnique(false);

            builder.Property(p => p.GoogleId).IsRequired(false).HasColumnName("GoogleId");
            builder.HasIndex(p => p.GoogleId).IsUnique(false);
            builder.Property(p => p.FacebookId).IsRequired(false).HasColumnName("FacebookId");
            builder.HasIndex(p => p.FacebookId).IsUnique(false);

            builder.Property(p => p.Salt).IsRequired(false).HasColumnName("Salt");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.HasIndex(p => p.IsDeleted).IsUnique(false);

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.UpdatedBy);
        }

        private void ProvinceConfigure(EntityTypeBuilder<Province> builder)
        {
            builder.ToTable("Province");
            builder.HasKey(p => p.ProvinceId);

            builder.Property(p => p.ProvinceId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("StatusAccess");
            builder.Property(p => p.NumberPupil).IsRequired().HasColumnName("i_NumberPupil");
            builder.Property(p => p.Name).IsRequired().HasMaxLength(50).HasColumnName("s_Name");
            builder.HasIndex(p => p.Name).IsUnique(false);

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.UpdatedBy);
        }

        private void SchoolConfigure(EntityTypeBuilder<School> builder)
        {
            builder.ToTable("School");
            builder.HasKey(p => p.SchoolId);

            builder.Property(p => p.SchoolId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.Level).IsRequired(false).HasMaxLength(5).HasColumnName("fk_Level");
            builder.Property(p => p.VillageId).IsRequired(false).HasColumnName("fk_VilageID");
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100).HasColumnName("s_Name");

            builder.HasOne(p => p.Village).WithMany(v => v.Schools).HasForeignKey(p => p.VillageId).OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(p => p.VillageId).IsUnique(false);

            builder.HasIndex(p => p.Level).IsUnique(false);

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.IsDeleted)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.UpdatedBy);
        }

        private void VillageConfigure(EntityTypeBuilder<Village> builder)
        {
            builder.ToTable("Village");
            builder.HasKey(p => p.VillageId);

            builder.Property(p => p.VillageId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("StatusAccess");
            builder.Property(p => p.ProvinceId).IsRequired(false).HasColumnName("i_ProvinceID");
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100).HasColumnName("s_Name");

            builder.HasOne(p => p.Province).WithMany(p=>p.Villages).HasForeignKey(p => p.ProvinceId);
            builder.HasIndex(p => p.ProvinceId).IsUnique(false);

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.UpdatedBy);
        }
    }
}