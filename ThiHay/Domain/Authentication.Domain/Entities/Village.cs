﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Authentication.Domain.Entities
{
    public partial class Village : BaseEntity
    {
        public Village()
        {
            this.Users = new HashSet<AuthUser>();
            this.Schools = new HashSet<School>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VillageId { get; set; }

        public int? ProvinceId { get; set; }
        public virtual Province Province { get; set; }

        public string Name { get; set; }

        public virtual ICollection<AuthUser> Users { get; set; }
        public virtual ICollection<School> Schools { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}