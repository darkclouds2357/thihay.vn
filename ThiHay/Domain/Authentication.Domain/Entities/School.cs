﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Authentication.Domain.Entities
{
    public partial class School : BaseEntity
    {
        public School()
        {
            this.Users = new HashSet<AuthUser>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SchoolId { get; set; }

        public int? VillageId { get; set; }
        public virtual Village Village { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public virtual ICollection<AuthUser> Users { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override bool IsDeleted { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}