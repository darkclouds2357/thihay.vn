﻿using ST.Library.Base.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Authentication.Domain.Entities
{
    public partial class AuthUser : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthUserId { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public byte? ClassGrade { get; set; }
        public int? SchoolId { get; set; }
        public virtual School School { get; set; }
        public string SchoolName { get; set; }
        public int? VillageId { get; set; }
        public virtual Village Village { get; set; }
        public string VillageName { get; set; }
        public int? ProvinceId { get; set; }
        public virtual Province Province { get; set; }
        public string ProvinceName { get; set; }

        public byte? UserType { get; set; }

        public string Salt { get; set; }
        public string Gmail { get; set; }
        public string GoogleId { get; set; }
        public string FacebookId { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}