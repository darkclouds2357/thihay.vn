﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Authentication.Domain.Entities
{
    public partial class Province : BaseEntity
    {
        public Province()
        {
            this.Users = new HashSet<AuthUser>();
            this.Villages = new HashSet<Village>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProvinceId { get; set; }

        public string Name { get; set; }

        public int NumberPupil { get; set; }

        public virtual ICollection<AuthUser> Users { get; set; }
        public virtual ICollection<Village> Villages { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}