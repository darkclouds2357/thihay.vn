﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Thihay.Domain.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddThihayContext(this IServiceCollection services, string connectionString)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ThihayContext>(options => options.UseSqlServer(connectionString),
                ServiceLifetime.Scoped);

            return services;
        }
    }
}