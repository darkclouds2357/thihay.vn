﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Thihay.Domain.Entities;

namespace Thihay.Domain
{
    public class ThihayContext : DbContext
    {
        public ThihayContext(DbContextOptions<ThihayContext> options) : base(options)
        {
        }

        public DbSet<AnswerContent> AnswerContents { get; set; }
        public DbSet<AnswerIndex> AnswerIndexs { get; set; }
        public DbSet<Class> Clases { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionDetail> QuestionDetails { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            try
            {
                modelBuilder.Entity<AnswerContent>(AnswerContentConfigure);
                modelBuilder.Entity<AnswerIndex>(AnswerIndexConfigure);
                modelBuilder.Entity<Class>(ClassConfigure);
                modelBuilder.Entity<Mark>(MarkConfigure);
                modelBuilder.Entity<Question>(QuestionConfigure);
                modelBuilder.Entity<QuestionDetail>(QuestionDetailConfigure);
                modelBuilder.Entity<Subject>(SubjectConfigure);
                modelBuilder.Entity<Topic>(TopicConfigure);
                modelBuilder.Entity<Unit>(UnitConfigure);
                modelBuilder.Entity<User>(UserConfigure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UserConfigure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(p => p.UserId);

            builder.Property(p => p.UserId).IsRequired().HasColumnName("pk_ID");

            builder.Property(p => p.FullName).IsRequired().HasMaxLength(120).HasColumnName("s_fullname");
            builder.Property(p => p.Gmail).IsRequired(false).HasMaxLength(100)
                .HasColumnName("s_gmail");
            builder.HasIndex(p => p.Gmail).IsUnique(false);

            builder.Property(p => p.GoogleId).IsRequired(false).HasMaxLength(50)
                .HasColumnName("s_google_id");
            builder.HasIndex(p => p.GoogleId).IsUnique(false);

            builder.Property(p => p.FacebookId).IsRequired(false).HasMaxLength(50)
                .HasColumnName("s_facebook_id");
            builder.HasIndex(p => p.FacebookId).IsUnique(false);

            builder.Property(p => p.AuthUserId).IsRequired(false)
                .HasColumnName("AuthUserId");
            builder.HasIndex(p => p.AuthUserId).IsUnique(false);

            builder.Property(p => p.Email).IsRequired(false).HasMaxLength(100)
                .HasColumnName("s_email");

            builder.Property(p => p.Phone).IsRequired(false).HasMaxLength(15)
                .HasColumnName("s_phone");

            builder.Property(p => p.DayOfBirth).IsRequired(false).HasColumnName("d_birth");

            builder.Property(p => p.Address).IsRequired(false).HasColumnName("s_address");

            builder.Property(p => p.Wallet).IsRequired(false).HasColumnName("i_wallet");

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.IsDeleted)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.UpdatedBy);
        }

        private void UnitConfigure(EntityTypeBuilder<Unit> builder)
        {
            builder.ToTable("MathUnit");
            builder.HasKey(p => p.UnitId);

            builder.Property(p => p.UnitId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("act");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("sDate");
            builder.Property(p => p.SortOrder).IsRequired(false).HasColumnName("iSort");
            builder.Property(p => p.NameTopic).IsRequired().HasColumnName("NameTopic");
            builder.Property(p => p.NameUnit).IsRequired().HasColumnName("NameUnit");
            builder.Property(p => p.DescriptionUnit).IsRequired().HasColumnName("DescriptionUnit");
            builder.Property(p => p.ListQuestionIds).IsRequired(false).HasColumnName("ListIQuestion");
            builder.Property(p => p.CreatedUserId).IsRequired(false).HasColumnName("Id_usercreate");

            builder.Property(p => p.ClassId).IsRequired(false).HasColumnName("iClass");
            builder.HasIndex(p => p.ClassId).IsUnique(false);
            //builder.HasOne(p => p.Class).WithMany(p => p.Units).HasForeignKey(p => p.ClassId).OnDelete(DeleteBehavior.SetNull);

            builder.Property(p => p.SubjectId).IsRequired(false).HasColumnName("iSubject");
            builder.HasIndex(p => p.SubjectId).IsUnique(false);
            //builder.HasOne(p => p.Subject).WithMany(p => p.Units).HasForeignKey(p => p.SubjectId).OnDelete(DeleteBehavior.SetNull);

            builder.Property(p => p.TopicId).IsRequired(false).HasColumnName("iTopic");
            builder.HasIndex(p => p.TopicId).IsUnique(false);
            builder.HasOne(p => p.Topic).WithMany(p => p.Units).HasForeignKey(p => p.TopicId).OnDelete(DeleteBehavior.SetNull);

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy)
                .Ignore(p => p.UpdatedOn);
        }

        private void TopicConfigure(EntityTypeBuilder<Topic> builder)
        {
            builder.ToTable("MathTopic");
            builder.HasKey(p => p.TopicId);

            builder.Property(p => p.TopicId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("act");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("sDate");

            builder.Property(p => p.ClassId).IsRequired(false).HasColumnName("iClass");
            builder.HasIndex(p => p.ClassId).IsUnique(false);
            builder.HasOne(p => p.Class).WithMany(c => c.Topics).HasForeignKey(p => p.ClassId).OnDelete(DeleteBehavior.SetNull);

            builder.Property(p => p.SubjectId).IsRequired(false).HasColumnName("iCategory");
            builder.HasIndex(p => p.SubjectId).IsUnique(false);
            builder.HasOne(p => p.Subject).WithMany(s => s.Topics).HasForeignKey(p => p.SubjectId).OnDelete(DeleteBehavior.SetNull);

            builder.Property(p => p.SubjectName).IsRequired().HasMaxLength(50).HasColumnName("sCategory");
            builder.HasIndex(p => p.SubjectName).IsUnique(false);

            builder.Property(p => p.NameTopic).IsRequired().HasColumnName("NameTopic");
            builder.Property(p => p.DescriptionTopic).IsRequired().HasColumnName("DescriptionTopic");
            builder.Property(p => p.CreatedUser).IsRequired(false).HasColumnName("Id_usercreate");

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy)
                .Ignore(p => p.UpdatedOn);
        }

        private void SubjectConfigure(EntityTypeBuilder<Subject> builder)
        {
            builder.ToTable(nameof(Subject));
            builder.HasKey(p => p.SubjectId);

            builder.Property(p => p.SubjectId).IsRequired().HasColumnName("Id");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            builder.Property(p => p.UpdatedOn).IsRequired().HasColumnName("UpdatedOn");

            builder.Property(p => p.SubjectName).IsRequired().HasMaxLength(100).HasColumnName("SubjectName");

            builder.HasIndex(p => p.SubjectName).IsUnique();

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void QuestionDetailConfigure(EntityTypeBuilder<QuestionDetail> builder)
        {
            builder.ToTable(nameof(QuestionDetail));
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).IsRequired().HasColumnName("Id");
            builder.Property(p => p.QuestionId).IsRequired().HasColumnName("QuestionId");
            builder.HasIndex(p => p.QuestionId).IsUnique(false);
            builder.HasOne(p => p.Question).WithMany(p => p.QuestionDetails).HasForeignKey(p => p.QuestionId).OnDelete(DeleteBehavior.Cascade);
            builder.Property(p => p.QuestionContent).IsRequired().HasColumnName("QuestionContent");

            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            builder.Property(p => p.UpdatedOn).IsRequired().HasColumnName("UpdatedOn");
            builder.Property(p => p.Order).IsRequired().HasColumnName("Order");
            builder
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void QuestionConfigure(EntityTypeBuilder<Question> builder)
        {
            builder.ToTable("JQuestion");
            builder.HasKey(p => p.QuestionId);

            builder.Property(p => p.QuestionId).IsRequired().HasColumnName("pk_ID");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("iAble");
            builder.Property(p => p.QuestionJsonContent).IsRequired().HasMaxLength(int.MaxValue).HasColumnName("jQuestion");

            builder.Property(p => p.Type).IsRequired().HasColumnName("iType");
            builder.HasIndex(p => p.Type).IsUnique(false);

            builder.Property(p => p.UnitId).IsRequired(false).HasColumnName("iUnit");
            builder.HasOne(p => p.Unit).WithMany(p => p.Questions).HasForeignKey(p => p.UnitId).OnDelete(DeleteBehavior.SetNull);

            builder.HasIndex(p => p.UnitId).IsUnique(false);

            builder.Property(p => p.Content).IsRequired().HasMaxLength(int.MaxValue).HasColumnName("Content");
            builder.Property(p => p.Explanation).IsRequired(false).HasMaxLength(500).HasColumnName("Explanation");

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.UpdatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void MarkConfigure(EntityTypeBuilder<Mark> builder)
        {
            builder.ToTable("Marks");
            builder.HasKey(p => p.MarkId);

            builder.Property(p => p.MarkId).IsRequired().HasColumnName("pk_ID");

            builder.Property(p => p.UserID).IsRequired(false).HasColumnName("fk_UserID");
            builder.HasOne(p => p.User).WithMany(p => p.Marks).HasForeignKey(p => p.UserID).OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(p => p.UserID).IsUnique(false);

            builder.Property(p => p.ClassId).IsRequired(false).HasColumnName("fk_GradeID");
            builder.HasOne(p => p.Class).WithMany(p => p.Marks).HasForeignKey(p => p.ClassId).OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(p => p.ClassId).IsUnique(false);

            builder.Property(p => p.SubjectId).IsRequired(false).HasColumnName("fk_SubjectID");
            builder.HasOne(p => p.Subject).WithMany(p => p.Marks).HasForeignKey(p => p.SubjectId).OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(p => p.SubjectId).IsUnique(false);

            builder.Property(p => p.UnitId).IsRequired(false).HasColumnName("fk_UnitID");
            builder.HasOne(p => p.Unit).WithMany(p => p.Marks).HasForeignKey(p => p.UnitId).OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(p => p.UnitId).IsUnique(false);

            builder.Property(p => p.MarkPoint).IsRequired(false).HasColumnName("iMark");

            builder.Property(p => p.Time).IsRequired(false).HasColumnName("iTime");
            builder.Property(p => p.AnsweredCount).IsRequired(false).HasColumnName("iAnswered");
            builder.Property(p => p.PassedCount).IsRequired(false).HasColumnName("iPassed");
            builder.Property(p => p.QuestionPassed).IsRequired(false).HasColumnName("sPassed");

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.IsDeleted)
                .Ignore(p => p.CreatedOn)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void ClassConfigure(EntityTypeBuilder<Class> builder)
        {
            builder.ToTable(nameof(Class));
            builder.HasKey(p => p.ClassId);

            builder.Property(p => p.ClassId).IsRequired().HasColumnName("Id");
            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            builder.Property(p => p.UpdatedOn).IsRequired().HasColumnName("UpdatedOn");

            builder.Property(p => p.ClassName).IsRequired().HasMaxLength(100).HasColumnName("ClassName");

            builder.HasIndex(p => p.ClassName).IsUnique();

            builder
                .Ignore(p => p.Id)
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void AnswerIndexConfigure(EntityTypeBuilder<AnswerIndex> builder)
        {
            builder.ToTable(nameof(AnswerIndex));
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).IsRequired().HasColumnName("Id");
            builder.Property(p => p.QuestionDetailId).IsRequired().HasColumnName("QuestionDetailId");
            builder.HasIndex(p => p.QuestionDetailId).IsUnique(false);
            builder.HasOne(p => p.Question).WithMany(p => p.AnswerIndexs).HasForeignKey(p => p.QuestionDetailId).OnDelete(DeleteBehavior.Cascade);
            builder.Property(p => p.Index).IsRequired().HasColumnName("Index");

            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            builder.Property(p => p.UpdatedOn).IsRequired().HasColumnName("UpdatedOn");

            builder
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }

        private void AnswerContentConfigure(EntityTypeBuilder<AnswerContent> builder)
        {
            builder.ToTable(nameof(AnswerContent));
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).IsRequired().HasColumnName("Id");
            builder.Property(p => p.QuestionDetailId).IsRequired().HasColumnName("QuestionDetailId");
            builder.HasIndex(p => p.QuestionDetailId).IsUnique(false);
            builder.HasOne(p => p.Question).WithMany(p => p.AnswerContents).HasForeignKey(p => p.QuestionDetailId).OnDelete(DeleteBehavior.Cascade);

            builder.Property(p => p.Content).IsRequired().HasColumnName("Content");
            builder.Property(p => p.Index).IsRequired().HasColumnName("Index");

            builder.Property(p => p.IsDeleted).IsRequired().HasColumnName("IsDeleted");
            builder.Property(p => p.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            builder.Property(p => p.UpdatedOn).IsRequired().HasColumnName("UpdatedOn");

            builder
                .Ignore(p => p.CreatedBy)
                .Ignore(p => p.UpdatedBy);
        }
    }
}