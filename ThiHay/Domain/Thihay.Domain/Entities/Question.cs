﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Thihay.Domain.Enum;
using System.Linq.Expressions;

namespace Thihay.Domain.Entities
{
    public partial class Question : BaseEntity, ISecuredCondition<Question>
    {
        public Question()
        {
            this.QuestionDetails = new HashSet<QuestionDetail>();
        }

        [Key]
        public int QuestionId { get; set; }

        [ForeignKey("Unit")]
        public int? UnitId { get; set; }

        public virtual Unit Unit { get; set; }

        public string QuestionJsonContent { get; set; }

        public QuestionType? Type { get; set; }

        public string Content { get; set; }

        public string Explanation { get; set; }

        public virtual ICollection<QuestionDetail> QuestionDetails { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }

        public Expression<Func<Question, bool>> SecuredCondition => q => q.UnitId.HasValue && q.Type != QuestionType.UnderlineAndRewrite && q.Type != QuestionType.DeleteCharacter;
    }
}