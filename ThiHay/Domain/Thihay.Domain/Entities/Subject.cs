﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Thihay.Domain.Entities
{
    public partial class Subject : BaseEntity
    {
        public Subject() : base()
        {
            this.Marks = new HashSet<Mark>();
            this.Topics = new HashSet<Topic>();
            //this.Units = new HashSet<Unit>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubjectId { get; set; }

        public string SubjectName { get; set; }
        public virtual ICollection<Mark> Marks { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        //public virtual ICollection<Unit> Units { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}