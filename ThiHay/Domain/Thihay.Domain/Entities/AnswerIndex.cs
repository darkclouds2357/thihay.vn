﻿using ST.Library.Base.Entity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Thihay.Domain.Entities
{
    public partial class AnswerIndex : BaseEntity
    {
        public int Index { get; set; }
        public virtual QuestionDetail Question { get; set; }

        [ForeignKey("Question")]
        public Guid QuestionDetailId { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}