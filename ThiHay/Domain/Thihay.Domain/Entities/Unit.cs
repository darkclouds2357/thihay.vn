﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Thihay.Domain.Entities
{
    public partial class Unit : BaseEntity, ISecuredCondition<Unit>
    {
        public Unit()
        {
            this.Marks = new HashSet<Mark>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UnitId { get; set; }

        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }

        //public virtual Subject Subject { get; set; }
        [ForeignKey("Class")]
        public int? ClassId { get; set; }

        //public virtual Class Class { get; set; }
        [ForeignKey("Topic")]
        public int? TopicId { get; set; }

        public virtual Topic Topic { get; set; }

        public string ListQuestionIds { get; set; }

        public string NameTopic { get; set; }

        public string NameUnit { get; set; }

        public string DescriptionUnit { get; set; }

        public int? CreatedUserId { get; set; }

        public int? SortOrder { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        [NotMapped]
        public IEnumerable<int> QuestionIds
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ListQuestionIds))
                {
                    var ids = ListQuestionIds.Split(';');
                    foreach (var id in ids)
                    {
                        if (int.TryParse(id, out int questionId))
                        {
                            yield return questionId;
                        }
                    }
                }
            }
        }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }

        public Expression<Func<Unit, bool>> SecuredCondition => u => u.TopicId.HasValue;
    }
}