﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Thihay.Domain.Entities
{
    public partial class Topic : BaseEntity, ISecuredCondition<Topic>
    {
        public Topic() : base()
        {
            this.Units = new HashSet<Unit>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TopicId { get; set; }

        [ForeignKey("Class")]
        public int? ClassId { get; set; }

        public virtual Class Class { get; set; }

        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }

        public virtual Subject Subject { get; set; }

        public string SubjectName { get; set; }

        public string NameTopic { get; set; }

        public string DescriptionTopic { get; set; }

        public int? CreatedUser { get; set; }

        public virtual ICollection<Unit> Units { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }

        public Expression<Func<Topic, bool>> SecuredCondition => t => t.SubjectId.HasValue && t.ClassId.HasValue;
    }
}