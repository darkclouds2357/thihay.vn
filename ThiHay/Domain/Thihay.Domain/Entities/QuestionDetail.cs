﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Thihay.Domain.Entities
{
    public partial class QuestionDetail : BaseEntity
    {
        public QuestionDetail()
        {
            this.AnswerContents = new HashSet<AnswerContent>();
            this.AnswerIndexs = new HashSet<AnswerIndex>();
        }

        public string QuestionContent { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }

        public virtual Question Question { get; set; }

        public virtual ICollection<AnswerContent> AnswerContents { get; set; }

        public virtual ICollection<AnswerIndex> AnswerIndexs { get; set; }

        public int Order { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}