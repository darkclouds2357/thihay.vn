﻿using ST.Library.Base.Entity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Thihay.Domain.Entities
{
    public partial class AnswerContent : BaseEntity
    {
        public string Content { get; set; }

        public int Index { get; set; }

        [ForeignKey("Question")]
        public Guid QuestionDetailId { get; set; }

        public QuestionDetail Question { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }
    }
}