﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Thihay.Domain.Entities
{
    public partial class User : BaseEntity
    {
        public User()
        {
            this.Marks = new HashSet<Mark>();
        }

        [Key]
        public int UserId { get; set; }

        public string FullName { get; set; }

        public string Gmail { get; set; }

        public string GoogleId { get; set; }

        public string FacebookId { get; set; }

        public int? AuthUserId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime? DayOfBirth { get; set; }

        public string Address { get; set; }

        public int? Wallet { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }

        [NotMapped]
        public override bool IsDeleted { get; set; }
    }
}