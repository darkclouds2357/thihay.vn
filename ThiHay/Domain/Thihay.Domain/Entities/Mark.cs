﻿using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Thihay.Domain.Entities
{
    public partial class Mark : BaseEntity, ISecuredCondition<Mark>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MarkId { get; set; }

        [ForeignKey("User")]
        public int? UserID { get; set; }

        public virtual User User { get; set; }

        [ForeignKey("Class")]
        public int? ClassId { get; set; }

        public virtual Class Class { get; set; }

        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }

        public virtual Subject Subject { get; set; }

        [ForeignKey("Unit")]
        public int? UnitId { get; set; }

        public virtual Unit Unit { get; set; }

        public int? MarkPoint { get; set; }

        public int? Time { get; set; }

        public int? AnsweredCount { get; set; }

        public int? PassedCount { get; set; }

        public string QuestionPassed { get; set; }

        [NotMapped]
        public IEnumerable<int> PassedQuestionId
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(QuestionPassed))
                {
                    var ids = QuestionPassed.Split(';');
                    foreach (var id in ids)
                    {
                        if (int.TryParse(id, out int questionId))
                        {
                            yield return questionId;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
        }

        [NotMapped]
        public override Guid Id { get; set; }

        [NotMapped]
        public override DateTime? CreatedOn { get; set; }

        [NotMapped]
        public override Guid? CreatedBy { get; set; }

        [NotMapped]
        public override DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public override Guid? UpdatedBy { get; set; }

        [NotMapped]
        public override bool IsDeleted { get; set; }

        public Expression<Func<Mark, bool>> SecuredCondition => m => m.UnitId.HasValue && m.ClassId.HasValue && m.SubjectId.HasValue;
    }
}