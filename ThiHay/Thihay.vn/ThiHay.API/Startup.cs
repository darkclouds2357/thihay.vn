﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ST.Library.Common;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using Thihay.Common;
using Thihay.Domain;
using Thihay.Domain.Entities;
using Thihay.Domain.Infrastructure;
using Thihay.Services;
using Thihay.Services.Infrastructure;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Thihay.Common.TokenProvider;
using Thihay.Common.Securities;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ThiHay.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("keys.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.Configure<AppSettings>(Configuration);
            services.AddThihayContext(Configuration["ConnectionString"]);
            services.AddScoped<ICurrentUser, ThihayUser>();
            services.AddTransient<IAudienceStore, AudiencesStore>(sp =>
            {
                return new AudiencesStore(Configuration["ClientId"], Configuration["Secret"]);
            });

            services.AddTransient<Credential>(sp =>
            {
                return new Credential(sp.GetRequiredService<IAudienceStore>(), Configuration["ClientId"], Configuration["Name"]);
            });
            services.AddUnitOfWork<ThihayContext>(new List<Type> {
                typeof(AnswerContent),
                typeof(AnswerIndex),
                typeof(Class),
                typeof(Mark),
                typeof(Question),
                typeof(QuestionDetail),
                typeof(Subject),
                typeof(Topic),
                typeof(Unit),
                typeof(User)
            });

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Thihay HTTP API",
                    Version = "v1",
                    Description = "The Thihay Service HTTP API",
                    TermsOfService = "Terms Of Service"
                });
            });

            //services.AddThihayService();
            var container = new ContainerBuilder();
            container.Populate(services);
            container.Register(c => c.Resolve<ICurrentUser>() as IThihayUser);
            //container.RegisterType<ThihayUser>().As<IThihayUser>().InstancePerRequest();
            container.RegisterModule(new ServiceModule());

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

           

            app.UseSwagger()
               .UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "Thihay API V1");
               });

            var credential = app.ApplicationServices.GetService<Credential>();
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = credential.SecurityKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = credential.Issuer,
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = credential.ClientId,
                // Validate the token expiry
                ValidateLifetime = false,
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters,
                AuthenticationScheme = JwtBearerDefaults.AuthenticationScheme
            });

            app.Use(async (ctx, next) =>
            {
                var user = ctx.RequestServices.GetRequiredService<IThihayUser>();
                user.UpdateIdentity(ctx.User.Identity as ClaimsIdentity);
                await next.Invoke();
            });

            //app.UseFacebookAuthentication(new FacebookOptions
            //{
            //    AppId = Configuration["facebook:appid"],
            //    AppSecret = Configuration["facebook:appsecret"],
            //    Scope = { "email" },
            //    Fields = { "name", "email" },
            //    SaveTokens = true,
            //});

            //app.UseGoogleAuthentication(new GoogleOptions
            //{

            //});

            //app.Use(async (ctx, next) =>
            //{
            //    var user = ctx.RequestServices.GetRequiredService<IThihayUser>();
            //    user.UpdateIdentity(ctx.User.Identity as ClaimsIdentity);
            //    await next.Invoke();
            //});
            app.UseMvcWithDefaultRoute();
            app.UseDeveloperExceptionPage();

        }        
    }
}