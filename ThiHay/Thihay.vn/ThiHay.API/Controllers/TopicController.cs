using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace ThiHay.API.Controllers
{
    [Produces("application/json")]
    [Route("api/ChuyenDe")]
    public class TopicController : Controller
    {
        private readonly IThihayService _thihayServices;

        public TopicController(IThihayService thihayService)
        {
            _thihayServices = thihayService;
        }
        [Authorize]
        [HttpGet]
        [Route("topic")]
        public IActionResult GetTopic([FromQuery]string searchKey, [FromQuery]int? subjectId, [FromQuery]int? classId, [FromQuery]int? page, [FromQuery]int? pageSize)
        {
            var topics = _thihayServices.GetTopics(searchKey, subjectId, classId, page, pageSize, out int totalPage);

            if (topics.Count() > 0)
            {
                return Ok(new { Topics = topics, TotalPage = totalPage });
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpGet]
        [Route("topic/{topicId}")]
        public IActionResult GetTopicDetail(int topicId)
        {
            var topic = _thihayServices.GetTopicDetail(topicId);
            if (topic != null)
            {
                return Ok(topic);
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpPost]
        [Route("topic/update")]
        public async Task<IActionResult> UpdateTopic([FromBody] TopicDTO topic)
        {
            var result = await _thihayServices.UpdateTopic(topic);
            return Ok(result);
        }
        [Authorize]
        [HttpGet]
        [Route("topic/{topicId}/status/{isDelete}")]
        public async Task<IActionResult> ChangeTopicStatus(int topicId, bool isDelete)
        {
            var result = await _thihayServices.ChangeTopicStatus(topicId, isDelete);
            return Ok(result);
        }
        [Authorize]
        [HttpGet]
        [Route("unit")]
        public IActionResult GetUnit([FromQuery]string searchKey, [FromQuery]int? subjectId, [FromQuery]int? classId, [FromQuery] int? topicId, [FromQuery]int? page, [FromQuery]int? pageSize)
        {
            var units = _thihayServices.GetUnits(searchKey, subjectId, classId, topicId, page, pageSize, out int totalPage);

            if (units.Count() > 0)
            {
                return Ok(new { Units = units, TotalPage = totalPage });
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpGet]
        [Route("unit/{unitId}")]
        public IActionResult GetUnitDetail(int unitId)
        {
            var unit = _thihayServices.GetUnitDetail(unitId);
            if (unit != null)
            {
                return Ok(unit);
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpPost]
        [Route("unit/update")]
        public async Task<IActionResult> UpdateUnit([FromBody]UnitDTO unitDto)
        {
            var result = await _thihayServices.UpdateUnit(unitDto);

            return Ok(result);
        }
        [Authorize]
        [HttpGet]
        [Route("unit/{unitId}/order/{newOrder}")]
        public async Task<IActionResult> UpdateUnitOrder(int unitId, int newOrder)
        {
            var result = await _thihayServices.UpdateUnitOrder(unitId, newOrder);

            return Ok(result);
        }
        [Authorize]
        [HttpGet]
        [Route("unit/{unitId}/status/{isDelete}")]
        public async Task<IActionResult> ChangeUnitStatus(int unitId, bool isDelete)
        {
            var result = await _thihayServices.ChangeUnitStatus(unitId, isDelete);
            return Ok(result);
        }
    }
}