using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace ThiHay.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ClassController : Controller
    {
        private readonly IThihayService _thihayServices;

        public ClassController(IThihayService thihayServices)
        {
            _thihayServices = thihayServices;
        }
        [Authorize]
        [HttpGet]
        [Route("")]
        public IActionResult GetClass([FromQuery]string searchKey)
        {
            var classes = _thihayServices.GetClass(searchKey);
            if (classes.Count() > 0)
            {
                return Ok(classes);
            }
            return NotFound();
        }
        [Authorize]
        [HttpGet]
        [Route("{classId}/status/{isDelete}")]
        public async Task<IActionResult> ChangeClassStatus(int classId, bool isDelete)
        {
            var result = await _thihayServices.ChangeClassStatus(classId, isDelete);
            return Ok(result);
        }
        [Authorize]
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateClass([FromBody]ClassDTO @class)
        {
            var result = await _thihayServices.UpdateClass(@class);
            return Ok(result);
        }
    }
}