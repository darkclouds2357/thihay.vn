using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.Domain.Enum;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace ThiHay.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Question")]
    public class QuestionController : Controller
    {
        private readonly IThihayService _thihayService;

        public QuestionController(IThihayService thihayService)
        {
            _thihayService = thihayService;
        }
        [Authorize]
        [HttpGet]
        [Route("{questionId}")]
        public IActionResult GetQuestionDetail(int questionId)
        {
            var result = _thihayService.GetQuestionDetail(questionId);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpGet]
        [Route("")]
        public IActionResult GetAllQuestion([FromQuery]string searchKey, [FromQuery]int? subjectId, [FromQuery] int? classId, [FromQuery] int? topicId, [FromQuery] int? unitId, [FromQuery] QuestionType? questionType, [FromQuery] int? page, [FromQuery] int? pageSize)
        {
            var questions = _thihayService.GetAllQuestion(searchKey, subjectId, classId, topicId, unitId, questionType, out int total, page, pageSize);

            if (questions.Count() > 0)
            {
                return Ok(new
                {
                    Questions = questions,
                    TotalPage = total
                });
            }
            else
            {
                return NotFound();
            }
        }
        [Authorize]
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateQuestion([FromBody]QuestionDTO questionDto)
        {
            var result = await _thihayService.UpdateQuestion(questionDto);
            return Ok(result);
        }
        [Authorize]
        [HttpGet]
        [Route("{questionId}/status/{isDelete}")]
        public async Task<IActionResult> ChangeQuestionStatus(int questionId, bool isDelete)
        {
            var result = await _thihayService.ChangeQuestionStatus(questionId, isDelete);
            return Ok(result);
        }
    }
}