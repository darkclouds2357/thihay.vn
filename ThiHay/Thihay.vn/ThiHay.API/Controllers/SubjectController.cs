using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace ThiHay.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Subject")]
    public class SubjectController : Controller
    {
        private readonly IThihayService _thihayService;

        public SubjectController(IThihayService thihayService)
        {
            _thihayService = thihayService;
        }
        [Authorize]
        [HttpGet]
        [Route("")]
        public IActionResult GetSubjects([FromQuery]string searchKey)
        {
            var classes = _thihayService.GetSubject(searchKey);
            if (classes.Count() > 0)
            {
                return Ok(classes);
            }
            return NotFound();
        }
        [Authorize]
        [HttpGet]
        [Route("{subjectId}/status/{isDelete}")]
        public async Task<IActionResult> ChangeSubjectStatus(int subjectId, bool isDelete)
        {
            var result = await _thihayService.ChangeSubjectStatus(subjectId, isDelete);
            return Ok(result);
        }
        [Authorize]
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateSubject([FromBody]SubjectDTO subject)
        {
            var result = await _thihayService.UpdateSubject(subject);
            return Ok(result);
        }
    }
}