using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace ThiHay.API.Controllers
{
    [Produces("application/json")]
    [Route("api/EndUser")]
    public class EndUserController : Controller
    {
        private readonly IEndUserService _endUserService;

        public EndUserController(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        [HttpGet]
        [Route("summary")]
        public IActionResult GetSummary()
        {
            var summaries = _endUserService.GetSummary();
            if (summaries.Count() > 0)
            {
                return Ok(summaries);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("subject")]
        public IActionResult GetAllSubject()
        {
            var subjects = _endUserService.GetAllSubject();
            if (subjects.Count() > 0)
            {
                return Ok(subjects);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("{subjectId:int}/class")]
        public IActionResult GetClassBySubjectId(int subjectId)
        {
            var classes = _endUserService.GetValidClassesBySubjetId(subjectId);
            if (classes.Count() > 0)
            {
                return Ok(classes);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("{subjectId:int}/class/{classId:int}/topics")]
        public IActionResult GetTopicByClassIdAndSubjectId(int subjectId, int classId)
        {
            var topics = _endUserService.GetTopicByClassIdAndSubjectId(classId, subjectId);
            if (topics.Count() > 0)
            {
                return Ok(topics);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("unit/{unitId:int}")]
        public IActionResult GetUnit(int unitId)
        {
            var unit = _endUserService.GetUnit(unitId);
            if (unit != null)
            {
                return Ok(unit);
            }
            return NotFound();
        }
        [Authorize]
        [HttpGet]
        [Route("unit/{unitId:int}/randomQuestion/")]
        public IActionResult GetRandomeQuestion(int unitId)
        {
            var question = _endUserService.GetRandomQuestion(unitId);
            if (question != null)
            {
                return Ok(question);
            }
            return NotFound();
        }

        [Authorize]
        [HttpPost]
        [Route("submitAnswer")]
        public IActionResult SubmitAnswer([FromBody] QuestionDTO model)
        {            
            var question = _endUserService.SubmitAnswer(model);
            if (question != null)
            {
                return Ok(question);
            }
            return NotFound();
        }

        [Authorize]
        [HttpGet]
        [Route("userProfile")]
        public IActionResult GetUserProfile([FromQuery] int? subjectId)
        {
            var profile = _endUserService.GetUserProfile(subjectId);
            if(profile != null)
            {
                return Ok(profile);
            }
            return NotFound();
        }
    }
}