﻿using Microsoft.Extensions.Options;
using ST.Library.Common.EncryptedPassword;
using System;
using System.Collections.Concurrent;

namespace Thihay.Common.Securities
{
    public class AudiencesStore : IAudienceStore
    {
        private static ConcurrentDictionary<string, Audience> AudiencesList = new ConcurrentDictionary<string, Audience>();

        public AudiencesStore(string clientId, string secret)
        {
            if (!string.IsNullOrWhiteSpace(clientId) && !string.IsNullOrWhiteSpace(secret))
            {
                AudiencesList.TryAdd(clientId,
                                  new Audience
                                  {
                                      ClientId = clientId,
                                      Secret = secret,
                                      Name = "ThihayAuthenticationServer.Api.v1"
                                  });
            }
        }

        public Audience AddAudience(string name)
        {
            var clientId = Guid.NewGuid().ToString("N");
            var salt = HashEncrypted.GenerateSalt(32);
            var base64Secret = HashEncrypted.EncryptedHash(clientId, salt);
            Audience newAudience = new Audience { ClientId = clientId, Secret = base64Secret, Name = name };
            AudiencesList.TryAdd(clientId, newAudience);
            return newAudience;
        }

        public Audience FindAudience(string clientId)
        {
            if (AudiencesList.TryGetValue(clientId, out Audience audience))
            {
                return audience;
            }
            return null;
        }
    }
}