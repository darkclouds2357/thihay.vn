﻿namespace Thihay.Common.Securities
{
    public interface IAudienceStore
    {
        Audience AddAudience(string name);

        Audience FindAudience(string clientId);
    }
}