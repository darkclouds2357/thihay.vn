﻿namespace Thihay.Common.Securities
{
    public class Audience
    {
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public string Name { get; set; }
    }
}