﻿using Microsoft.IdentityModel.Tokens;
using System;
using Thihay.Common.Securities;

namespace Thihay.Common.TokenProvider
{
    public class Credential
    {
        private readonly IAudienceStore _audience;

        public Credential(IAudienceStore audienceStore, string clientId, string issuer)
        {
            _audience = audienceStore;
            ClientId = clientId;
            Issuer = issuer;
        }

        public string ClientId { get; private set; }
        public string Issuer { get; private set; }

        public SymmetricSecurityKey SecurityKey
        {
            get
            {
                Audience audience = _audience.FindAudience(ClientId);
                string symmetricKeyAsBase64 = audience.Secret;
                var keyByteArray = Convert.FromBase64String(symmetricKeyAsBase64);
                return new SymmetricSecurityKey(keyByteArray);
            }
        }

        ///// <summary>
        ///// The signing key to use when generating tokens.
        ///// </summary>
        public SigningCredentials SigningCredentials
        {
            get
            {
                return new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256Signature);
            }
        }
    }
}