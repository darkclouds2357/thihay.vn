﻿using ST.Library.Common;
using System;
using System.Security.Claims;

namespace Thihay.Common
{
    public interface IThihayUser : ICurrentUser
    {
        int UserId { get; set; }
        string GoogleId { get; set; }
        string FacebookId { get; set; }

        DateTime DateOfBirth { get; set; }
        string Phone { get; set; }
        string Address { get; set; }
        int? SchoolId { get; set; }

        string SchoolName { get; set; }
        int? VillageId { get; set; }
        string VillageName { get; set; }
        int? ProvinceId { get; set; }
        string ProvinceName { get; set; }        

        double UnixLoginTime { get; set; }

        int VipDayLeft { get; set; }

        DateTime CreatedDate { get; set; }

        void UpdateIdentity(ClaimsIdentity claimsIdentity);
    }
}