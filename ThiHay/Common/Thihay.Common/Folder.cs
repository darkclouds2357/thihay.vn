﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thihay.Common
{
    public class Folder
    {
        public string FolderName { get; set; }
        public string FullUrl { get; set; }

        public bool IsHasSubFolder => SubFolders != null && SubFolders.Count() > 0;
        public IEnumerable<Folder> SubFolders { get; set; }
    }

    public class ImgFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
    }
}
