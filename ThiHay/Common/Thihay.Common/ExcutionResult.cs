﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thihay.API.Client.Models
{
    public class ExecutionResult
    {
        public object Result { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
        public string[] Messages { get; set; }
    }

    public class ExecutionResult<T> : ExecutionResult
    {
        public new T Result { get; set; }
    }
}
