﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Common;
using System;
using System.Globalization;
using System.Security.Claims;

namespace Thihay.Common
{
    public class ThihayUser : IThihayUser
    {
        public ThihayUser()
        {
            IsSystemUser = true;
        }
        public ThihayUser(bool isSystemUser)
        {
            IsSystemUser = isSystemUser;
        }
        public int UserId { get; set; }
        public string GoogleId { get; set; }
        public string FacebookId { get; set; }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public int FrontEndGMTMinutes { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? SchoolId { get; set; }
        public string SchoolName { get; set; }
        public int? VillageId { get; set; }
        public string VillageName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsSystemUser { get; private set; }
        public double UnixLoginTime { get; set; }
        public int VipDayLeft { get; set; }
        public DateTime CreatedDate { get; set; }

        public void CommitHistory(DbContext dbContext)
        {
        }

        public void CommitHistory()
        {
        }

        public void Init(Action<ICurrentUser> acquire)
        {
        }

        public bool IsEndpointAuthorized(string endpoint, string method)
        {
            throw new NotImplementedException();
        }

        public void KeepHistory(object entity, bool saveInBaseRepository = false)
        {
        }

        public void KeepHistory(object entity, string tableName, bool saveInBaseRepository = false)
        {
        }

        public void UpdateIdentity(ClaimsIdentity claimsIdentity)
        {
            if (claimsIdentity != null && claimsIdentity.IsAuthenticated)
            {
                int.TryParse(claimsIdentity.FindFirst(ClaimTypes.PrimarySid).Value, out int userId);
                UserId = userId;

                CreatedDate = DateTime.ParseExact(claimsIdentity.FindFirst("CREATE_DATE").Value, "dd MMM yyyy", CultureInfo.InvariantCulture);
                FacebookId = claimsIdentity.FindFirst("FACEBOOK_ID").Value;
                UserName = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
                FullName = claimsIdentity.FindFirst(ClaimTypes.Name).Value;
                FirstName = claimsIdentity.FindFirst(ClaimTypes.GivenName).Value;
                LastName = claimsIdentity.FindFirst(ClaimTypes.Surname).Value;
                Email = claimsIdentity.FindFirst(ClaimTypes.Email).Value;
                Phone = claimsIdentity.FindFirst(ClaimTypes.MobilePhone).Value;
                Address = claimsIdentity.FindFirst(ClaimTypes.StreetAddress).Value;
                DateOfBirth = DateTime.ParseExact(claimsIdentity.FindFirst(ClaimTypes.DateOfBirth).Value, "dd MMM yyyy", CultureInfo.InvariantCulture);
                SchoolId = !string.IsNullOrWhiteSpace(claimsIdentity.FindFirst("SCHOOL_ID")?.Value) ? int.Parse(claimsIdentity.FindFirst("SCHOOL_ID")?.Value) : default(int?);
                SchoolName = claimsIdentity.FindFirst("SCHOOL_NAME").Value;
                VillageId = !string.IsNullOrWhiteSpace(claimsIdentity.FindFirst("VILLAGE_ID")?.Value) ? int.Parse(claimsIdentity.FindFirst("VILLAGE_ID")?.Value) : default(int?);
                VillageName = claimsIdentity.FindFirst("VILLAGE_NAME").Value;
                ProvinceId = !string.IsNullOrWhiteSpace(claimsIdentity.FindFirst("PPROVINCE_ID")?.Value) ? int.Parse(claimsIdentity.FindFirst("PPROVINCE_ID")?.Value) : default(int?);
                ProvinceName = claimsIdentity.FindFirst(ClaimTypes.StateOrProvince).Value;
                IsSystemUser = bool.Parse(claimsIdentity.FindFirst("IS_SYSTEM_USER").Value);
                VipDayLeft = !string.IsNullOrWhiteSpace(claimsIdentity.FindFirst("VIP_DAY_LEFT")?.Value) ? int.Parse(claimsIdentity.FindFirst("VIP_DAY_LEFT")?.Value) : 0;
                UnixLoginTime = !string.IsNullOrWhiteSpace(claimsIdentity.FindFirst("LOGIN_TIME_UNIX_TIMESTAMPS")?.Value) ? double.Parse(claimsIdentity.FindFirst("LOGIN_TIME_UNIX_TIMESTAMPS")?.Value) : 0;
            }
        }
    }
}