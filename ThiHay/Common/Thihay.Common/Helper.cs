﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Thihay.Common
{
    public static class Helper
    {
        private static Random rnd = new Random();
        public static IList<T> Shuffle<T>(IList<T> input)
        {
            //var rnd = new Random();
            int m = input.Count, i;
            T t;
            while (1 == 1)
            {
                m--;
                if (m < 0) break;
                i = rnd.Next(m);
                t = input[m];
                input[m] = input[i];
                input[i] = t;
            }
            return input;
        }

        public static string EncryptMd5(string input)
        {
            byte[] encryptByte = new byte[16];
            encryptByte = MD5.Create().ComputeHash(Encoding.Unicode.GetBytes(input));
            string encryptString = string.Empty;
            for (int i = 0; i < 16; i++)
            {
                encryptString += encryptByte[i].ToString("x2");
            }
            return encryptString;
        }

        public static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        public static JToken GetDirectory(DirectoryInfo directory, int readLevel = 0)
        {
            if (readLevel <= 2)
            {
                readLevel++;
                var dirJson = directory.EnumerateDirectories()
                    .ToDictionary(x => x.Name, x => GetDirectory(x, readLevel));
                return JToken.FromObject(new
                {
                    directory = dirJson,
                });
            }
            return null;
        }

        public static IEnumerable<Folder> GetDirectoryFolder(DirectoryInfo directory, string removePath)
        {
            var subDirectories = directory.EnumerateDirectories();
            if(subDirectories.Count() > 0)
            {
                var folders = subDirectories.Select(f => new Folder
                {
                    FolderName = f.Name,
                    FullUrl = f.FullName.Replace(removePath, ""),
                    SubFolders = GetDirectoryFolder(f, removePath)
                });
                return folders;
            }
            return null;
        }
    }
}