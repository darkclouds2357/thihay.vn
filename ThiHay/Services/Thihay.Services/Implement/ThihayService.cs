﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Thihay.Common;
using Thihay.Domain;
using Thihay.Domain.Entities;
using Thihay.Domain.Enum;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace Thihay.Services.Implement
{
    public class ThihayService : IThihayService, IDependency
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IThihayUser _currentUser;
        private readonly IQuestionManager _questionManager;
        private readonly ThihayContext _context;

        public ThihayService(IUnitOfWork unitOfWork, IThihayUser currentUser, IQuestionManager questionManager, ThihayContext context)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
            _questionManager = questionManager;
            _context = context;
        }

        #region Class Manager

        public IEnumerable<ClassDTO> GetClass(string searchKey)
        {
            var clases = _unitOfWork.GetRepository<Class>().GetAll(true);
            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                clases = clases.Where(p => p.ClassName.Contains(searchKey));
            }
            return clases.AsEnumerable().Select(c => new ClassDTO
            {
                Id = c.ClassId,
                ClassName = c.ClassName,
                IsDeleted = c.IsDeleted,
                Topics = c.Topics.Select(t => new TopicDTO
                {
                    Id = t.TopicId,
                    TopicName = t.NameTopic
                }).AsEnumerable()
            });
        }

        //public ClassDTO GetClassDetail(int classId)
        //{
        //    var @class = _unitOfWork.GetRepository<Class>().Find(classId);
        //    if (@class != null)
        //    {
        //        return new ClassDTO
        //        {
        //            Id = @class.ClassId,
        //            ClassName = @class.ClassName,
        //            IsDeleted = @class.IsDeleted,
        //            Topics = @class.Topics.Select(t => new TopicDTO
        //            {
        //                Id = t.TopicId,
        //                TopicName = t.NameTopic
        //            }).ToList()
        //        };
        //    }
        //    return null;
        //}
        public async Task<bool> ChangeClassStatus(int classId, bool isDelete)
        {
            try
            {
                var classRepository = _unitOfWork.GetRepository<Class>();
                var @class = classRepository.Find(classId);
                if (@class != null && @class.IsDeleted != isDelete)
                {
                    @class.IsDeleted = isDelete;
                    classRepository.Update(@class);
                    var result = await _unitOfWork.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateClass(ClassDTO @class)
        {
            try
            {
                if (@class != null)
                {
                    var classRepository = _unitOfWork.GetRepository<Class>();
                    var existedClass = classRepository.Find(@class.Id);

                    if (existedClass != null)
                    {
                        existedClass.ClassName = @class.ClassName;
                        classRepository.Update(existedClass);
                    }
                    else
                    {
                        await classRepository.InsertAsync(new Class
                        {
                            ClassName = @class.ClassName
                        });
                    }
                    var result = await _unitOfWork.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Class Manager

        #region Subject Manager

        public IEnumerable<SubjectDTO> GetSubject(string searchKey)
        {
            var subjects = _unitOfWork.GetRepository<Subject>().GetAll(true);
            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                subjects = subjects.Where(p => p.SubjectName.Contains(searchKey));
            }
            return subjects.AsEnumerable().Select(c => new SubjectDTO
            {
                Id = c.SubjectId,
                SubjectName = c.SubjectName,
                IsDeleted = c.IsDeleted,
                Topics = c.Topics.Select(t => new TopicDTO
                {
                    Id = t.TopicId,
                    TopicName = t.NameTopic
                }).AsEnumerable()
            });
        }

        public async Task<bool> ChangeSubjectStatus(int subjectId, bool isDelete)
        {
            try
            {
                var subjectRepository = _unitOfWork.GetRepository<Subject>();
                var subject = subjectRepository.Find(subjectId);
                if (subject != null && subject.IsDeleted != isDelete)
                {
                    subject.IsDeleted = isDelete;
                    subjectRepository.Update(subject);
                    var result = await _unitOfWork.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateSubject(SubjectDTO subject)
        {
            try
            {
                if (subject != null)
                {
                    var subjectRepository = _unitOfWork.GetRepository<Subject>();
                    var existedSubject = subjectRepository.Find(subject.Id);

                    if (existedSubject != null)
                    {
                        existedSubject.SubjectName = subject.SubjectName;
                        subjectRepository.Update(existedSubject);
                    }
                    else
                    {
                        await subjectRepository.InsertAsync(new Subject
                        {
                            SubjectName = subject.SubjectName
                        });
                    }
                    var result = await _unitOfWork.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Subject Manager

        #region Topic

        public IEnumerable<TopicDTO> GetTopics(string searchKey, int? subjectId, int? classId, int? page, int? pageSize, out int totalPage)
        {
            var topicRepository = _unitOfWork.GetRepository<Topic>();
            var topics = topicRepository.GetAll(true).Include(nameof(Topic.Subject)).Include(nameof(Topic.Class)).Where(t => !t.Class.IsDeleted && !t.Subject.IsDeleted);
            if (subjectId.HasValue && classId.HasValue)
            {
                topics = topics.Where(t => t.SubjectId == subjectId.Value && t.ClassId == classId.Value);
            }
            else if (classId.HasValue && !subjectId.HasValue)
            {
                topics = topics.Where(t => t.ClassId == classId.Value);
            }
            else if (!classId.HasValue && subjectId.HasValue)
            {
                topics = topics.Where(t => t.SubjectId == subjectId.Value);
            }
            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                topics = topics.Where(t => t.NameTopic.Contains(searchKey));
            }

            var totalRecord = topics.Count();

            if (page != null && pageSize != null)
            {
                topics = topics.Skip(((int)page - 1) * (int)pageSize);
            }
            if (pageSize != null)
            {
                totalPage = (int)Math.Ceiling(1.0 * totalRecord / pageSize.Value);
                topics = topics.Take((int)pageSize);
            }
            else
            {
                totalPage = 0;
            }
            var result = topics.ToList().Select(s => new TopicDTO
            {
                Id = s.TopicId,
                TopicName = s.NameTopic,
                ClassId = s.ClassId,
                SubjectId = s.SubjectId,
                SubjectName = s.Subject?.SubjectName ?? s.SubjectName,
                ClassName = s.Class?.ClassName,
                DescriptionTopic = s.DescriptionTopic,
                IsDelete = s.IsDeleted
            });
            return result;
        }

        public async Task<bool> UpdateTopic(TopicDTO topic)
        {
            try
            {
                if (topic != null)
                {
                    var topicRepository = _unitOfWork.GetRepository<Topic>();

                    var @class = _unitOfWork.GetRepository<Class>().Find(topic.ClassId);

                    var subject = _unitOfWork.GetRepository<Subject>().Find(topic.SubjectId);

                    if (@class != null && subject != null)
                    {
                        var existedTopic = topicRepository.Find(topic.Id);

                        if (existedTopic != null)
                        {
                            existedTopic.NameTopic = topic.TopicName;
                            existedTopic.SubjectId = topic.SubjectId;
                            existedTopic.ClassId = topic.ClassId;
                            existedTopic.SubjectName = subject.SubjectName;
                            existedTopic.DescriptionTopic = topic.DescriptionTopic;

                            topicRepository.Update(existedTopic);
                        }
                        else
                        {
                            var newTopic = new Topic
                            {
                                SubjectId = topic.SubjectId,
                                SubjectName = subject.SubjectName,
                                NameTopic = topic.TopicName,
                                DescriptionTopic = topic.DescriptionTopic,
                                ClassId = topic.ClassId
                            };
                            await topicRepository.InsertAsync(newTopic);
                        }
                        var result = await _unitOfWork.SaveChangesAsync();
                        return result > 0;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeTopicStatus(int topicId, bool isDelete)
        {
            try
            {
                var topic = _unitOfWork.GetRepository<Topic>().Find(topicId);
                if (topic != null && topic.IsDeleted != isDelete)
                {
                    topic.IsDeleted = isDelete;
                    _unitOfWork.GetRepository<Topic>().Update(topic);
                    return await _unitOfWork.SaveChangesAsync() > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public TopicDTO GetTopicDetail(int topicId)
        {
            var topicRepository = _unitOfWork.GetRepository<Topic>();
            var topic = topicRepository.GetAll(true).Include(nameof(Topic.Class)).Include(nameof(Topic.Subject)).Where(t => t.TopicId == topicId).FirstOrDefault();
            if (topic != null)
            {
                var units = this.GetUnits(searchKey: null, subjectId: topic.SubjectId, classId: topic.ClassId, topicId: topic.TopicId, page: null, pageSize: null, totalPage: out int totalPage);
                var result = new TopicDTO
                {
                    Id = topic.TopicId,
                    TopicName = topic.NameTopic,
                    ClassId = topic.ClassId,
                    SubjectId = topic.SubjectId,
                    SubjectName = topic.Subject?.SubjectName ?? topic.SubjectName,
                    ClassName = topic.Class?.ClassName,
                    DescriptionTopic = topic.DescriptionTopic,
                    Units = units,
                    IsDelete = topic.IsDeleted
                };
                return result;
            }
            return null;
        }

        #endregion Topic

        #region Unit

        public IEnumerable<UnitDTO> GetUnits(string searchKey, int? subjectId, int? classId, int? topicId, int? page, int? pageSize, out int totalPage)
        {
            var unitRepository = _unitOfWork.GetRepository<Unit>();
            var units = unitRepository.GetAll(true).Include(nameof(Unit.Topic));

            if (topicId.HasValue)
            {
                units = units.Where(t => t.TopicId == topicId.Value);
            }
            if (subjectId.HasValue && classId.HasValue)
            {
                units = units.Where(t => t.Topic != null && t.Topic.SubjectId == subjectId.Value && t.Topic.ClassId == classId.Value);
            }
            else if (classId.HasValue && !subjectId.HasValue)
            {
                units = units.Where(t => t.Topic != null && t.Topic.ClassId == classId.Value);
            }
            else if (!classId.HasValue && subjectId.HasValue)
            {
                units = units.Where(t => t.Topic != null && t.Topic.SubjectId == subjectId.Value);
            }

            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                units = units.Where(t => t.NameUnit.Contains(searchKey));
            }

            var totalRecord = units.Count();

            if (page != null && pageSize != null)
            {
                units = units.Skip(((int)page - 1) * (int)pageSize);
            }
            if (pageSize != null)
            {
                totalPage = (int)Math.Ceiling(1.0 * totalRecord / pageSize.Value);
                units = units.Take((int)pageSize);
            }
            else
            {
                totalPage = 0;
            }
            var result = units.ToList().Select(s => new UnitDTO
            {
                Id = s.UnitId,
                TopicName = s.NameTopic,
                SortOrder = s.SortOrder ?? 0,
                QuestionCount = s.QuestionIds.Count(),
                NameUnit = s.NameUnit,
                TopicId = s.TopicId,
                IsDeleted = s.IsDeleted,
                DescriptionUnit = s.DescriptionUnit
            });
            return result.OrderBy(u => u.TopicId).ThenBy(u => u.SortOrder);
        }

        public UnitDTO GetUnitDetail(int unitId)
        {
            var unit = _unitOfWork.GetRepository<Unit>().Find(unitId);
            if (unit != null)
            {
                var topic = _unitOfWork.GetRepository<Topic>().Find(unit.TopicId);
                return new UnitDTO
                {
                    Id = unit.UnitId,
                    IsDeleted = unit.IsDeleted,
                    NameUnit = unit.NameUnit,
                    QuestionCount = unit.QuestionIds.Count(),
                    SortOrder = unit.SortOrder ?? 0,
                    DescriptionUnit = unit.DescriptionUnit,
                    TopicId = unit.TopicId,
                    TopicName = topic?.NameTopic ?? unit.NameTopic,
                    Topic = new TopicDTO
                    {
                        ClassId = topic?.ClassId,
                        SubjectId = topic?.SubjectId
                    }
                };
            }
            return null;
        }

        public async Task<bool> UpdateUnit(UnitDTO unit)
        {
            try
            {
                var currentUnit = _unitOfWork.GetRepository<Unit>().Find(unit.Id);
                var currentTopic = _unitOfWork.GetRepository<Topic>().Find(unit.TopicId);
                if (currentUnit != null && currentTopic != null)
                {
                    currentUnit.TopicId = unit.TopicId;
                    currentUnit.ClassId = currentTopic.ClassId;
                    currentUnit.CreatedUserId = _currentUser.UserId;
                    currentUnit.DescriptionUnit = unit.DescriptionUnit;
                    currentUnit.NameTopic = currentTopic.NameTopic;
                    currentUnit.SubjectId = currentTopic.SubjectId;
                    _unitOfWork.GetRepository<Unit>().Update(currentUnit);
                    return await _unitOfWork.SaveChangesAsync() > 0;
                }
                else if (currentUnit == null && currentTopic != null)
                {
                    currentUnit = new Unit
                    {
                        NameUnit = unit.NameUnit,
                        DescriptionUnit = unit.DescriptionUnit,
                        NameTopic = currentTopic.NameTopic,
                        SubjectId = currentTopic.SubjectId,
                        TopicId = currentTopic.TopicId,
                        ClassId = currentTopic.ClassId
                    };
                    await _unitOfWork.GetRepository<Unit>().InsertAsync(currentUnit);
                    return await _unitOfWork.SaveChangesAsync() > 0;
                    /// Insert new Unit here
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateUnitOrder(int unitId, int newOrder)
        {
            try
            {
                var unitRepository = _unitOfWork.GetRepository<Unit>();

                var unit = unitRepository.GetAll(u => u.UnitId == unitId).Select(u => new
                {
                    TopicId = u.TopicId,
                    OldOrder = u.SortOrder
                }).FirstOrDefault();

                if (unit != null && unit.TopicId.HasValue && unit.OldOrder != newOrder)
                {
                    var unitsInTopic = unitRepository.GetAll(u => unit.TopicId.Value == u.TopicId && u.UnitId != unitId);

                    if (unit.OldOrder < newOrder)
                    {
                        var increaseOrder = unitsInTopic.Where(u => u.SortOrder > unit.OldOrder && u.SortOrder <= newOrder).ToList();
                        foreach (var item in increaseOrder)
                        {
                            item.SortOrder++;
                        }
                        unitRepository.Update(increaseOrder);
                    }
                    else
                    {
                        var decreaseOrder = unitsInTopic.Where(u => u.SortOrder < unit.OldOrder && u.SortOrder >= newOrder).ToList();
                        foreach (var item in decreaseOrder)
                        {
                            item.SortOrder--;
                        }
                        unitRepository.Update(decreaseOrder);
                    }
                    return await _unitOfWork.SaveChangesAsync() > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeUnitStatus(int unitId, bool isDelete)
        {
            try
            {
                var unit = _unitOfWork.GetRepository<Unit>().Find(unitId);
                if (unit != null && unit.IsDeleted != isDelete)
                {
                    unit.IsDeleted = isDelete;
                    _unitOfWork.GetRepository<Unit>().Update(unit);
                    return await _unitOfWork.SaveChangesAsync() > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Unit

        #region Question And Answer Manager

        public async Task<bool> ChangeQuestionStatus(int questionId, bool isDelete)
        {
            try
            {
                var questeionRepository = _unitOfWork.GetRepository<Question>();
                var question = questeionRepository.Find(questionId);
                if (question != null && question.IsDeleted != isDelete)
                {
                    question.IsDeleted = isDelete;
                    questeionRepository.Update(question);
                    var result = await _unitOfWork.SaveChangesAsync();
                    return result > 0;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public QuestionDTO GetQuestionDetail(int questionId)
        {
            var question = _unitOfWork.GetRepository<Question>().GetAll(true).Include(nameof(Question.Unit))
            .Include(nameof(Question.QuestionDetails))
            .Include("QuestionDetails.AnswerContents")
            .Include("QuestionDetails.AnswerIndexs").FirstOrDefault(q => q.QuestionId == questionId);
            if (question != null)
            {
                var result = _questionManager.ConvertToQuestionDTO(question);
                result.ClassId = question.Unit?.ClassId;
                result.SubjectId = question.Unit?.SubjectId;
                result.TopicId = question.Unit?.TopicId;
                return result;
            }
            return null;
        }

        public IEnumerable<QuestionDTO> GetAllQuestion(string searchKey, int? subjectId, int? classId, int? topicId, int? unitId, QuestionType? questionType, out int totalPage, int? page = 1, int? pageSize = 10)
        {
            var questionRepository = _unitOfWork.GetRepository<Question>();
            var questions = questionRepository.GetAll(true)
            .Include(q => q.Unit)
            .Include(q => q.QuestionDetails)
            .ThenInclude(qd => qd.AnswerContents)
            .Include(q => q.QuestionDetails)
            .ThenInclude(qd => qd.AnswerIndexs).AsQueryable();

            var units = _unitOfWork.GetRepository<Unit>().GetAll();
            var topics = _unitOfWork.GetRepository<Topic>().GetAll();
            //var subjects = _unitOfWork.GetRepository<Subject>().GetAll();
            //var classes = _unitOfWork.GetRepository<Class>().GetAll();

            //var classTopics = topics.Join(classes, t => t.ClassId, c => c.ClassId, (t, c) => new { ClassId = t.ClassId, SubjectId = t.SubjectId, TopicId = t.TopicId });
            //var classSubjectTopics = classTopics.Join(subjects, c => c.SubjectId, s => s.SubjectId, (c, s) => new { ClassId = c.ClassId, TopicId = c.TopicId, SubjectId = s.SubjectId });

            var unitIds = units.Join(topics, u => u.TopicId, c => c.TopicId, (u, c) => new
            {
                ClassId = c.ClassId,
                SubjectId = c.SubjectId,
                TopicId = c.TopicId,
                UnitId = u.UnitId
            });

            unitIds = subjectId.HasValue ? unitIds.Where(u => u.SubjectId == subjectId.Value) : unitIds;
            unitIds = classId.HasValue ? unitIds.Where(u => u.ClassId == classId.Value) : unitIds;
            unitIds = topicId.HasValue ? unitIds.Where(u => u.TopicId == topicId.Value) : unitIds;
            unitIds = unitId.HasValue ? unitIds.Where(u => u.UnitId == unitId.Value) : unitIds;
            if (subjectId.HasValue || classId.HasValue || topicId.HasValue || unitId.HasValue)
            {
                var ids = unitIds.Select(u => u.UnitId).ToList();
                questions = questions.Where(u => u.UnitId.HasValue).Where(q => ids.Contains(q.UnitId.Value));
            }


            //questions = subjectId.HasValue ? questions.Where(q => q.Unit.SubjectId == subjectId.Value) : questions;

            //questions = classId.HasValue ? questions.Where(q => q.Unit.ClassId == classId.Value) : questions;

            //questions = topicId.HasValue ? questions.Where(q => q.Unit.TopicId == topicId.Value) : questions;

            questions = questionType.HasValue ? questions.Where(q => q.Type == questionType.Value) : questions;

            //questions = unitId.HasValue ? questions.Where(q => q.UnitId == unitId.Value) : questions;

            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                var contentSearch = questions.Where(q => q.Content.Contains(searchKey));

                var questionDetailSearch = questions.SelectMany(q => q.QuestionDetails).Where(q => q.QuestionContent.Contains(searchKey)).Select(q => q.Question);

                questions = contentSearch.Union(questionDetailSearch).Distinct();
            }
            //isLast = true;
            questions = questions.OrderByDescending(q => q.QuestionId);
            totalPage = 0;
            var totalRecord = questions.Count();
            if (page != null && pageSize != null)
            {
                if (page.Value == 0)
                {
                    page = 1;
                }
                questions = questions.Skip(((int)page - 1) * (int)pageSize);
            }
            if (pageSize != null)
            {
                totalPage = (totalRecord / pageSize.Value) + 1;
                totalPage = totalRecord == pageSize.Value ? 1 : totalPage;
                questions = questions.Take((int)pageSize);
                //isLast = totalRecord < pageSize.Value;
            }
            var questionResult = questions.ToList();
            var result = new List<QuestionDTO>();
            foreach (var item in questionResult)
            {
                var questionDTO = _questionManager.ConvertToQuestionDTO(item);
                questionDTO.ClassId = item.Unit?.ClassId;
                questionDTO.SubjectId = item.Unit?.SubjectId;
                questionDTO.TopicId = item.Unit?.TopicId;
                questionDTO.IsDeleted = item.IsDeleted;
                result.Add(questionDTO);

            }
            return result;
        }

        private List<QuestionDetail> ConvertUpdateDragAndDropQuestion(string content, List<AnswerDTO> answersList)
        {
            var questionDetailList = new List<QuestionDetail>();
            var regex = "{[1-9]}";
            var matches = Regex.Matches(content, regex);
            var questionMatches = new List<KeyValuePair<int, string>>();
            foreach (Match match in matches)
            {
                var valueNumber = Regex.Match(match.Value, "[1-9]")?.Value;
                if (int.TryParse(valueNumber, out int index))
                {
                    questionMatches.Add(new KeyValuePair<int, string>(index, match.Value)
                   );
                }
            }
            int order = 0;
            foreach (var question in questionMatches)
            {
                if (question.Key <= answersList.Count)
                {
                    var answerIndex = question.Key;
                    var questionDetailId = Guid.NewGuid();
                    var questionContent = question.Value;


                    questionDetailList.Add(new QuestionDetail
                    {
                        Id = questionDetailId,
                        QuestionContent = question.Value,
                        Order = order,
                        AnswerIndexs = new AnswerIndex[]
                        {
                            new AnswerIndex{ QuestionDetailId = questionDetailId, Index = answerIndex }
                        }
                    });
                    order++;
                }
            }
            questionDetailList[0].AnswerContents = answersList.Select(a => new AnswerContent
            {
                Content = a.AnswerContent,
                Index = a.AnswerIndex + 1,
                Id = Guid.NewGuid()
            }).ToList();
            return questionDetailList;
        }

        public async Task<bool> UpdateQuestion(QuestionDTO questionDto)
        {
            try
            {
                var questionRepository = _unitOfWork.GetRepository<Question>();
                var currentQuestion = questionRepository.GetAll()
                    .Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerContents)
                    .Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerIndexs)
                    .FirstOrDefault(q => q.QuestionId == questionDto.Id);

                var questionDetail = questionDto.QuestionDetail;


                if (currentQuestion != null)
                {
                    var questionDetails = currentQuestion.QuestionDetails;
                    var answerContents = questionDetails.SelectMany(a => a.AnswerContents);
                    var answerIndexs = questionDetails.SelectMany(a => a.AnswerIndexs);
                    _unitOfWork.GetRepository<QuestionDetail>().HardDelete(questionDetails);
                    _unitOfWork.GetRepository<AnswerContent>().HardDelete(answerContents);
                    _unitOfWork.GetRepository<AnswerIndex>().HardDelete(answerIndexs);


                    //_unitOfWork.SaveChanges();
                    currentQuestion.Type = questionDto.QuestionType;
                    currentQuestion.UnitId = questionDto.UnitId;
                    currentQuestion.Explanation = questionDto.QuestionExplanation;
                    currentQuestion.Content = questionDto.QuestionContent;
                    //currentQuestion.QuestionDetails = 

                    questionRepository.Update(currentQuestion);

                    var newQuestionDetails = questionDto.QuestionType != QuestionType.DragAndDrop ? questionDetail.Select(q =>
                    {
                        var detailId = Guid.NewGuid();
                        var detailQuestion = q.QuestionDetailContent;

                        var newAnswerContents = new List<AnswerContent>();
                        var newAnswerIndexs = new List<AnswerIndex>();
                        if (questionDto.QuestionType == QuestionType.Arrange)
                        {
                            newAnswerContents.Add(new AnswerContent
                            {
                                Id = Guid.NewGuid(),
                                Content = q.QuestionDetailContent,
                                Index = q.Order + 1,
                                QuestionDetailId = detailId
                            });
                        }
                        else
                        {
                            var answersDTO = q.Answers.ToList();
                            for (var i = 0; i < answersDTO.Count; i++)
                            {
                                newAnswerContents.Add(new AnswerContent
                                {
                                    Id = Guid.NewGuid(),
                                    Content = answersDTO[i].AnswerContent,
                                    Index = i,
                                    QuestionDetailId = detailId,
                                });
                                if (answersDTO[i]?.IsCorrectAnswer == true)
                                {
                                    newAnswerIndexs.Add(new AnswerIndex
                                    {
                                        Id = Guid.NewGuid(),
                                        Index = i,
                                        QuestionDetailId = detailId
                                    });
                                }
                            }
                        }
                        return new QuestionDetail
                        {
                            Id = detailId,
                            QuestionId = currentQuestion.QuestionId,
                            AnswerContents = newAnswerContents.ToList(),
                            AnswerIndexs = newAnswerIndexs.ToList(),
                            QuestionContent = detailQuestion,
                            Order = q.Order
                        };
                    }).ToList() : ConvertUpdateDragAndDropQuestion(questionDto.QuestionContent, questionDetail.FirstOrDefault().Answers.ToList());
                    _unitOfWork.GetRepository<QuestionDetail>().Insert(newQuestionDetails);
                    //var newAnswerIndexs = newQuestionDetails.SelectMany(q => q.AnswerIndexs).ToList();
                    //var newAnswerContents = newQuestionDetails.SelectMany(q => q.AnswerContents).ToList();
                }
                else
                {
                    var question = new Question()
                    {
                        Explanation = questionDto.QuestionExplanation,
                        Content = questionDto.QuestionContent,
                        Type = questionDto.QuestionType,
                        UnitId = questionDto.UnitId,
                        QuestionDetails = questionDto.QuestionType != QuestionType.DragAndDrop ? questionDetail.Select(q =>
                        {
                            var detailQuestion = q.QuestionDetailContent;
                            var newAnswerContents = new List<AnswerContent>();
                            var newAnswerIndexs = new List<AnswerIndex>();
                            if (questionDto.QuestionType == QuestionType.Arrange)
                            {
                                newAnswerContents.Add(new AnswerContent
                                {
                                    Id = Guid.NewGuid(),
                                    Content = q.QuestionDetailContent,
                                    Index = q.Order + 1
                                });
                            }
                            else
                            {
                                var answersDTO = q.Answers.ToList();
                                for (var i = 0; i < answersDTO.Count; i++)
                                {
                                    newAnswerContents.Add(new AnswerContent
                                    {
                                        Content = answersDTO[i].AnswerContent,
                                        Index = i
                                    });
                                    if (answersDTO[i]?.IsCorrectAnswer == true)
                                    {
                                        newAnswerIndexs.Add(new AnswerIndex
                                        {
                                            Index = i
                                        });
                                    }
                                }
                            }
                            return new QuestionDetail
                            {
                                AnswerContents = newAnswerContents.ToList(),
                                AnswerIndexs = newAnswerIndexs.ToList(),
                                QuestionContent = detailQuestion,
                                Order = q.Order,
                            };
                        }).ToList() : ConvertUpdateDragAndDropQuestion(questionDto.QuestionContent, questionDetail.FirstOrDefault().Answers.ToList())
                    };
                    await questionRepository.InsertAsync(question);
                }
                return await _unitOfWork.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion Question And Answer Manager
    }
}