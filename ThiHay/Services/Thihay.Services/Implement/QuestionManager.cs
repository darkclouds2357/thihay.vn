﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Thihay.Common;
using Thihay.Domain.Entities;
using Thihay.Domain.Enum;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;

namespace Thihay.Services.Implement
{
    public class QuestionManager : IQuestionManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IThihayUser _currentUser;
        private readonly IOptionsSnapshot<AppSettings> _settings;

        public QuestionManager(IUnitOfWork unitOfWork, IThihayUser currentUser, IOptionsSnapshot<AppSettings> settings)
        {
            this._unitOfWork = unitOfWork;
            this._currentUser = currentUser;
            this._settings = settings;
        }

        public IEnumerable<AnswerDTO> GetAnswerDTO(QuestionType questionType, QuestionDetail questionDetail)
        {
            var answerContents = questionDetail.AnswerContents;
            var answerIndexs = questionDetail.AnswerIndexs;
            foreach (var answer in answerContents)
            {
                yield return new AnswerDTO
                {
                    AnswerContent = answer.Content.Replace("<img alt=\"\" src=\"", "<img alt=\"\" src=\"" + _settings.Value.ImageServer),
                    AnswerIndex = answer.Index,
                    IsCorrectAnswer = answerIndexs.Any(i => i.Index == answer.Index),
                    AnswerId = answer.Id
                };
            }
        }

        public async Task<bool> AddQuestionDetail(IEnumerable<QuestionDetail> questionDetail)
        {
            var questionDetailRepository = _unitOfWork.GetRepository<QuestionDetail>();

            foreach (var question in questionDetail)
            {
                var exsitedQuestionDetail = questionDetailRepository.GetAll(q => q.QuestionId == question.QuestionId).Any(q => q.QuestionContent == question.QuestionContent);
                if (!exsitedQuestionDetail)
                {
                    await questionDetailRepository.InsertAsync(question);
                }
            }
            return await _unitOfWork.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// Convert from the Bullshit QuestionJson struct to an Human Understandable
        /// </summary>
        /// <param name="questionType"></param>
        /// <returns></returns>
        public IEnumerable<QuestionDetail> ConvertQuestionJson(QuestionType questionType, QuestionJson questionJson, int questionId)
        {
            switch (questionType)
            {
                case QuestionType.MultipleChoice:
                    return ConvertMultipleChoice(questionJson, questionId);

                case QuestionType.ReadingComprehension:
                    return ConvertReadingComprehension(questionJson, questionId);

                case QuestionType.FillInTheBlank:
                    return ConvertFillInTheBlank(questionJson, questionId);

                case QuestionType.RewriteTheSentences:
                    return ConvertRewriteTheSentences(questionJson, questionId);

                case QuestionType.ClozeTest:
                    return ConvertClozeTest(questionJson, questionId);

                case QuestionType.Matching:
                    return ConvertMatching(questionJson, questionId);

                case QuestionType.Arrange:
                    return ConvertArrange(questionJson, questionId);

                case QuestionType.DragAndDrop:
                    return ConvertDragAndDrop(questionJson, questionId);

                case QuestionType.Groups:
                    return ConvertGroups(questionJson, questionId);

                case QuestionType.Underline:
                    return ConvertUnderline(questionJson, questionId);

                case QuestionType.UnderlineAndRewrite:
                    return ConvertUnderlineAndRewrite(questionJson, questionId);

                case QuestionType.DeleteCharacter:
                    return ConvertDeleteCharacter(questionJson, questionId);

                default:
                    break;
            }
            return null;
        }

        private IEnumerable<QuestionDetail> ConvertDeleteCharacter(QuestionJson questionJson, int questionId)
        {
            return ConvertUnderlineAndRewrite(questionJson, questionId);
        }

        private IEnumerable<QuestionDetail> ConvertUnderlineAndRewrite(QuestionJson questionJson, int questionId)
        {
            var regex = "{.+?}";
            var matches = Regex.Matches(questionJson.sContent, regex);
            var questionMatches = new List<string>();
            foreach (Match match in matches)
            {
                var question = match.Value.Replace("{", "").Replace("}", "");
                questionMatches.Add(question);
            }
            int questionCnt = questionMatches.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var question = questionMatches[i];
                //var questionContents = question.Split(' ').Select(e => { return e.Trim().ToLower(); });
                if (i < questionJson.sAnswer.Length)
                {
                    var answer = questionJson.sAnswer[i];
                    // var answerContents = answer.Split(' ').Select(e => { return e.Trim().ToLower(); });
                    yield return new QuestionDetail
                    {
                        QuestionContent = question,
                        QuestionId = questionId,
                        AnswerContents = new List<AnswerContent> { new AnswerContent { Content = answer.Replace("\n", "") } }
                    };
                }
            }
        }

        private IEnumerable<QuestionDetail> ConvertUnderline(QuestionJson questionJson, int questionId)
        {
            var regex = "{.+?}";
            var matches = Regex.Matches(questionJson.sContent, regex);
            var questionMatches = new List<string>();
            foreach (Match match in matches)
            {
                var question = match.Value.Replace("{", "").Replace("}", "");
                questionMatches.Add(question);
            }
            int questionCnt = questionMatches.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContent = questionMatches[i];
                var answerContents = questionContent.Split(' ');
                if (i < questionJson.sAnswer.Length)
                {
                    var resultAnswers = new List<AnswerContent>();
                    for (int ai = 0; ai < answerContents.Length; ai++)
                    {
                        resultAnswers.Add(new AnswerContent
                        {
                            Index = ai + 1,
                            Content = answerContents[ai]
                        });
                    }
                    var answers = questionJson.sAnswer[i].Replace("\n", "").Split(new string[] { "{div}" }, StringSplitOptions.None).Select(a =>
                    {
                        if (int.TryParse(a, out int index))
                        {
                            return new AnswerIndex
                            {
                                Index = index
                            };
                        }
                        else return null;
                    }).Where(a => a != null);

                    yield return new QuestionDetail
                    {
                        QuestionContent = questionContent,
                        QuestionId = questionId,
                        AnswerContents = resultAnswers,
                        AnswerIndexs = answers.ToList()
                    };
                }
            }
        }

        private IEnumerable<QuestionDetail> ConvertGroups(QuestionJson questionJson, int questionId)
        {
            int questionCnt = questionJson.sQuestion.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContents = questionJson.sQuestion[i];
                var answerContents = questionJson.sAnswer[i].Split(new string[] { "{div}" }, StringSplitOptions.None);

                var answers = new List<AnswerContent>();
                for (int ac = 0; ac < answerContents.Length; ac++)
                {
                    answers.Add(new AnswerContent
                    {
                        Content = answerContents[ac],
                        Index = ac + 1
                    });
                }
                yield return new QuestionDetail
                {
                    QuestionId = questionId,
                    QuestionContent = questionContents,
                    AnswerContents = answers
                };
            }
        }

        private IEnumerable<QuestionDetail> ConvertDragAndDrop(QuestionJson questionJson, int questionId)
        {
            var regex = "{[1-9]}";
            var matches = Regex.Matches(questionJson.sContent, regex);
            var questionMatches = new List<KeyValuePair<int, string>>();
            foreach (Match match in matches)
            {
                var valueNumber = Regex.Match(match.Value, "[1-9]")?.Value;
                if (int.TryParse(valueNumber, out int index))
                {
                    questionMatches.Add(new KeyValuePair<int, string>(index, match.Value)
                   );
                }
            }
            //var questionMatches = (matches as IEnumerable<Match>)
            //    .Select(m =>
            //    {
            //        var valueNumber = Regex.Match(m.Value, "[1-9]")?.Value;
            //        if (int.TryParse(valueNumber, out int index))
            //        {
            //            return new
            //            {
            //                Value = m.Value,
            //                Index = index
            //            };
            //        }
            //        return null;
            //    }).Where(r => r != null);

            var answers = questionJson.sQuestion[0].Split(new string[] { "{div}" }, StringSplitOptions.None);
            var answersContents = new List<AnswerContent>();
            for (int i = 0; i < answers.Length; i++)
            {
                answersContents.Add(new AnswerContent
                {
                    Content = answers[i].Replace(@"\n", ""),
                    Index = i + 1
                });
            }
            foreach (var question in questionMatches)
            {
                if (question.Key <= answersContents.Count)
                {
                    var answerIndex = answersContents[question.Key - 1].Index;
                    var questionDetailId = Guid.NewGuid();
                    var questionAnswers = answersContents.Select(a =>
                    {
                        a.QuestionDetailId = questionDetailId;
                        a.Id = Guid.NewGuid();
                        return a;
                    }).ToList();

                    yield return new QuestionDetail
                    {
                        Id = questionDetailId,
                        QuestionContent = question.Value,
                        AnswerContents = questionAnswers,
                        AnswerIndexs = new AnswerIndex[]
                        {
                            new AnswerIndex{ QuestionDetailId = questionDetailId, Index = answerIndex }
                        },
                        QuestionId = questionId
                    };
                }
            }
        }

        private IEnumerable<QuestionDetail> ConvertArrange(QuestionJson questionJson, int questionId)
        {
            return ConvertMatching(questionJson, questionId);
        }

        private IEnumerable<QuestionDetail> ConvertMatching(QuestionJson questionJson, int questionId)
        {
            int questionCnt = questionJson.sQuestion.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContents = questionJson.sQuestion[i].Split(new string[] { "{div}" }, StringSplitOptions.None);
                var answerContents = questionJson.sAnswer[i].Split(new string[] { "{div}" }, StringSplitOptions.None);
                if (questionContents.Count() <= answerContents.Count())
                {
                    int subQuestionCnt = questionContents.Count();
                    for (int questionIndex = 0; questionIndex < subQuestionCnt; questionIndex++)
                    {
                        yield return new QuestionDetail
                        {
                            QuestionId = questionId,
                            QuestionContent = questionContents[questionIndex],
                            AnswerContents = new AnswerContent[]
                            {
                                new AnswerContent
                                {
                                    Content = answerContents[questionIndex],
                                    Index = questionIndex+1
                                }
                            }
                        };
                    }
                }
            }
        }

        private IEnumerable<QuestionDetail> ConvertClozeTest(QuestionJson questionJson, int questionId)
        {
            var quesntionIndex = new List<string>();
            for (int index = 0; ; index += "{}".Length)
            {
                index = questionJson.sContent.IndexOf("{}", index);
                if (index == -1)
                    break;
                quesntionIndex.Add("{}");
            }
            int questionCnt = quesntionIndex.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContent = quesntionIndex[i];
                if (i >= questionJson.sAnswer.Length)
                {
                    continue;
                }
                var answerContents = questionJson.sAnswer[i];

                var answerContentStr = answerContents.Split(new string[] { "{div}" }, StringSplitOptions.None);
                var answers = new List<AnswerContent>();
                for (int ac = 0; ac < answerContentStr.Length; ac++)
                {
                    answers.Add(new AnswerContent
                    {
                        Content = answerContentStr[ac],
                        Index = ac + 1
                    });
                }
                var indexs = new List<AnswerIndex>();
                if (i < questionJson.iAnswer.Length)
                {
                    var answerIndexs = questionJson.iAnswer[i];
                    indexs = answerIndexs.Split(';').Select(ai =>
                    {
                        if (int.TryParse(ai.Trim(), out int index))
                        {
                            return new AnswerIndex() { Index = index };
                        }
                        return null;
                    }).Where(a => a != null).ToList();
                }

                yield return new QuestionDetail
                {
                    QuestionContent = questionContent,
                    QuestionId = questionId,
                    AnswerContents = answers,
                    AnswerIndexs = indexs
                };
            }
        }

        private IEnumerable<QuestionDetail> ConvertRewriteTheSentences(QuestionJson questionJson, int questionId)
        {
            return ConvertFillInTheBlank(questionJson, questionId);
        }

        private IEnumerable<QuestionDetail> ConvertFillInTheBlank(QuestionJson questionJson, int questionId)
        {
            int questionCnt = questionJson.sQuestion.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContent = questionJson.sQuestion[i];
                var answerContents = questionJson.sAnswer[i].Split(new string[] { "{div}" }, StringSplitOptions.None);
                //var answers = new List<AnswerContent>();
                //for (int ai = 0; ai < answerContents.Length; ai++)
                //{
                //    answers.Add(new AnswerContent
                //    {
                //        Content = answerContents[ai],
                //        Index = ai
                //    });
                //}

                var questions = new List<string>();
                for (int index = 0; ; index += "{}".Length)
                {
                    index = questionContent.IndexOf("{}", index);
                    if (index == -1)
                        break;
                    questions.Add("{}");
                }
                for (int index = 0; index < questions.Count; index++)
                {
                    if (index < answerContents.Length)
                    {
                        yield return new QuestionDetail
                        {
                            QuestionContent = "{}",
                            QuestionId = questionId,
                            AnswerContents = new List<AnswerContent> {
                                new AnswerContent
                                {
                                    Content =answerContents[index],
                                    Index = 1
                                }
                            }
                        };
                    }
                }
            }
            //return ConvertUnderline(questionJson, questionId);
        }

        private IEnumerable<QuestionDetail> ConvertReadingComprehension(QuestionJson questionJson, int questionId)
        {
            int questionCnt = questionJson.sQuestion.Count();
            for (int i = 0; i < questionCnt; i++)
            {
                var questionContent = questionJson.sQuestion[i];
                var answerContents = questionJson.sAnswer[i];
                var answerIndexs = questionJson.iAnswer[i];

                var answerContentStr = answerContents.Split(new string[] { "{div}" }, StringSplitOptions.None);
                var answers = new List<AnswerContent>();
                for (int ac = 0; ac < answerContentStr.Length; ac++)
                {
                    answers.Add(new AnswerContent
                    {
                        Content = answerContentStr[ac],
                        Index = ac + 1
                    });
                }

                var indexs = answerIndexs.Split(';').Select(ai =>
                {
                    if (int.TryParse(ai.Trim(), out int index))
                    {
                        return new AnswerIndex() { Index = index };
                    }
                    return null;
                }).Where(a => a != null).ToList();

                yield return new QuestionDetail
                {
                    QuestionContent = questionContent,
                    QuestionId = questionId,
                    AnswerContents = answers,
                    AnswerIndexs = indexs
                };
            }
        }

        private IEnumerable<QuestionDetail> ConvertMultipleChoice(QuestionJson questionJson, int questionId)
        {
            int questionCnt = questionJson.sQuestion.Count();
            for (int questionIndex = 0; questionIndex < questionCnt; questionIndex++)
            {
                var questionContent = questionJson.sQuestion[questionIndex];
                var answerContentStr = questionJson.sAnswer[questionIndex].Split(new string[] { "{div}" }, StringSplitOptions.None);
                var answerContents = new List<AnswerContent>();
                for (int i = 0; i < answerContentStr.Length; i++)
                {
                    answerContents.Add(new AnswerContent
                    {
                        Content = answerContentStr[i],
                        Index = i + 1
                    });
                }

                var answerIndexs = questionJson.iAnswer[questionIndex].Split(';').Select(i =>
                {
                    if (int.TryParse(i.Trim(), out int index))
                    {
                        return new AnswerIndex() { Index = index };
                    }
                    return null;
                }).Where(a => a != null).ToList();

                yield return new QuestionDetail
                {
                    QuestionContent = questionContent,
                    QuestionId = questionId,
                    AnswerContents = answerContents,
                    AnswerIndexs = answerIndexs
                };
            }
        }

        public QuestionDTO ConvertToQuestionDTO(Question question)
        {
            string questionContent = question.Content;
            string questionExplanation = question.Explanation;

            var questionJson = !string.IsNullOrWhiteSpace(question.QuestionJsonContent) ? JsonConvert.DeserializeObject<QuestionJson>(question.QuestionJsonContent) : new QuestionJson();
            questionContent = string.IsNullOrWhiteSpace(questionContent) ? questionJson.sContent : questionContent;
            questionExplanation = string.IsNullOrWhiteSpace(questionExplanation) ? questionJson.sExplanation : questionExplanation;

            var questionDetails = question.QuestionDetails.AsEnumerable();
            if (questionDetails.Count() == 0)
            {
                var questionDetailsFromJson = ConvertQuestionJson(question.Type ?? QuestionType.MultipleChoice, questionJson, question.QuestionId).ToList();
                var addQuestionDetail = AddQuestionDetail(questionDetailsFromJson);
                Task.WaitAll(addQuestionDetail);
                if (addQuestionDetail.Result && string.IsNullOrWhiteSpace(question.Content))
                {
                    var updateQuestion = _unitOfWork.GetRepository<Question>().Find(question.QuestionId);
                    if(updateQuestion != null)
                    {
                        try
                        {
                            updateQuestion.Content = questionContent.Trim();
                            updateQuestion.Explanation = questionExplanation.Trim();
                            _unitOfWork.GetRepository<Question>().Update(updateQuestion);
                            _unitOfWork.SaveChanges();
                        }
                        catch
                        {
                        }
                    }
                    
                }
                questionDetails = _unitOfWork.GetRepository<QuestionDetail>().GetAll().Include(nameof(QuestionDetail.AnswerContents)).Include(nameof(QuestionDetail.AnswerIndexs)).Where(q => q.QuestionId == question.QuestionId).ToList();
            }

            var unitId = question.UnitId ?? questionJson.iUnit;
            return new QuestionDTO()
            {
                QuestionContent = questionContent.Replace("<img alt=\"\" src=\"", "<img alt=\"\" src=\"" + _settings.Value.ImageServer),
                QuestionExplanation = questionExplanation.Replace("<img alt=\"\" src=\"", "<img alt=\"\" src=\"" + _settings.Value.ImageServer),
                QuestionDetail = questionDetails.Select(q => new QuestionDetailDTO
                {
                    QuestionDetailId = q.Id,
                    QuestionDetailContent = q.QuestionContent.Replace("<img alt=\"\" src=\"", "<img alt=\"\" src=\"" + _settings.Value.ImageServer),
                    QuestionId = q.QuestionId,
                    Answers = GetAnswerDTO(question.Type ?? QuestionType.MultipleChoice, q),
                    Order = q.Order
                }),
                Id = question.QuestionId,
                QuestionType = question.Type ?? QuestionType.MultipleChoice,
                UnitId = unitId,
                UnitName = question.Unit?.NameUnit
            };
        }

        

        public int CheckAnswer(QuestionDTO model, out bool isPassed)
        {
            var confrimQuestion = _unitOfWork.GetRepository<Question>().GetAll().Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerContents).Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerIndexs).FirstOrDefault(q => q.QuestionId == model.Id);
            isPassed = false;
            if (confrimQuestion != null)
            {
                var confrimQuestionDTO = ConvertToQuestionDTO(confrimQuestion);
                return CheckAnswer(confrimQuestion.Type ?? QuestionType.MultipleChoice, model, confrimQuestionDTO, out isPassed);
            }
            return 0;
        }

        private int CheckAnswer(QuestionType type, QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            isPassed = false;
            switch (type)
            {
                case QuestionType.MultipleChoice:
                case QuestionType.ReadingComprehension:
                case QuestionType.Underline:
                    return CheckMultipleChoiceAnswer(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.FillInTheBlank:
                case QuestionType.RewriteTheSentences:
                    return CheckFillInTheBlank(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.ClozeTest:
                    return CheckClozeTest(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.Matching:
                    return CheckMatching(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.Arrange:
                    return CheckArrange(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.DragAndDrop:
                    return CheckDragAndDrop(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.Groups:
                    return CheckGroups(checkQuestion, confirmQuestion, out isPassed);
                case QuestionType.UnderlineAndRewrite:
                case QuestionType.DeleteCharacter:
                default:
                    return 0;
            }
        }



        private int CheckGroups(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var mixAnswer = checkQuestion.QuestionDetail.Join(confirmQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => new
            {
                Id = cq.QuestionDetailId,
                RequestAnswers = cfq.Answers.Select(a => a.AnswerId),
                ResultAnswers = cq.Answers.Select(a => a.AnswerId)
            });
            if (mixAnswer.Count() == confirmQuestion.QuestionDetail.Count())
            {
                foreach (var question in mixAnswer)
                {
                    var correctAnswer = question.RequestAnswers.Join(question.ResultAnswers, r => r, a => a, (r, a) => r);
                    point = correctAnswer.Count() == question.ResultAnswers.Count() ? point + 1 : point;
                    isPassed = isPassed && correctAnswer.Count() == question.ResultAnswers.Count();
                }
            }
            return point;
        }

        private int CheckDragAndDrop(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var answerContentList = _unitOfWork.GetRepository<AnswerContent>().GetAll().Include(a => a.Question).ThenInclude(q => q.Question).Where(q => q.Question.Question.QuestionId == confirmQuestion.Id).Select(a => new { Id = a.Id, Index = a.Index }).ToList();
            var mixAnswer = checkQuestion.QuestionDetail.Join(confirmQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => cq.QuestionDetailId);
            if (mixAnswer.Count() == confirmQuestion.QuestionDetail.Count())
            {
                foreach (var question in mixAnswer)
                {
                    var getResultAnswerId = _unitOfWork.GetRepository<AnswerIndex>().Find(a => a.QuestionDetailId == question);
                    var answerContentIndex = answerContentList.Where(a => a.Index == getResultAnswerId?.Index).FirstOrDefault();
                    var selectedAnswerId = checkQuestion.QuestionDetail.Where(q => q.QuestionDetailId == question).SelectMany(q => q.Answers).FirstOrDefault();

                    point = answerContentIndex?.Id == selectedAnswerId.AnswerId ? point + 1 : point;
                    isPassed = isPassed && answerContentIndex?.Id == selectedAnswerId.AnswerId;
                }
            }
            return point;
        }

        private int CheckArrange(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var mixAnswer = checkQuestion.QuestionDetail.Join(confirmQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => new
            {
                QuestionDetailId = cq.QuestionDetailId,
                ResultAnswer = cq.Answers.FirstOrDefault()?.AnswerIndex,
                RequestAnswer = cfq.Answers?.FirstOrDefault()?.AnswerIndex
            });
            if (mixAnswer.Count() == confirmQuestion.QuestionDetail.Count() && mixAnswer.All(m => m.RequestAnswer != null))
            {
                foreach (var question in mixAnswer)
                {
                    point = question.RequestAnswer == question.ResultAnswer ? point + 1 : point;
                    isPassed = isPassed && question.RequestAnswer == question.ResultAnswer;
                }
            }
            return point;
        }

        private int CheckMatching(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var mixAnswer = checkQuestion.QuestionDetail.Join(confirmQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => new
            {
                QuestionDetailId = cq.QuestionDetailId,
                ResultAnswer = cq.Answers.FirstOrDefault()?.AnswerId,
                RequestAnswer = cfq.Answers?.FirstOrDefault()?.AnswerId
            });
            if (mixAnswer.Count() == confirmQuestion.QuestionDetail.Count() && mixAnswer.All(m => m.RequestAnswer != null))
            {
                foreach (var question in mixAnswer)
                {
                    point = question.RequestAnswer == question.ResultAnswer ? point + 1 : point;
                    isPassed = isPassed && question.RequestAnswer == question.ResultAnswer;
                }
            }
            return point;
        }

        private int CheckClozeTest(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var mixAnswer = checkQuestion.QuestionDetail.Join(confirmQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => new
            {
                QuestionDetailId = cq.QuestionDetailId,
                ResultAnswer = cq.Answers.Where(q => q.IsCorrectAnswer).FirstOrDefault()?.AnswerId,
                RequestAnswer = cfq.Answers?.FirstOrDefault()?.AnswerId
            });
            if (mixAnswer.Count() == confirmQuestion.QuestionDetail.Count() && mixAnswer.All(m => m.RequestAnswer != null))
            {
                foreach (var question in mixAnswer)
                {
                    point = question.RequestAnswer == question.ResultAnswer ? point + 1 : point;
                    isPassed = isPassed && question.RequestAnswer == question.ResultAnswer;
                }
            }
            return point;
        }

        private int CheckFillInTheBlank(QuestionDTO checkQuestion, QuestionDTO confrimQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            var mixAnswer = checkQuestion.QuestionDetail.Join(confrimQuestion.QuestionDetail, cq => cq.QuestionDetailId, cfq => cfq.QuestionDetailId, (cq, cfq) => new
            {
                QuestionDetailId = cq.QuestionDetailId,
                ResultAnswer = cq.Answers.FirstOrDefault()?.AnswerContent.Replace("\n", ""),
                RequestAnswer = cfq.Answers?.FirstOrDefault()?.AnswerContent.Replace("\n", "")
            });

            if (mixAnswer.Count() == confrimQuestion.QuestionDetail.Count() && mixAnswer.All(m => !string.IsNullOrWhiteSpace(m.RequestAnswer)))
            {
                foreach (var question in mixAnswer)
                {
                    point = System.Net.WebUtility.HtmlDecode(question.RequestAnswer).ToLower().Trim().Equals(question.ResultAnswer.ToLower().Trim()) ? point + 1 : point;
                    isPassed = isPassed && question.RequestAnswer.ToLower().Trim().Equals(question.ResultAnswer.ToLower().Trim());
                }
            }
            return point;
        }

        private int CheckMultipleChoiceAnswer(QuestionDTO checkQuestion, QuestionDTO confirmQuestion, out bool isPassed)
        {
            int point = 0;
            isPassed = true;
            foreach (var cofirm in confirmQuestion.QuestionDetail)
            {
                var checkDetail = checkQuestion.QuestionDetail.FirstOrDefault(c => c.QuestionDetailId == cofirm.QuestionDetailId);

                var mixAnswer = cofirm.Answers.Join(checkDetail.Answers, cf => cf.AnswerId, ck => ck.AnswerId, (cf, ck) => new { AnswerId = cf.AnswerId, Result = cf.IsCorrectAnswer, Request = ck.IsCorrectAnswer });

                point = mixAnswer.Count() == cofirm.Answers.Count() && mixAnswer.All(m => m.Result == m.Request) ? point + 1 : point;
                isPassed = isPassed && mixAnswer.Count() == cofirm.Answers.Count() && mixAnswer.All(m => m.Result == m.Request);

            }
            return point;
        }
    }
}