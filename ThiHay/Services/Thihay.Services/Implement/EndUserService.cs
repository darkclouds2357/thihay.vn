﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using Thihay.Common;
using Thihay.Domain.Entities;
using Thihay.Services.DTO;
using Thihay.Services.Interfaces;
using Thihay.Services.ViewModel;
using Thihay.API.Client.Models;

namespace Thihay.Services.Implement
{
    public class EndUserService : IEndUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IThihayUser _currentUser;
        private readonly IQuestionManager _questionManager;
        private readonly IOptionsSnapshot<AppSettings> _settings;
        private readonly IUserManager _userManager;
        public EndUserService(IOptionsSnapshot<AppSettings> settings, IUnitOfWork unitOfWork, IThihayUser currentUser, IQuestionManager questionManager, IUserManager userManager)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
            _questionManager = questionManager;
            _userManager = userManager;
            _settings = settings;
        }

        public IEnumerable<ClassDTO> GetValidClassesBySubjetId(int subjectId)
        {
            var classes = _unitOfWork.GetRepository<Class>().GetAll();
            var topics = _unitOfWork.GetRepository<Topic>().GetAll(t => t.SubjectId == subjectId);
            var units = _unitOfWork.GetRepository<Unit>().GetAll();

            var validTopics = topics.Join(units, t => t.TopicId, u => u.TopicId, (t, u) => t.ClassId).Distinct();


            var vaildClasses = classes.Join(validTopics, c => c.ClassId, t => t, (c, t) => new ClassDTO
            {
                Id = c.ClassId,
                ClassName = c.ClassName
            }).OrderBy(c => c.Id);

            return vaildClasses;
        }

        public IEnumerable<TopicDTO> GetTopicByClassIdAndSubjectId(int classId, int subjectId)
        {
            var classes = _unitOfWork.GetRepository<Class>().GetAll(c => c.ClassId == classId);
            var subjects = _unitOfWork.GetRepository<Subject>().GetAll(s => s.SubjectId == subjectId);
            var units = _unitOfWork.GetRepository<Unit>().GetAll();
            var topics = _unitOfWork.GetRepository<Topic>().GetAll();

            var classTopic = topics.Join(classes, t => t.ClassId, c => c.ClassId, (t, c) => new
            {
                TopicId = t.TopicId,
                TopicName = t.NameTopic,
                DescriptionTopic = t.DescriptionTopic,
                SubjectId = t.SubjectId,
                ClassId = c.ClassId,
                ClassName = c.ClassName
            });
            var searchTopic = classTopic.Join(subjects, t => t.SubjectId, s => s.SubjectId, (t, s) => new
            {
                TopicId = t.TopicId,
                TopicName = t.TopicName,
                DescriptionTopic = t.DescriptionTopic,
                SubjectId = s.SubjectId,
                SubjectName = s.SubjectName,
                ClassId = t.ClassId,
                ClassName = t.ClassName
            });

            var result = searchTopic.Join(units, t => t.TopicId, u => u.TopicId, (t, u) => new
            {
                TopicId = t.TopicId,
                TopicName = t.TopicName,
                DescriptionTopic = t.DescriptionTopic,
                SubjectId = t.SubjectId,
                SubjectName = t.SubjectName,
                ClassId = t.ClassId,
                ClassName = t.ClassName,
                UnitId = u.UnitId,
                NameUnit = u.NameUnit,
                SortOrder = u.SortOrder
            }).ToList().GroupBy(t => new
            {
                t.ClassId,
                t.ClassName,
                t.SubjectId,
                t.SubjectName,
                t.TopicId,
                t.TopicName,
                t.DescriptionTopic
            }).Select(t => new TopicDTO
            {

                ClassId = t.Key.ClassId,
                ClassName = t.Key.ClassName,
                SubjectId = t.Key.SubjectId,
                SubjectName = t.Key.SubjectName,
                Id = t.Key.TopicId,
                TopicName = t.Key.TopicName,
                DescriptionTopic = t.Key.DescriptionTopic,
                Units = t.Select(u => new UnitDTO
                {
                    Id = u.UnitId,
                    NameUnit = u.NameUnit,
                    SortOrder = u.SortOrder
                })
            }).ToList();

            return result;
            //var topics = _unitOfWork.GetRepository<Topic>().GetAll().Include(nameof(Topic.Class)).Include(nameof(Topic.Subject)).Include(nameof(Topic.Units)).Include("Units.Questions").Where(t => t.ClassId == classId && t.SubjectId == subjectId && t.Units.Any()).ToList().Select(t => new TopicDTO
            //{
            //    ClassId = classId,
            //    ClassName = t.Class.ClassName,
            //    SubjectName = t.Subject.SubjectName,
            //    Id = t.TopicId,
            //    DescriptionTopic = t.DescriptionTopic,
            //    SubjectId = subjectId,
            //    TopicName = t.NameTopic,
            //    Units = t.Units.Select(u => new UnitDTO
            //    {
            //        Id = u.UnitId,
            //        NameUnit = u.NameUnit,
            //        SortOrder = u.SortOrder
            //    })
            //});
            //return topics;
        }

        public IEnumerable<SummaryModel> GetSummary()
        {

            var classes = _unitOfWork.GetRepository<Class>().GetAll();
            var subjects = _unitOfWork.GetRepository<Subject>().GetAll();
            var unit = _unitOfWork.GetRepository<Unit>().GetAll();
            var topics = _unitOfWork.GetRepository<Topic>().GetAll();

            var unitCount = topics.Join(unit, t => t.TopicId, u => u.TopicId, (t, u) => new { SubjectId = t.SubjectId, ClassId = t.ClassId, UnitId = u.UnitId }).GroupBy(u => new { u.ClassId, u.SubjectId }, u => u.UnitId)
                .Select(u => new
                {
                    ClassId = u.Key.ClassId,
                    SubjectId = u.Key.SubjectId,
                    UnitCount = u.Count()
                });

            var subjectUnit = subjects.Join(unitCount, s => s.SubjectId, u => u.SubjectId, (s, u) => new { SubjectId = s.SubjectId, SubjectName = s.SubjectName, ClassId = u.ClassId, UnitCount = u.UnitCount });

            var summary = classes.Join(subjectUnit, c => c.ClassId, s => s.ClassId, (c, s) => new { SubjectId = s.SubjectId, SubjectName = s.SubjectName, ClassId = c.ClassId, ClassName = c.ClassName, UnitCount = s.UnitCount })
                .GroupBy(c => new { c.ClassId, c.ClassName }, s => new { s.SubjectId, s.SubjectName, s.UnitCount }).ToDictionary(m => m.Key, s => s.ToList())
                .Select(m => new SummaryModel
                {
                    ClassId = m.Key.ClassId,
                    ClassName = m.Key.ClassName,
                    SubjectSummary = m.Value.Select(s => new SummaryDetailModel
                    {
                        SubjectId = s.SubjectId,
                        SubjectName = s.SubjectName,
                        TotalUnit = s.UnitCount
                    }).ToList()
                });

            return summary;

            //var classes = _unitOfWork.GetRepository<Class>().GetAll().Select(c => new { Id = c.ClassId, ClassName = c.ClassName });
            //var subjects = _unitOfWork.GetRepository<Subject>().GetAll().Select(c => new { Id = c.SubjectId, SubjectName = c.SubjectName });
            //var unit = _unitOfWork.GetRepository<Unit>().GetAll().Include(nameof(Unit.Topic)).Where(u => u.Topic != null);

            //var summary = classes.Join(subjects, c => 1, s => 1, (c, s) => new
            //{
            //    Class = c,
            //    Subject = s
            //}).GroupBy(c => c.Class, g => g.Subject).ToDictionary(c => c.Key, s => s.ToList());
            //foreach (var @class in summary)
            //{
            //    var summaryDetail = new List<SummaryDetailModel>();
            //    foreach (var subject in @class.Value)
            //    {
            //        var totalUnit = unit.Where(u => u.Topic.SubjectId == subject.Id && u.Topic.ClassId == @class.Key.Id).Count();
            //        summaryDetail.Add(new SummaryDetailModel
            //        {
            //            SubjectId = subject.Id,
            //            SubjectName = subject.SubjectName,
            //            TotalUnit = totalUnit
            //        });
            //    }
            //    yield return new SummaryModel
            //    {
            //        ClassId = @class.Key.Id,
            //        ClassName = @class.Key.ClassName,
            //        SubjectSummary = summaryDetail
            //    };
            //}
        }

        public IEnumerable<SubjectDTO> GetAllSubject()
        {
            var units = _unitOfWork.GetRepository<Unit>().GetAll();
            var topics = _unitOfWork.GetRepository<Topic>().GetAll();
            var vaildSubjectIds = topics.Join(units, t => t.TopicId, u => u.TopicId, (t, u) => t.SubjectId).Distinct();
            var subjects = _unitOfWork.GetRepository<Subject>().GetAll().Join(vaildSubjectIds, s => s.SubjectId, v => v, (s, v) => new SubjectDTO
            {
                Id = s.SubjectId,
                SubjectName = s.SubjectName
            });
            return subjects;
        }

        public UnitDTO GetUnit(int unitId)
        {
            var unit = _unitOfWork.GetRepository<Unit>().Find(unitId);

            if (unit != null)
            {
                return new UnitDTO
                {
                    Id = unit.UnitId,
                    DescriptionUnit = unit.DescriptionUnit,
                    NameUnit = unit.NameUnit
                };
            }
            return null;
        }

        public QuestionDTO GetRandomQuestion(int unitId)
        {
            var user = _userManager.GetLoginUser();
            var questionsOfUnit = _unitOfWork.GetRepository<Question>().GetAll().Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerContents).Include(q => q.QuestionDetails).ThenInclude(q => q.AnswerIndexs).Where(q => q.UnitId == unitId);
            if (user != null)
            {
                var marks = _unitOfWork.GetRepository<Mark>().GetAll(m => m.UserID == user.UserId && m.UnitId == unitId).ToList();
                var alreadyDone = marks.SelectMany(m => m.PassedQuestionId);
                questionsOfUnit = questionsOfUnit.Where(q => !alreadyDone.Contains(q.QuestionId));
            }

            Random rand = new Random();
            int questionsCnt = questionsOfUnit.Count();
            if (questionsCnt > 0)
            {
                int randomSkip = rand.Next(0, questionsCnt);

                var randomQuestion = questionsOfUnit.Skip(randomSkip).FirstOrDefault();

                if (randomQuestion != null)
                {
                    var result = _questionManager.ConvertToQuestionDTO(randomQuestion);
                    if (result.QuestionDetail == null)
                    {
                        return GetRandomQuestion(unitId);
                    }
                    return result;
                }
            }
            return null;
        }

        public IEnumerable<MarkDTO> GetUserProfile(int? subjectId)
        {
            var user = _userManager.GetLoginUser();
            if (user != null)
            {
                var marks = _unitOfWork.GetRepository<Mark>().GetAll().Include(m => m.Unit).Include(m => m.Subject).Include(m => m.Class).Where(m => m.UserID == user.UserId);

                subjectId = !subjectId.HasValue ? marks.Where(m => m.SubjectId != null).Min(m => m.SubjectId) : subjectId;
                subjectId = !subjectId.HasValue ? _unitOfWork.GetRepository<Subject>().GetAll().Select(s => s.SubjectId).OrderBy(s => s).FirstOrDefault() : subjectId;

                marks = marks.Where(m => m.SubjectId == subjectId);

                return marks.ToList().Select(m => new MarkDTO
                {
                    Mark = m.MarkPoint ?? 0,
                    PracticeTime = m.Time ?? 0,
                    QuestionsPassed = m.PassedCount ?? 0,
                    TotalAnswerd = m.AnsweredCount ?? 0,
                    UnitId = m.Unit?.UnitId ?? 0,
                    UnitName = m.Unit?.NameUnit,
                    ClassId = m.Class?.ClassId ?? 0,
                    ClassName = m.Class?.ClassName,
                    SubjectId = m.Subject?.SubjectId ?? 0,
                    SubjectName = m.Subject?.SubjectName
                });
            }
            return null;
        }

        public ExecutionResult<int> SubmitAnswer(QuestionDTO model)
        {
            var result = new ExecutionResult<int>();
            try
            {
                var markRepository = _unitOfWork.GetRepository<Mark>();
                var classAndSubject = _unitOfWork.GetRepository<Question>().GetAll().Include(q => q.Unit).Select(q => new
                {
                    ClassId = q.Unit.ClassId,
                    SubjectId = q.Unit.SubjectId,
                    UnitId = q.UnitId,
                    Id = q.QuestionId

                }).FirstOrDefault(q => q.Id == model.Id);
                //var question = _unitOfWork.GetRepository<Question>().GetAll().Where(q => q.QuestionId == model.Id);
                //var classes = _unitOfWork.GetRepository<Class>().GetAll();
                //var subjects = _unitOfWork.GetRepository<Subject>().GetAll();
                //var units = _unitOfWork.GetRepository<Unit>().GetAll();
                //var topics = _unitOfWork.GetRepository<Topic>().GetAll();

                //var questionUnits = question.Join(units, q => q.UnitId, u => u.UnitId, (q, u) => new { TopicId = u.TopicId, UnitId = q.UnitId });
                //var unitTopics = questionUnits.Join(topics, q => q.TopicId, t => t.TopicId, (q, t) => new { ClassId = t.ClassId, SubjectId = t.SubjectId, UnitId = q.UnitId });
                //var classTopics = unitTopics.Join(classes, t => t.ClassId, c => c.ClassId, (t, c) => new { ClassId = t.ClassId, SubjectId = t.SubjectId, UnitId = t.UnitId });
                //var classAndSubject = classTopics.Join(subjects, c => c.SubjectId, s => s.SubjectId, (c, s) => new { ClassId = c.ClassId, SubjectId = c.SubjectId, UnitId = c.UnitId }).FirstOrDefault();
                var user = _userManager.GetLoginUser();
                if (classAndSubject != null && user != null)
                {
                    var userMark = markRepository.Find(m => m.UserID == user.UserId && m.UnitId == classAndSubject.UnitId);
                    if (userMark == null)
                    {
                        userMark = new Mark
                        {
                            ClassId = classAndSubject.ClassId,
                            SubjectId = classAndSubject.SubjectId,
                            UserID = user.UserId,
                            UnitId = classAndSubject.UnitId,
                            Time = 0,
                            AnsweredCount = 0,
                            MarkPoint = 0,
                            PassedCount = 0,
                            QuestionPassed = string.Empty
                        };
                        markRepository.Insert(userMark);
                        _unitOfWork.SaveChanges();
                    }


                    var newTime = (userMark.Time ?? 0) + (model.AnswerTime ?? 0);
                    var answeredCnt = (userMark.AnsweredCount ?? 0) + 1;
                    var markPoint = userMark.MarkPoint ?? 0;
                    var passedCount = userMark.PassedCount ?? 0;
                    var questionPassed = userMark.QuestionPassed.Trim() ?? string.Empty;
                    int correctAnswer = _questionManager.CheckAnswer(model, out bool isPassed);
                    if (correctAnswer > 0)
                    {
                        markPoint = markPoint + correctAnswer;
                    }
                    if (isPassed)
                    {
                        passedCount++;
                        questionPassed = string.IsNullOrWhiteSpace(questionPassed) ? $"{model.Id}" : $"{questionPassed};{model.Id}";
                    }

                    userMark.Time = newTime;
                    userMark.AnsweredCount = answeredCnt;
                    userMark.MarkPoint = markPoint;
                    userMark.PassedCount = passedCount;
                    userMark.QuestionPassed = questionPassed.Trim();

                    markRepository.Update(userMark);
                    if (_unitOfWork.SaveChanges() > 0)
                    {
                        result.Success = true;
                        result.Result = correctAnswer;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Error = "Can't found question data of class and subject.";
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }
    }
}