﻿using Microsoft.Extensions.Options;
using ST.Library.Base.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thihay.Common;
using Thihay.Domain.Entities;
using Thihay.Services.Interfaces;

namespace Thihay.Services.Implement
{
    public class UserManager : IUserManager
    {
        private readonly IThihayUser _currentUser;
        private readonly IUnitOfWork _unitOfWork;
        public UserManager(IUnitOfWork unitOfWork, IThihayUser currentUser)
        {
            _currentUser = currentUser;
            _unitOfWork = unitOfWork;
        }
        public User GetLoginUser()
        {
            var user = _unitOfWork.GetRepository<User>().Find(u=>u.AuthUserId == _currentUser.UserId);
            if(user == null)
            {
                user = new User
                {
                    Address = _currentUser.Address,
                    AuthUserId = _currentUser.UserId,
                    FacebookId = _currentUser.FacebookId,
                    GoogleId = _currentUser.GoogleId,
                    DayOfBirth = _currentUser.DateOfBirth,
                    Email = _currentUser.Email,
                    FullName = _currentUser.FullName,
                    Gmail = _currentUser.Email,
                    Phone = _currentUser.Phone                    
                };
                _unitOfWork.GetRepository<User>().Insert(user);
                _unitOfWork.SaveChanges();
            }
            return user;
        }
    }
}
