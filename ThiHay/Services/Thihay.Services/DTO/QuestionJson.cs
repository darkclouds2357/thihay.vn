﻿namespace Thihay.Services.DTO
{
    public class QuestionJson
    {
        public int iType { get; set; }
        public int iUnit { get; set; }
        public string sContent { get; set; }

        public string sExplanation { get; set; }

        public string[] sQuestion { get; set; }

        public string[] sAnswer { get; set; }

        public string[] iAnswer { get; set; }
    }
}