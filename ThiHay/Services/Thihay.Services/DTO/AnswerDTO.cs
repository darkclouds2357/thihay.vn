﻿using System;

namespace Thihay.Services.DTO
{
    public class AnswerDTO
    {
        public string AnswerContent { get; set; }

        public int AnswerIndex { get; set; }

        public bool IsCorrectAnswer { get; set; }

        public Guid AnswerId { get; set; }

        public int QuestionOrder { get; set; }
    }
}