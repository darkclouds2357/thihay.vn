﻿using System.Collections.Generic;

namespace Thihay.Services.DTO
{
    public class TopicDTO : BaseDTO
    {
        public string TopicName { get; set; }

        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }

        public int? ClassId { get; set; }
        public string ClassName { get; set; }
        public string DescriptionTopic { get; set; }
        public bool IsDelete { get; set; }
        public IEnumerable<UnitDTO> Units { get; set; }
    }
}