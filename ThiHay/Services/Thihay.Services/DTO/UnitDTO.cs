﻿namespace Thihay.Services.DTO
{
    public class UnitDTO : BaseDTO
    {
        public string NameUnit { get; set; }

        public string DescriptionUnit { get; set; }

        public int? SortOrder { get; set; }

        public int QuestionCount { get; set; }

        public bool IsDeleted { get; set; }

        public int? TopicId { get; set; }

        public string TopicName { get; set; }

        public TopicDTO Topic { get; set; }
    }
}