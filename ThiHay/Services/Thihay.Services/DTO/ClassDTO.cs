﻿using System.Collections.Generic;

namespace Thihay.Services.DTO
{
    public class ClassDTO : BaseDTO
    {
        public string ClassName { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<TopicDTO> Topics { get; set; }
    }
}