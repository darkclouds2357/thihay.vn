﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thihay.Services.DTO
{
    public class MarkDTO
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public decimal Mark { get; set; }
        public int QuestionsPassed { get; set; }
        public int TotalAnswerd { get; set; }
        public int PracticeTime { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
    }
}
