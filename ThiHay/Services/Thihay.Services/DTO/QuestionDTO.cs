﻿using System.Collections.Generic;
using Thihay.Domain.Enum;

namespace Thihay.Services.DTO
{
    public class QuestionDTO : BaseDTO
    {
        public int? ClassId { get; set; }
        public string ClassName { get; set; }

        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }

        public int? TopicId { get; set; }
        public string TopicName { get; set; }

        public int UnitId { get; set; }
        public string UnitName { get; set; }

        public string QuestionContent { get; set; }

        public string QuestionExplanation { get; set; }

        public QuestionType QuestionType { get; set; }

        public IEnumerable<QuestionDetailDTO> QuestionDetail { get; set; }

        public int? AnswerTime { get; set; }

        public bool? IsDeleted { get; set; }
    }
}