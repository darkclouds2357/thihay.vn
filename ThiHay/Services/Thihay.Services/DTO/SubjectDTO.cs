﻿using System.Collections.Generic;

namespace Thihay.Services.DTO
{
    public class SubjectDTO : BaseDTO
    {
        public string SubjectName { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<TopicDTO> Topics { get; set; }
    }
}