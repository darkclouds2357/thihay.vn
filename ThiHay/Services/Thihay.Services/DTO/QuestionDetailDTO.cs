﻿using System;
using System.Collections.Generic;

namespace Thihay.Services.DTO
{
    public class QuestionDetailDTO : BaseDTO
    {
        public int Order { get; set; }
        public Guid QuestionDetailId { get; set; }
        public int QuestionId { get; set; }
        public string QuestionDetailContent { get; set; }
        public IEnumerable<AnswerDTO> Answers { get; set; }
    }
}