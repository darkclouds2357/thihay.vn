﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.Domain.Entities;
using Thihay.Domain.Enum;
using Thihay.Services.DTO;

namespace Thihay.Services.Interfaces
{
    public interface IQuestionManager
    {
        IEnumerable<QuestionDetail> ConvertQuestionJson(QuestionType questionType, QuestionJson questionJson, int questionId);

        Task<bool> AddQuestionDetail(IEnumerable<QuestionDetail> questionDetail);

        IEnumerable<AnswerDTO> GetAnswerDTO(QuestionType questionType, QuestionDetail questionDetail);

        QuestionDTO ConvertToQuestionDTO(Question question);
        int CheckAnswer(QuestionDTO model, out bool isPassed);
    }
}