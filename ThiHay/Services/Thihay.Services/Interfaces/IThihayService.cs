﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.Domain.Enum;
using Thihay.Services.DTO;

namespace Thihay.Services.Interfaces
{
    public interface IThihayService
    {
        IEnumerable<ClassDTO> GetClass(string searchKey);

        Task<bool> ChangeClassStatus(int classId, bool isDelete);

        Task<bool> UpdateClass(ClassDTO @class);

        IEnumerable<SubjectDTO> GetSubject(string searchKey);

        Task<bool> ChangeSubjectStatus(int subjectId, bool isDelete);

        Task<bool> UpdateSubject(SubjectDTO subject);

        IEnumerable<TopicDTO> GetTopics(string searchKey, int? subjectId, int? classId, int? page, int? pageSize, out int totalPage);

        Task<bool> UpdateTopic(TopicDTO topic);

        Task<bool> ChangeTopicStatus(int topicId, bool isDelete);

        TopicDTO GetTopicDetail(int topicId);

        IEnumerable<UnitDTO> GetUnits(string searchKey, int? subjectId, int? classId, int? topicId, int? page, int? pageSize, out int totalPage);

        UnitDTO GetUnitDetail(int unitId);

        Task<bool> UpdateUnit(UnitDTO unit);

        Task<bool> UpdateUnitOrder(int unitId, int newOrder);

        Task<bool> ChangeUnitStatus(int unitId, bool isDelete);

        IEnumerable<QuestionDTO> GetAllQuestion(string searchKey, int? subjectId, int? classId, int? topicId, int? unitId, QuestionType? questionType, out int totalPage, int? page, int? pageSize);

        Task<bool> UpdateQuestion(QuestionDTO questionDto);

        QuestionDTO GetQuestionDetail(int questionId);

        Task<bool> ChangeQuestionStatus(int questionId, bool isDelete);
    }
}