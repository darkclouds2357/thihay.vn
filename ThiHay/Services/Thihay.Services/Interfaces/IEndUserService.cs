﻿using System.Collections.Generic;
using Thihay.API.Client.Models;
using Thihay.Services.DTO;
using Thihay.Services.ViewModel;

namespace Thihay.Services.Interfaces
{
    public interface IEndUserService
    {
        IEnumerable<SummaryModel> GetSummary();

        IEnumerable<ClassDTO> GetValidClassesBySubjetId(int subjectId);

        IEnumerable<TopicDTO> GetTopicByClassIdAndSubjectId(int classId, int subjectId);

        IEnumerable<SubjectDTO> GetAllSubject();

        UnitDTO GetUnit(int unitId);

        QuestionDTO GetRandomQuestion(int unitId);

        IEnumerable<MarkDTO> GetUserProfile(int? subjectId);
        ExecutionResult<int> SubmitAnswer(QuestionDTO model);
    }
}