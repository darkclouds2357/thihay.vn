﻿using System;
using System.Collections.Generic;
using System.Text;
using Thihay.Domain.Entities;

namespace Thihay.Services.Interfaces
{
    public interface IUserManager
    {
        User GetLoginUser();
    }
}
