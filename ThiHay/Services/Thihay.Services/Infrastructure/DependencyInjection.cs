﻿using Microsoft.Extensions.DependencyInjection;
using Thihay.Services.Implement;
using Thihay.Services.Interfaces;

namespace Thihay.Services.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddThihayService(this IServiceCollection services)
        {
            services.AddTransient<IQuestionManager, QuestionManager>();
            services.AddTransient<IThihayService, ThihayService>();
            services.AddTransient<IEndUserService, EndUserService>();
            return services;
        }
    }
}