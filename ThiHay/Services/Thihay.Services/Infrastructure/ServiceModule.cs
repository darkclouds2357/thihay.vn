﻿using Autofac;
using Thihay.Services.Implement;
using Thihay.Services.Interfaces;

namespace Thihay.Services.Infrastructure
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.Register<QuestionManager>(c =>
            //{
            //    var unitOfWork = c.Resolve<IUnitOfWork>();
            //    var thihayUser = c.Resolve<ICurrentUser>() as IThihayUser;

            //    return new QuestionManager(unitOfWork, thihayUser);
            //})
            //.As<IQuestionManager>().InstancePerLifetimeScope();

            builder.RegisterType<QuestionManager>()
                .As<IQuestionManager>()
                .InstancePerLifetimeScope();
            builder.RegisterType<UserManager>()
               .As<IUserManager>()
               .InstancePerLifetimeScope();
            builder.RegisterType<ThihayService>()
                .As<IThihayService>()
                .InstancePerLifetimeScope();
            builder.RegisterType<EndUserService>()
                .As<IEndUserService>()
                .InstancePerLifetimeScope();
        }
    }
}