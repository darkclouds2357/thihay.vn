﻿namespace Thihay.Services.ViewModel
{
    public class SummaryDetailModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int TotalUnit { get; set; }
    }
}