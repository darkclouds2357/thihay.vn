﻿using System.Collections.Generic;

namespace Thihay.Services.ViewModel
{
    public class SummaryModel
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public IList<SummaryDetailModel> SubjectSummary { get; set; }
    }
}