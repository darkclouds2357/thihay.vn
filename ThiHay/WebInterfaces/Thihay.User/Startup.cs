﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Resilience.Http;
using System;
using Thihay.API.Client;
using Thihay.API.Client.Infrastructure;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.User
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.Configure<AppSettings>(Configuration);
            // Add application services.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            if (Configuration.GetValue<string>("UseResilientHttp") == bool.TrueString)
            {
                services.AddSingleton<IResilientHttpClientFactory, ResilientHttpClientFactory>();
                services.AddSingleton<IHttpClient, ResilientHttpClient>(sp => sp.GetService<IResilientHttpClientFactory>().CreateResilientHttpClient());
            }
            else
            {
                services.AddSingleton<IHttpClient, StandardHttpClient>();
            }

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            //services.AddSession(options =>
            //{
            //    options.CookieName = ".Thihay.Session";
            //    options.IdleTimeout = TimeSpan.FromSeconds(3600);
            //});
            services.AddScoped<ITokenModel, TokenModel>();

            var container = new ContainerBuilder();
            container.Populate(services);
            container.RegisterModule(new EndUserServiceModule());

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            

            app.Use(async (ctx, next) =>
            {
                var accessToken = ctx.RequestServices.GetRequiredService<ITokenModel>();
                var tokenCookies = ctx.Request.Cookies["access_token"];
                accessToken.UpdateToken(tokenCookies);
                await next.Invoke();
            });
            //app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "profile",
                    template: "profile/{subjectId?}");
                routes.MapRoute(
                    name: "information",
                    template: "information");
                routes.MapRoute(
                    name: "vipinfo",
                    template: "vipinfo");
                routes.MapRoute(
                    name: "changepassword",
                    template: "changepassword"
                    );
                routes.MapRoute(
                   name: "changepasswordpost",
                   template: "changepasswordpost"
                   );
                routes.MapRoute(
                    name: "registerpost",
                    template: "registerpost");
                routes.MapRoute(
                   name: "loginpost",
                   template: "loginpost");
                routes.MapRoute(
                    name: "register",
                    template: "register");
                routes.MapRoute(
                    name: "homepage",
                    template: "homepage"
                    );
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "login",
                    template: "login");

                //routes.MapRoute(
                //    name: "facebook-sign-in",
                //    template: "facebookSignIn");

                //routes.MapRoute(
                //   name: "sign-in-google",
                //   template: "googleSignIn");

                routes.MapRoute(
                    name: "logout",
                    template: "logout");

                routes.MapRoute(
                    name: "practice",
                    template: "practice/{id}");
                routes.MapRoute(
                    name: "topic",
                    template: "Topic/subject/{subjectId}/class/{classId?}");
            });
        }
    }
}