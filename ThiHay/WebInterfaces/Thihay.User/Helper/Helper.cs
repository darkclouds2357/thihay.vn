﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.User.Helper
{
    public static class Helper
    {
        public static T[] Shuffle<T>(T[] input)
        {
            var rnd = new Random();
            int m = input.Length, i;
            T t;
            while (1 == 1)
            {
                m--;
                if (m < 0) break;
                i = rnd.Next(m);
                t = input[m];
                input[m] = input[i];
                input[i] = t;
            }
            return input;
        }
    }
}
