using Microsoft.AspNetCore.Mvc;
using Thihay.API.Client.Interfaces;

namespace Thihay.User.Controllers
{
    public class SubjectController : BaseController
    {
        private readonly IEndUserService _endUserService;

        public SubjectController(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}