﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.User.Controllers
{
    public class PracticeController : BaseController
    {
        private readonly IEndUserService _service;
        private readonly string _token;
        public PracticeController(IEndUserService service, ITokenModel tokenModel)
        {
            _service = service;
            _token = tokenModel.AccessToken;
        }

        [HttpGet]
        [Route("practice/{id:int}")]
        public async Task<IActionResult> Index(int id)
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var unit = await _service.GetUnit(id);
                return View(unit);
            }
            ViewBag.Message = "Bạn phải đăng nhập để có thể làm bài kiểm tra này";
            return View();
        }

        [HttpGet]
        [Route("practice/getNextQuestion/{id:int}")]
        public IActionResult NextQuestion(int id)
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                return ViewComponent("Question", id);
            }
            return RedirectToRoute("homepage");
        }

        [HttpPost]
        [Route("submitAnswer")]
        public async Task<IActionResult> SubmitAnswer([FromBody]Question question)
        {
            question.QuestionContent = question.QuestionContent ?? "";
            question.SubjectId = question.SubjectId ?? 0;
            question.ClassId = question.ClassId ?? 0;
            question.TopicId = question.TopicId ?? 0;
            question.UnitId = question.UnitId ?? 0;            
            var result = await _service.SubmitAnswer(question);
            return Ok(result);
        }
    }
}