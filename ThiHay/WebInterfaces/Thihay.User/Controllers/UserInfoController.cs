﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.User.Controllers
{
    public class UserInfoController : BaseController
    {
        private readonly IEndUserService _endUserService;
        private readonly IIdentityService _identityService;
        private readonly string _token;
        public UserInfoController(IEndUserService endUserService, IIdentityService identityService, ITokenModel token)
        {
            _endUserService = endUserService;
            _identityService = identityService;
            _token = token.AccessToken;
        }

        [HttpGet]
        [Route("information")]
        public async Task<IActionResult> Index()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return View(userInfo);
                }
            }
            return RedirectToRoute("register");

            //var province = _identityService.GetAllProvince();
            //var village = _identityService.GetAllVillage();
            //var sch


        }

        [HttpGet]
        [Route("profile/{subjectId:int?}")]
        public async Task<IActionResult> Profile(int? subjectId)
        {
            var subjectAsync = _endUserService.GetAllSubject();

            var userProfileAsync = _endUserService.GetUserProfile(subjectId);
            await Task.WhenAll(subjectAsync, userProfileAsync);

            var profile = userProfileAsync.Result;

            ViewBag.Subjects = subjectAsync.Result.Select(s => new Subject { Id = s.Id, SubjectName = s.SubjectName });
            if (profile.Marks.Count == 0 && subjectId.HasValue)
            {
                profile.SubjectId = subjectId.Value;
            }
            return View(profile);
        }

        [HttpGet]
        [Route("changepassword")]
        public async Task<IActionResult> ChangePassword()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return View();
                }
            }
            return RedirectToRoute("register");
        }
        [HttpPost]
        [Route("changepasswordpost")]
        public async Task<IActionResult> ChangePassword([FromForm]ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                var result = await _identityService.ChangePassword(model);
                if (result.Success)
                {
                    return RedirectToRoute("profile");
                }
                else
                {
                    ViewBag.Error = result.Messages;
                }
            }
            return View();
        }

        [HttpGet]
        [Route("register")]
        public async Task<IActionResult> Register()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return RedirectToRoute("information");
                }
            }
            RegisterModel model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        [Route("registerpost")]
        public async Task<IActionResult> Register([FromForm]RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                ExecutionResult<string> result = await _identityService.RegisterNewUser(model);
                if (result.Success && !string.IsNullOrWhiteSpace(result.Result))
                {
                    Response.Cookies.Append("access_token", result.Result);
                    return RedirectToRoute("homepage");
                }
                ViewBag.Error = result.Messages;
            }
            ViewBag.Error = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage);
            return View(model);
        }

        [HttpGet]
        [Route("vipinfo")]
        public IActionResult Vip()
        {
            return View();
        }

        [HttpGet]
        [Route("logout")]
        public IActionResult Logout()
        {
            //var logout = await _identityService.Logout
            Response.Cookies.Delete("access_token");
            return RedirectToRoute("homepage");
        }


        [HttpGet]
        [Route("login")]
        public async Task<IActionResult> Login()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return RedirectToRoute("information");
                }
            }
            return View();
        }
        [HttpPost]
        [Route("loginpost")]
        public async Task<IActionResult> Login([FromForm]LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var accessToken = await _identityService.Login(model.UserName, model.Password);
                if (accessToken.Success && accessToken.Result != null)
                {
                    Response.Cookies.Append("access_token", accessToken.Result.AccessToken);
                    return RedirectToRoute("homepage");
                }
                ViewBag.Error = "Thông tin đăng nhập không chính xác";
            }
            return View();
        }

        //[HttpGet]
        //[Route("facebookSignIn")]
        //public async Task<IActionResult> FacebookSignIn([FromQuery] string fbaccesstoken)
        //{

        //    var accessToken = await _identityService.FacebookLogin(fbaccesstoken);
        //    if (!string.IsNullOrWhiteSpace(accessToken))
        //    {
        //        Response.Cookies.Append("access_token", accessToken);
        //    }

        //    return RedirectToRoute("homepage");
        //}

        //[HttpGet]
        //[Route("googleSignIn")]
        //public async Task<IActionResult> GoogleSignIn([FromQuery] string googletoken)
        //{
        //    var accessToken = await _identityService.GoogleLogin(googletoken);
        //    if (!string.IsNullOrWhiteSpace(accessToken))
        //    {
        //        Response.Cookies.Append("access_token", accessToken);
        //    }

        //    return RedirectToRoute("homepage");
        //}
    }
}