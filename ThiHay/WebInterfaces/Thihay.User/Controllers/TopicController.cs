using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.User.ViewModel;

namespace Thihay.User.Controllers
{
    public class TopicController : BaseController
    {
        private const int GROUP_COLUMNS = 3;
        private readonly IEndUserService _endUserService;

        public TopicController(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        [HttpGet]
        [Route("[Controller]/subject/{subjectId:int}/class/{classId:int?}")]
        public async Task<IActionResult> Index(int subjectId, int? classId)
        {
            if (!classId.HasValue)
            {
                var classes = await _endUserService.GetClasses(subjectId);
                var firstClassed = classes.OrderBy(c => c.Id).FirstOrDefault();

                classId = firstClassed?.Id ?? 0;
            }
            var topics = await _endUserService.GetTopics(subjectId, classId.Value);

            var topicsPerGroup = (topics.Count() / GROUP_COLUMNS) + 1;
            string className = topics.FirstOrDefault()?.ClassName;
            string subjectName = topics.FirstOrDefault()?.SubjectName;

            var topicViewModel = new List<TopicViewModel>();
            for (int i = 0; i < GROUP_COLUMNS; i++)
            {
                topicViewModel.Add(new TopicViewModel
                {
                    Topics = topics.Skip(i * topicsPerGroup).Take(topicsPerGroup).ToList()
                });
            }

            return View(new ClassTopicViewModel
            {
                ClassId = classId.Value,
                ClassName = className,
                SubjectId = subjectId,
                SubjectName = subjectName,
                TopicsGroup = topicViewModel
            });
        }
    }
}