﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.User.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IEndUserService _endUserService;

        public HomeController(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }
        [HttpGet]
        [Route("")]
        [Route("homepage")]
        public async Task<IActionResult> Index()
        {
            var summary = await _endUserService.GetSummary();
            return View(summary.Where(s => s.SubjectSummary.Any(d => d.TotalUnit > 0)));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}