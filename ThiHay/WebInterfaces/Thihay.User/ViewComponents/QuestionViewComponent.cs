﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;
using Thihay.Common;
using Thihay.User.ViewModel;

namespace Thihay.User.ViewComponents
{
    [ViewComponent(Name = "Question")]
    public class QuestionViewComponent : ViewComponent
    {
        private readonly IEndUserService _endUserService;

        public QuestionViewComponent(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int unitId)
        {
            var randomQuestion = await _endUserService.GetRandomQuestion(unitId, 0);
            if (randomQuestion != null)
            {
                randomQuestion.QuestionContent = WebUtility.HtmlDecode(randomQuestion.QuestionContent);
                switch (randomQuestion.QuestionType)
                {
                    case API.Client.QuestionType.MultipleChoice:
                        return MultipleChoiceViewComponent(randomQuestion);

                    case API.Client.QuestionType.ReadingComprehension:
                        return ReadingComprehensionViewComponent(randomQuestion);

                    case API.Client.QuestionType.FillInTheBlank:
                        return FillInTheBlankViewComponent(randomQuestion);

                    case API.Client.QuestionType.RewriteTheSentences:
                        return RewriteTheSentencesViewComponent(randomQuestion);

                    case API.Client.QuestionType.ClozeTest:
                        return ClozeTestViewComponent(randomQuestion);

                    case API.Client.QuestionType.Matching:
                        return MatchingViewComponent(randomQuestion);

                    case API.Client.QuestionType.Arrange:
                        return ArrangeViewComponent(randomQuestion);

                    case API.Client.QuestionType.DragAndDrop:
                        return DragAndDropViewComponent(randomQuestion);

                    case API.Client.QuestionType.Groups:
                        return GroupsViewComponent(randomQuestion);

                    case API.Client.QuestionType.Underline:
                        return UnderlineViewComponent(randomQuestion);

                    case API.Client.QuestionType.UnderlineAndRewrite:
                        return UnderlineAndRewriteViewComponent(randomQuestion);

                    case API.Client.QuestionType.DeleteCharacter:
                        return DeleteCharacterViewComponent(randomQuestion);

                    default:
                        break;
                }
            }
            return View();
        }

        private IViewComponentResult DeleteCharacterViewComponent(Question randomQuestion)
        {
            return View("DeleteCharacter");
        }

        private IViewComponentResult UnderlineAndRewriteViewComponent(Question randomQuestion)
        {
            return View("UnderlineAndRewrite");
        }

        private IViewComponentResult UnderlineViewComponent(Question randomQuestion)
        {
            var questionDetail = randomQuestion.QuestionDetail;
            randomQuestion.QuestionContent = Regex.Replace(randomQuestion.QuestionContent, @"\{(.*?)\}", "");
            foreach (var question in questionDetail)
            {
                var questionItem = question.QuestionDetailContent.Split(' ').Select(s =>
                {
                    var answerId = question.Answers.Where(a => a.AnswerContent == s).Select(a => a.AnswerId).FirstOrDefault();
                    return $"<span data-answer-id='{answerId}' class=\"p-type-10\" onclick=\"play_10(this)\">{s}</span>";
                });
                question.QuestionDetailContent = string.Join("", questionItem);
            }

            return View("Underline", randomQuestion);
        }

        private IViewComponentResult GroupsViewComponent(Question randomQuestion)
        {
            var groupQuestion = new GroupQuestionModel
            {
                QuestionId = randomQuestion.Id,
                QuestionType = randomQuestion.QuestionType,
                QuestionContent = randomQuestion.QuestionContent,
                QuestionExplanation = randomQuestion.QuestionExplanation,
                Questions = Helper.Shuffle(randomQuestion.QuestionDetail),
                Answers = Helper.Shuffle(randomQuestion.QuestionDetail.SelectMany(q => q.Answers).ToArray())
            };
            return View("Groups", groupQuestion);
        }

        private IViewComponentResult DragAndDropViewComponent(Question randomQuestion)
        {
            string content = randomQuestion.QuestionContent;
            Regex rgx = new Regex("{[0-9]+}");
            //string pattern = @"{.*}";
            string pattern = @"{[0-9]+}";
            Match m = Regex.Match(content, pattern);
            while (m.Success)
            {
                m = m.NextMatch();
            }

            foreach (var question in randomQuestion.QuestionDetail)
            {
                var questionRegx = question.QuestionDetailContent.Replace("{", @"\{").Replace("}", @"\}");
                var regex = new Regex(questionRegx);
                randomQuestion.QuestionContent = regex.Replace(randomQuestion.QuestionContent, $"<span data-question-id='{question.QuestionDetailId}' class=\"blank8\" ></span>", 1);
            }

            //var errorContentRgx = new Regex(@"\[vn\$(.*?)\$\]");
             //content8 = rgx.Replace(content8, "<input type=\"text\" class=\"blank8\" disabled value=\"\"></input>");
            //randomQuestion.QuestionContent = rgx.Replace(content, "<span class=\"blank8\" ></span>");
            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("[vn$", "").Replace("$]", "");

            return View("DragAndDrop", randomQuestion);
        }

        private IViewComponentResult ArrangeViewComponent(Question randomQuestion)
        {
            randomQuestion.QuestionDetail = Helper.Shuffle(randomQuestion.QuestionDetail.ToArray());
            return View("Arrange", randomQuestion);
        }

        private IViewComponentResult MatchingViewComponent(Question randomQuestion)
        {
            var machingQuestion = randomQuestion.QuestionDetail.Select(q => new
            {
                Question = new QuestionDetail
                {
                    Id = q.Id,
                    QuestionDetailContent = q.QuestionDetailContent,
                    QuestionDetailId = q.QuestionDetailId,
                    QuestionId = q.QuestionId
                },
                Answer = new Answer
                {
                    AnswerContent = q.Answers.FirstOrDefault().AnswerContent,
                    AnswerId = q.Answers.FirstOrDefault().AnswerId,
                    AnswerIndex = q.Answers.FirstOrDefault().AnswerIndex,
                    IsCorrectAnswer = q.Answers.FirstOrDefault().IsCorrectAnswer
                }
            });
            var left = machingQuestion.Select(q => q.Question).ToArray();
            var right = machingQuestion.Select(q => q.Answer).ToArray();
            left = Helper.Shuffle(left).ToArray();
            right = Helper.Shuffle(right).ToArray();
            return View("Matching", new MatchingQuestion
            {
                QuestionId = randomQuestion.Id,
                QuestionContent = randomQuestion.QuestionContent,
                QuestionExplanation = randomQuestion.QuestionExplanation,
                QuestionType = randomQuestion.QuestionType,
                Question = left,
                Answer = right
            });
        }

        private IViewComponentResult ClozeTestViewComponent(Question randomQuestion)
        {
            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("->", "<i class='fa fa-hand-o-right' aria-hidden='true'></i>");
            var questionContent = randomQuestion.QuestionContent;
            foreach (var node in randomQuestion.QuestionDetail)
            {
                string dropdown = $"<select class='ThSelect' data-question-detail-id='{node.QuestionDetailId}'>";
                dropdown += string.Join("", node.Answers.Select(s =>
                {
                    var answer = s.AnswerContent.Replace("\"", "&quot;");
                    return $"<option value='{s.AnswerId}'>{s.AnswerContent}</option>";
                }));
                dropdown += "</select>";
                questionContent = Helper.ReplaceFirst(questionContent, "{}", dropdown);
            }
            randomQuestion.QuestionContent = questionContent;



            return View("ClozeTest", randomQuestion);
        }

        private IViewComponentResult RewriteTheSentencesViewComponent(Question randomQuestion)
        {
            return FillInTheBlankViewComponent(randomQuestion);
        }

        private IViewComponentResult FillInTheBlankViewComponent(Question randomQuestion)
        {
            //var regex = @"\{(.*?)\}";
            Regex rgx = new Regex("{}");
            //            while (rgx.IsMatch(randomQuestion.QuestionContent))
            //            {
            //                var guid = Guid.NewGuid();
            //                randomQuestion.QuestionContent = rgx.Replace(randomQuestion.QuestionContent, $@"
            //<span class ='input-math' data-answer-guid='{guid}' id='math-field-{guid}'></span>
            //<input type='hidden' id='value-field-{guid}'>", 1);
            //            }
            bool isMath = false;
            var questionDetails = randomQuestion.QuestionDetail;
            foreach (var question in questionDetails)
            {
                var questionDetailId = question.QuestionDetailId;
                var replaceContent = $"<input id='value-field-{questionDetailId}' data-question-id='{questionDetailId}' type='text' class='Thblank question-input' required='true' />";

                var numberRgx = new Regex("[0-9]+");
                var latexRgx = new Regex(@"\$(.*?)\$");
                var answers = question.Answers.FirstOrDefault();

                if (answers != null && (numberRgx.IsMatch(answers.AnswerContent) || latexRgx.IsMatch(answers.AnswerContent)))
                {
                    isMath = true;
                    replaceContent = $@"
<span class ='input-math' data-question-id='{questionDetailId}' id='math-field-{questionDetailId}'></span>
<input type='hidden' class='question-input' data-question-id='{questionDetailId}' id='value-field-{questionDetailId}'>";
                }
                randomQuestion.QuestionContent = rgx.Replace(randomQuestion.QuestionContent, replaceContent, 1);
            }


            //            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("{}", $@"
            //<span class ='input-math' data-question-id='{randomQuestion.Id}' id='math-field-{randomQuestion.Id}'></span>
            //<input type='hidden' id='value-field-{randomQuestion.Id}'>");
            //randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("{}", "<input type='text' class='Thblank' required='true' onkeypress='autolen(this)'/>");
            string mmlBlank = "<semantics><annotation-xml encoding=\"application/xhtml+xml\"><input xmlns=\"http://www.w3.org/1999/xhtml\"  class=\"Thblank\" onkeypress=\"autolen(this)\"  type=\"text\"/></annotation-xml></semantics>";
            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("<mo>{</mo><mo>}</mo>", mmlBlank);

            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("->", "<i class='fa fa-hand-o-right' aria-hidden='true'></i>");
            randomQuestion.QuestionContent = randomQuestion.QuestionContent.Replace("-&gt;", "<i class='fa fa-hand-o-right' aria-hidden='true' style='color: blue;'></i>");
            ViewBag.IsMath = isMath;
            return View("FillInTheBlank", randomQuestion);
        }

        private IViewComponentResult ReadingComprehensionViewComponent(Question randomQuestion)
        {
            return MultipleChoiceViewComponent(randomQuestion);
        }

        private IViewComponentResult MultipleChoiceViewComponent(Question randomQuestion)
        {
            return View("MultipleChoice", randomQuestion);
        }
    }
}