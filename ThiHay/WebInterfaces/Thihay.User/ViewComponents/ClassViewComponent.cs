﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.User.ViewModel;

namespace Thihay.User.ViewComponents
{
    [ViewComponent(Name = "ClassList")]
    public class ClassViewComponent : ViewComponent
    {
        private readonly IEndUserService _endUserService;

        public ClassViewComponent(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int subjectId, int? classId)
        {
            var classes = await _endUserService.GetClasses(subjectId);
            return View(classes.Select(c => new ClassTopicViewModel
            {
                ClassId = c.Id,
                ClassName = c.ClassName,
                SubjectId = subjectId,
                IsActive = c.Id == classId
            }));
        }
    }
}