﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.User.ViewComponents
{
    [ViewComponent(Name = "User")]
    public class UserViewComponent : ViewComponent
    {
        private readonly IEndUserService _endUserService;
        private readonly IIdentityService _identityService;
        private readonly string _token;
        public UserViewComponent(IEndUserService endUserService, IIdentityService identityService, ITokenModel token)
        {
            _endUserService = endUserService;
            _token = token.AccessToken;
            _identityService = identityService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return View(userInfo);
                }
            }
            return View("NotLogin");

        }
    }
}