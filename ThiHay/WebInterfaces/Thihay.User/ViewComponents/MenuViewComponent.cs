﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.User.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IEndUserService _endUserService;

        public MenuViewComponent(IEndUserService endUserService)
        {
            _endUserService = endUserService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var subjects = await _endUserService.GetAllSubject();
            return View(subjects);
        }
    }
}