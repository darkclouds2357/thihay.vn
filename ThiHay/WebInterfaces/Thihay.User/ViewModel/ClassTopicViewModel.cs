﻿using System.Collections.Generic;
using Thihay.API.Client.Models;

namespace Thihay.User.ViewModel
{
    public class ClassTopicViewModel
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public bool IsActive { get; set; }
        public IList<TopicViewModel> TopicsGroup { get; set; }
    }

    public class TopicViewModel
    {
        public IList<Topic> Topics { get; set; }
    }
}