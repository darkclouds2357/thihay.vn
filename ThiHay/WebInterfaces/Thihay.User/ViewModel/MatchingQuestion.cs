﻿using System.Collections.Generic;
using Thihay.API.Client;
using Thihay.API.Client.Models;

namespace Thihay.User.ViewModel
{
    public class MatchingQuestion
    {
        public string QuestionContent { get; set; }
        public string QuestionExplanation { get; set; }
        public int QuestionId { get; set; }
        public QuestionType QuestionType { get; set; }
        public IList<QuestionDetail> Question { get; set; }

        public IList<Answer> Answer { get; set; }
    }
}