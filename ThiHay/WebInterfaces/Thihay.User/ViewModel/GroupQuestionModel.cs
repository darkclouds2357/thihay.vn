﻿using System.Collections.Generic;
using Thihay.API.Client;
using Thihay.API.Client.Models;

namespace Thihay.User.ViewModel
{
    public class GroupQuestionModel
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public string QuestionExplanation { get; set; }
        public QuestionType QuestionType { get; set; }
        public IList<QuestionDetail> Questions { get; set; }

        public IList<Answer> Answers { get; set; }
    }
}