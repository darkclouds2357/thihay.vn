﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.User.ViewModel
{
    public class ClassViewModel
    {
        public int SubjectId { get; set; }
        public int ClassId { get; set; }
        public IEnumerable<Class> Class { get; set; }
        public IEnumerable<TopicViewModel> TopicGroup { get; set; }
    }
    public class TopicViewModel
    {
        public IEnumerable<Topic> Topics { get; set; }
    }
}
