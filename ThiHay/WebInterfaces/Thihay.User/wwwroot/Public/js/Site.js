﻿function LTrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
function RTrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
function Trim(str, chars) {
    return LTrim(RTrim(str, chars), chars);
}
function AlertNoRedirect(text) {
    $.jAlert({
        'title': 'THÔNG BÁO',
        'content': '<p class ="front-alert"><i class="fa fa-exclamation-triangle fa-4x"></i>' + text + '</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
        'btns': { 'text': 'OK' }
    });
    //$.fn.jAlert.defaults.showAnimation = 'flash';
    ////$.fn.jAlert.defaults.hideAnimation = 'rollOut';
    return false;
}
function AlertRedirect(text, url) {
    $.jAlert({
        'title': 'THÔNG BÁO',
        'content': '<p class ="front-alert"><i class="fa fa-exclamation-triangle fa-4x"></i>' + text + '</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
        'btns': {
            'text': 'OK',
            'onClick': function (e, btn) {
                window.location.href = url;
            }
        }
    });
    //$.fn.jAlert.defaults.showAnimation = 'flash';
    ////$.fn.jAlert.defaults.hideAnimation = 'rollOut';
    return false;
}

function WaitingBox(text) {
    $.jAlert({
        'content': '<p class ="front-alert"><i class="fa fa-spinner fa-4x"></i>' + text + '</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
    });
    return false;
}

window.onload = function () {
    //if (window.location.href.indexOf('UserInfo') > 0) {
    //    LoadDayDDL();
    //    LoadCity();
    //    UserInfo_active();
    //}
    //if (window.location.href.indexOf('profile') > 0) {
    //    LoadDayDDL1();
    //    Profile_active();
    //    Gradebook(1);
    //}
    //if (window.location.href.indexOf('English.aspx') > 0) {
    //    English_active();
    //}
    //if (window.location.href.indexOf('Math.aspx') > 0) {
    //    English_active();
    //}
    //if (window.location.href.indexOf('Toan.aspx') > 0) {
    //    English_active();
    //}
    //if (window.location.href.indexOf('Play.aspx') > 0) {
    //    NextQuestion();
    //    $("[class='block-21']").find("li").find("a").html(localStorage['class']);
    //    $("[class='block-21']").find("li").find("a").attr("href", localStorage['url']);
    //    $("#nameunit").html(localStorage['unit']);
    //    TimeUp();
    //    $('body').disableTextSelect();
    //}
    //if (window.location.href.indexOf('Log.aspx') > 0) {
    //    LogHeader();
    //    localStorage['No'] = 0;
    //    LoadLog();
    //    $("#postback").attr("href", localStorage['logUrl']);
    //    $(window).scroll(function () {
    //        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    //            LoadLog();
    //        }
    //    });
    //}
    //if (window.location.href.indexOf('Register.aspx?type=loginG') > 0) {
    //    RegisterG();
    //}
    $("#sortable").sortable();
    $("#sortable").disableSelection();
};

function English_active() {
    var url = window.location.href;
    var arr = url.split('=');
    group = $("[class='block-14']").find("ul").find("li").find("a");
    i = 1;
    group.removeClass("active");
    group.each(function () {
        if (arr[1] === i) {
            $(this).addClass("active");
        }
        i++;
    });
}

function TimeUp() {
    timer = parseInt($("#timer-in-second").val());
    sec = parseInt($("#seconds").text());
    min = parseInt($("#minutes").text());
    hour = parseInt($("#hours").text());
    sec = sec + 1;
    timer = timer + 1;
    if (sec === 60) {
        min += 1;
        sec = 0;
    }
    if (min === 60) {
        hour += 1;
        min = 0;
    }
    if (sec < 10) { vsec = '0' + sec; } else { vsec = sec; }
    if (min < 10) { vmin = '0' + min; } else { vmin = min; }
    if (hour < 10) { vhour = '0' + hour; } else { vhour = hour; }
    $("#seconds").text(vsec);
    $("#minutes").text(vmin);
    $("#hours").text(vhour);
    $("#timer-in-second").val(timer);
    setTimeout("TimeUp()", 1000);
}
jQuery.fn.disableTextSelect = function () {
    return this.each(function () {
        $(this).css({
            'MozUserSelect': 'none',
            'webkitUserSelect': 'none'
        }).attr('unselectable', 'on').bind('selectstart', function () {
            return false;
        });
    });
};
jQuery.fn.enableTextSelect = function () {
    return this.each(function () {
        $(this).css({
            'MozUserSelect': '',
            'webkitUserSelect': ''
        }).attr('unselectable', 'off').unbind('selectstart');
    });
};
function drawChart() {
    var chart = new CanvasJS.Chart("pie",
        {
            title: {
                text: "Tháng 3 Năm 2010",
                fontSize: 30
            },
            animationEnabled: true,
            axisX: {
                gridColor: "Silver",
                tickColor: "silver",
                valueFormatString: "DD"
            },
            toolTip: {
                shared: true
            },
            theme: "theme4",
            axisY: {
                gridColor: "Silver",
                tickColor: "silver"
            },
            //legend: {
            //    verticalAlign: "center",
            //    horizontalAlign: "right"
            //},
            data: [
                {
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    name: "Câu đã làm",
                    markerType: "square",
                    color: "#F08080",
                    dataPoints: [
                        { x: new Date(2010, 0, 1), y: 60 },
                        { x: new Date(2010, 0, 2), y: 70 },
                        { x: new Date(2010, 0, 3), y: 210 },
                        { x: new Date(2010, 0, 4), y: 65 },
                        { x: new Date(2010, 0, 5), y: 73 },
                        { x: new Date(2010, 0, 6), y: 0 },
                        { x: new Date(2010, 0, 7), y: 0 },
                        { x: new Date(2010, 0, 8), y: 85 },
                        { x: new Date(2010, 0, 9), y: 89 },
                        { x: new Date(2010, 0, 10), y: 43 },
                        { x: new Date(2010, 0, 11), y: 0 },
                        { x: new Date(2010, 0, 11), y: 0 },
                        { x: new Date(2010, 0, 12), y: 0 },
                        { x: new Date(2010, 0, 13), y: 0 },
                        { x: new Date(2010, 0, 14), y: 0 },
                        { x: new Date(2010, 0, 15), y: 0 },
                        { x: new Date(2010, 0, 16), y: 20 },
                        { x: new Date(2010, 0, 17), y: 30 },
                        { x: new Date(2010, 0, 18), y: 40 },
                        { x: new Date(2010, 0, 19), y: 100 },
                        { x: new Date(2010, 0, 20), y: 0 },
                        { x: new Date(2010, 0, 21), y: 0 },
                        { x: new Date(2010, 0, 22), y: 40 },
                        { x: new Date(2010, 0, 23), y: 50 },
                        { x: new Date(2010, 0, 24), y: 60 },
                        { x: new Date(2010, 0, 25), y: 70 },
                        { x: new Date(2010, 0, 26), y: 30 },
                        { x: new Date(2010, 0, 27), y: 20 },
                        { x: new Date(2010, 0, 28), y: 10 },
                        { x: new Date(2010, 0, 29), y: 20 },

                    ]
                },
            ],
            legend: {
                verticalAlign: "center",
                horizontalAlign: "right",
                cursor: "pointer",
                itemclick: function (e) {
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    }
                    else {
                        e.dataSeries.visible = true;
                    }
                    chart.render();
                }
            }
        });

    chart.render();
}

function changpage(e) {
    $("div[class='paging']").find("a").removeClass("active");
    $(e).addClass("active");
    var i = parseInt($(e).text());
    i = (i - 1) * 10 + 1;
    Gradebook(i);
}
function nextpage() {
    var now = $("div[class='paging']").find("a[class='active']").parent();
    if (now.next().is("li:last-child")) {
        var node = $("div[class='paging']").find("li:first-child").next();
        changpage(node.find("a"));
        return false;
    }
    changpage(now.next().find("a"));
}
function prevpage() {
    var now = $("div[class='paging']").find("a[class='active']").parent();
    if (now.prev().is("li:first-child")) {
        var node = $("div[class='paging']").find("li:last-child").prev();
        changpage(node.find("a"));
        return false;
    }
    changpage(now.prev().find("a"));
}

function loginG() {
    var j = { "type": "login" };
    $.post("LoginG.aspx"
        , JSON.stringify(j)
        , function (data) {
            alert(data);
        });
}
function RegisterG() {
    $("[name='email']").attr('readonly', true);
    $("#boxLoginG").hide();
}
function voice_speak(e) {
    var voice = $(e).next("span[class='font-voice']").text();
    responsiveVoice.speak(voice);
}
function voice_speakvn(e) {
    var voice = $(e).next("span[class='font-voice']").text();
    responsiveVoice.speak(voice, "Vietnamese Male");
}