﻿function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function onSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
}
function onFailure(error) {
    console.log(error);
}
function showKeyBoard() {
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-key-pad"],
        function () {
            $("#math-key-pad").css("visibility", "");
        });
}
var QuestionType = {
    MultipleChoice: "MultipleChoice",
    ReadingComprehension: "ReadingComprehension",
    FillInTheBlank: "FillInTheBlank",
    RewriteTheSentences: "RewriteTheSentences",
    ClozeTest: "ClozeTest",
    Matching: "Matching",
    Arrange: "Arrange",
    DragAndDrop: "DragAndDrop",
    Groups: "Groups",
    Underline: "Underline",
    UnderlineAndRewrite: "UnderlineAndRewrite",
    DeleteCharacter: "DeleteCharacter"
}

function processQuestion(questionType, nextQuestion) {
    var isSuccess = false;
    var question = {};
    var questionDetails = [];
    var questionId = parseInt($("#question-id").data("question-id"));
    var answerTime = $("#timer-in-second").val();
    switch (questionType) {
        case QuestionType.MultipleChoice:
        case QuestionType.ReadingComprehension:
            questionDetails = processMultipleChoiceQuestion();
            break;
        case QuestionType.FillInTheBlank:
        case QuestionType.RewriteTheSentences:
            questionDetails = processFillInTheBlank();
            break;
        case QuestionType.ClozeTest:
            questionDetails = processClozeTest();
            break;
        case QuestionType.Matching:
            questionDetails = processMaching();
            break;
        case QuestionType.Arrange:
            questionDetails = processArrange();
            break;
        case QuestionType.DragAndDrop:
            questionDetails = processDragAndDrop();
            break;
        case QuestionType.Groups:
            questionDetails = processGroup();
            break;
        case QuestionType.Underline:
            questionDetails = processUnderline();
            break;
        case QuestionType.UnderlineAndRewrite:
        case QuestionType.DeleteCharacter:
        default:
            break;
    }
    question = {
        Id: questionId,
        QuestionDetail: questionDetails,
        AnswerTime: answerTime
    };
    $.ajax({
        type: "POST",
        url: "/submitAnswer",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(question),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $("#timer-in-second").val(0);
                var correctCnt = data.result;
                var message = data.messages[0];
                nextQuestion(correctCnt, message);
            }            
        },
    });
}

function processUnderline() {
    var questions = [];
    $(".subQues").each(function () {
        var questionDetailId = $(this).data("question-detail-id");
        var answers = [];
        $(this).find('span.p-type-10').each(function () {
            var answerdId = $(this).data('answer-id');
            var isChecked = $(this).hasClass("check-10");
            if (answerdId) {
                var answer = {
                    AnswerId: answerdId,
                    IsCorrectAnswer: isChecked
                };
                answers.push(answer);
            }
        });
        var question = {
            QuestionDetailId: questionDetailId,
            Answers: answers
        };
        questions.push(question);
    });
    return questions;
}
function processGroup() {
    var questions = [];
    $(".Td-Drog-9").each(function () {
        var questionId = $(this).data("question-detail-id");
        var answers = [];
        $(this).find('.Th-Row-9 p').each(function () {
            var answerId = $(this).attr('data-answer-id');
            if (answerId) {
                var answer = {
                    AnswerId: answerId
                };
                answers.push(answer);
            }
        });
        var question = {
            QuestionDetailId: questionId,
            Answers: answers
        };
        questions.push(question);
    });
    return questions;
}
function processDragAndDrop() {
    var questions = [];
    $(".blank8").each(function () {
        var questionId = $(this).data("question-id");
        var answeredId = $(this).data("answer-id");
        if (answeredId) {
            var question = {
                QuestionDetailId: questionId,
                Answers: [
                    {
                        AnswerId: answeredId
                    }
                ]
            };
            questions.push(question);
        }
    });
    return questions;
}
function processArrange() {
    var questions = [];
    var order = 1;
    $("#ThOrder .rowOrder").each(function (index, el) {
        var questionId = $(this).data("question-id");
        var answeredOrder = index + 1;
        var question = {
            QuestionDetailId: questionId,
            Answers: [
                {
                    AnswerIndex: answeredOrder
                }
            ]
        }
        questions.push(question);
    });
    return questions;
}
function processMaching() {
    var questions = [];
    $("#SourceDD .LeftDD").each(function () {
        var questionId = $(this).data("question-detail-id");
        var answeredId = $("#M-" + questionId).find('p.answer-match').data("answer-id");
        if (answeredId) {
            var question = {
                QuestionDetailId: questionId,
                Answers: [
                    {
                        AnswerId: answeredId
                    }
                ]
            };
            questions.push(question);
        }
    });

    return questions;
}
function processClozeTest() {
    var questions = [];
    $(".ThSelect").each(function () {
        var questionId = $(this).data("question-detail-id");
        var selectAnswer = $(this).find(':selected').val();
        var question = {
            QuestionDetailId: questionId,
            Answers: [
                {
                    AnswerId: selectAnswer
                }
            ]
        };
        questions.push(question);
    });

    return questions;
}


function processFillInTheBlank() {
    var questions = [];
    $("#question-id input.question-input").each(function (index, elm) {
        var questionDetailId = $(this).data("question-id");
        var answer = $(this).val();
        var question = {
            QuestionDetailId: questionDetailId,
            Answers: [
                {
                    AnswerContent: answer
                }
            ]
        };
        questions.push(question);
    });
    return questions;
}

function processMultipleChoiceQuestion() {
    var questions = [];
    $(".question-content").each(function (index, elm) {
        var questionDetailElm = $(this).find('input[type=hidden][class=question-detail-id]')
        var questionDetailId = $(questionDetailElm).val();
        var answers = [];
        $(this).find('table[class=subAns] tr.ThRowA td.checkA').each(function () {
            var answerIndex = $(this).data("answer-index");
            var isSelected = $(this).data("value");
            var answerId = $(this).data("answer-id");
            var answer = {
                AnswerIndex: answerIndex, IsCorrectAnswer: isSelected, AnswerId: answerId
            };
            answers.push(answer);
        });
        var question = {
            QuestionDetailId: questionDetailId,
            Answers: answers
        };
        questions.push(question);
    });
    return questions;
}
//function renderButton() {
//      gapi.signin2.render('my-signin2', {
//        //'scope': 'profile email',
//        'width': 240,
//        'height': 50,
//        'longtitle': true,
//        'theme': 'dark',
//        'onsuccess': onSuccess,
//        'onfailure': onFailure
//      });
//}


$(document).ready(function () {
    $(".class-style").each(function () {
        $(this).css("background-color", getRandomColor())
    });
    //renderButton();
    //$(".topic-menu").load("/question/type/@questionType/Detail/@Model.Id", function (response, status, xhr) {
    //    ConvertCKE();
    //});


})