using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Thihay.Admin.Controllers
{
    [Produces("application/json")]
    [Route("api/Dropdown")]
    public class DropdownController : Controller
    {

        [HttpGet]
        [Route("topic")]
        public IActionResult RefreshTopicDropdown([FromQuery]int? subjectId, [FromQuery]int? classId)
        {
            return ViewComponent("TopicDropdown", new { classId, subjectId });
        }

        [HttpGet]
        [Route("unit")]
        public IActionResult RefreshUnitDropdown([FromQuery]int? subjectId, [FromQuery]int? classId, [FromQuery]int? topicId)
        {
            return ViewComponent("UnitDropdown", new { classId, subjectId, topicId });
        }
    }
}