﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly IIdentityService _identityService;
        private readonly ITokenModel _tokenModel;
        public HomeController(IIdentityService identityService, ITokenModel token)
        {
            _identityService = identityService;
            _tokenModel = token;
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var accessToken = await _identityService.Login(model.UserName, model.Password, true);
                if (accessToken.Success && accessToken.Result != null && accessToken.Result.IsAdmin)
                {
                    //Response.Cookies.Append("access_token", accessToken.Result.AccessToken);
                    HttpContext.Session.SetString("access_token", accessToken.Result.AccessToken);
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.Error = "Thông tin đăng nhập không chính xác";
            }
            return View();
        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("access_token");
            return RedirectToAction("Login");
        }

        [HttpGet]
        public async Task<IActionResult> ChangePassword()
        {
            if (!string.IsNullOrWhiteSpace(_tokenModel?.AccessToken) && _tokenModel?.IsAdmin == true)
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return View();
                }
            }
            return RedirectToAction("Login");
        }



        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromForm]ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                var result = await _identityService.ChangePassword(model);
                if (result.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = result.Messages;
                }
            }
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}