using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class TopicController : Controller
    {
        private readonly IChuyenDeServices _services;
        private readonly IClassServices _classServices;
        private readonly ISubjectServices _subjectServices;

        public TopicController(IChuyenDeServices services,
            IClassServices classServices,
            ISubjectServices subjectServices)
        {
            _services = services;
            _classServices = classServices;
            _subjectServices = subjectServices;
        }

        public async Task<IActionResult> Index([FromQuery]string searchKey, [FromQuery] int? subjectId, [FromQuery]int? classId, [FromQuery]int? page = 1, [FromQuery]int? pageSize = 10)
        {
            var topics = await _services.GetAllTopics(searchKey, subjectId, classId, page, pageSize);
            
            ViewBag.TotalPage = topics.TotalPage;
            ViewBag.Page = page ?? 0;

            ViewBag.Search = string.IsNullOrWhiteSpace(searchKey) ? string.Empty : searchKey;

            return View(topics.Topics);
        }

        [HttpGet]
        [Route("[Controller]/detail/{topicId:int?}")]
        public async Task<IActionResult> Detail(int? topicId)
        {
            
            if (topicId.HasValue)
            {
                var topic = await _services.GetTopicDetail(topicId.Value);
                
                return View(topic);
            }
            else
            {
                return View(new Topic());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Update(Topic topic)
        {
            if (ModelState.IsValid)
            {
                var isUpdated = await _services.UpdateTopic(topic);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Detail", topic.Id);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(int topicId, bool isDelete)
        {
            var isChanged = await _services.ChangeTopicStatus(topicId, isDelete);
            return RedirectToAction("Index");
        }
    }
}