using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class UnitController : Controller
    {
        private readonly IChuyenDeServices _services;
        private readonly IClassServices _classServices;
        private readonly ISubjectServices _subjectServices;

        public UnitController(IChuyenDeServices services, IClassServices classServices,
            ISubjectServices subjectServices)
        {
            _services = services;
            _classServices = classServices;
            _subjectServices = subjectServices;
        }

        public async Task<IActionResult> Index([FromQuery]string searchKey, [FromQuery] int? subjectId, [FromQuery]int? classId, [FromQuery] int? topicId, [FromQuery] int? page = 1, [FromQuery] int? pageSize = 10)
        {
            ViewBag.ClassId = classId;
            ViewBag.SubjectId = subjectId;
            ViewBag.TopicId = topicId;
            var units = await _services.GetAllUnits(searchKey, subjectId, classId, topicId, page, pageSize);
            ViewBag.TotalPage = units.TotalPage;
            ViewBag.Page = page ?? 0;

            ViewBag.Search = string.IsNullOrWhiteSpace(searchKey) ? string.Empty : searchKey;

            return View(units.Units);
        }

        [HttpGet]
        [Route("[Controller]/detail/{unitId:int?}")]
        public async Task<IActionResult> Detail(int? unitId)
        {

            var unit = new Unit();
            if (unitId.HasValue)
            {
                unit = await _services.GetUnitDetail(unitId.Value);
            }

            return View(unit);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Unit unit)
        {
            var isUpdated = await _services.UpdateUnit(unit);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(int unitId, bool isDelete)
        {
            var isChanged = await _services.ChangeUnitStatus(unitId, isDelete);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeOrder(int unitId, int newOrder)
        {
            var isChanged = await _services.UpdateUnitOrder(unitId, newOrder);
            return RedirectToAction("Index");
        }
    }
}