using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class QuestionController : Controller
    {
        private readonly IChuyenDeServices _services;
        private readonly IClassServices _classServices;
        private readonly ISubjectServices _subjectServices;
        private readonly IQuestionServices _questionServices;

        public QuestionController(IChuyenDeServices services, IClassServices classServices, ISubjectServices subjectServices, IQuestionServices questionServices)
        {
            _services = services;
            _classServices = classServices;
            _subjectServices = subjectServices;
            _questionServices = questionServices;
        }

        public async Task<IActionResult> Index([FromQuery]int? unitId, [FromQuery]string searchKey, [FromQuery] int? subjectId, [FromQuery]int? classId, [FromQuery] int? topicId, [FromQuery]QuestionType? questionType, [FromQuery] int? page = 1, [FromQuery] int? pageSize = 10)
        {
            ViewBag.ClassId = classId;
            ViewBag.SubjectId = subjectId;
            ViewBag.UnitId = unitId;
            ViewBag.TopicId = topicId;
            if (!unitId.HasValue && string.IsNullOrWhiteSpace(searchKey) && !questionType.HasValue && !subjectId.HasValue && !classId.HasValue && !topicId.HasValue)
            {
                ViewBag.TotalPage = 0;
                ViewBag.Page = page ?? 0;

                ViewBag.Search = string.Empty;
                return View();
            }

            var questions = await _questionServices.GetAllQuestion(searchKey, unitId, subjectId, classId, topicId, questionType, page, pageSize);

            var result = questions.Questions?.Select(q =>
            {
                var questionDetail = q.QuestionDetail.Select(qd =>
                {
                    var answers = qd.Answers.Select(a =>
                    {
                        a.AnswerContent = WebUtility.HtmlDecode(a.AnswerContent);
                        return a;
                    }).OrderBy(a => a.AnswerIndex).ToList();
                    qd.Answers = answers;
                    qd.QuestionDetailContent = WebUtility.HtmlDecode(qd.QuestionDetailContent);
                    return qd;
                }).ToList();
                if (questionDetail.All(qd => qd.Answers.Count == 1))
                {
                    questionDetail = questionDetail.OrderBy(qd => qd.Order).ThenBy(qd => qd.Answers.FirstOrDefault().AnswerIndex).ToList();
                }
                q.QuestionContent = WebUtility.HtmlDecode(q.QuestionContent);
                q.QuestionDetail = questionDetail;


                return q;
            }).ToList();

            ViewBag.TotalPage = questions.TotalPage;
            ViewBag.Page = page ?? 0;

            ViewBag.Search = string.IsNullOrWhiteSpace(searchKey) ? string.Empty : searchKey;

            return View(result);
        }

        [HttpGet]
        [Route("[Controller]/detail/{questionId:int?}")]
        public async Task<IActionResult> GetQuestionDetail(int? questionId, bool? isStay, int? classId, int? subjectId, int? topicId, int? unitId)
        {
            var question = new Question()
            {
                ClassId = classId,
                SubjectId = subjectId,
                TopicId = topicId,
                UnitId = unitId
            };
            bool isCreateNew = true;
            if (questionId.HasValue || questionId > 0)
            {
                isCreateNew = false;
                question = await _questionServices.GetQuestionDetail(questionId.Value);
                //IList<QuestionDetail> questionDetail = question.QuestionDetail.Select(qd =>
                //{
                //    var answers = qd.Answers.Select(a =>
                //    {
                //        a.AnswerContent = WebUtility.HtmlDecode(a.AnswerContent);
                //        return a;
                //    }).OrderBy(a => a.AnswerIndex).ToList();
                //    qd.Answers = answers;
                //    qd.QuestionDetailContent = WebUtility.HtmlDecode(qd.QuestionDetailContent);
                //    return qd;
                //}).ToList();
                //if (questionDetail.All(qd => qd.Answers.Count == 1))
                //{
                //    questionDetail = questionDetail.OrderBy(qd => qd.Answers.FirstOrDefault().AnswerIndex).ToList();
                //}
                //question.QuestionContent = WebUtility.HtmlDecode(question.QuestionContent);
                //question.QuestionDetail = questionDetail;
            }
            ViewBag.IsCreateNew = isCreateNew;
            ViewBag.IsStay = isStay ?? false;
            return View(question);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(int questionId, bool isDelete)
        {
            var isChanged = await _questionServices.ChangeQuestionStatus(questionId, isDelete);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("[Controller]/addmultiplechoice/answer")]
        public async Task<IActionResult> GetMultipleChoiceAnswer([FromQuery]int questionOrder, [FromQuery]int index)
        {
            return ViewComponent("MultipleChoiceAnswer", new { questionOrder = questionOrder, index = index, isCorrectAnswer = false });
        }

        [HttpGet]
        [Route("[Controller]/addmultiplechoice/question")]
        public async Task<IActionResult> GetMultipleChoiceQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("MultipleChoiceQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/addgroup/answer")]
        public async Task<IActionResult> GetGroupAnswer([FromQuery]int questionOrder, [FromQuery]int index)
        {
            return ViewComponent("GroupAnswer", new { questionOrder = questionOrder, index = index, isCorrectAnswer = false });
        }

        [HttpGet]
        [Route("[Controller]/addgroup/question")]
        public async Task<IActionResult> GetGroupQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("GroupQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/addfillintheblank/question")]
        public async Task<IActionResult> GetFillInTheBlankQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("FillInBlankQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/addclozetest/question")]
        public async Task<IActionResult> GetClozeTestQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("ClozeTestQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/addmatching/question")]
        public async Task<IActionResult> GetMatchingQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("MatchingQuestion", new { questionOrder = questionOrder });
        }
        [HttpGet]
        [Route("[Controller]/addarrange/question")]
        public async Task<IActionResult> GetArrangeQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("ArrangeQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/adddrapanddrop/question")]
        public async Task<IActionResult> GetDrapAndDropQuestion([FromQuery]int questionOrder)
        {
            return ViewComponent("DrapAndDropQuestion", new { questionOrder = questionOrder });
        }

        [HttpGet]
        [Route("[Controller]/addUnderlineQuestion/question")]
        public async Task<IActionResult> GetUnderlineQuestion([FromQuery]string questionContent, [FromQuery]int questionOrder)
        {
            return ViewComponent("UnderlineQuestion", new { questionContent = questionContent, questionOrder = questionOrder });
        }


        [HttpPost]
        public async Task<IActionResult> Update(Question question, QuestionDetail[] questionDetail, bool isStay)
        {
            if (ModelState.IsValid)
            {
                question.QuestionDetail = questionDetail;
                //question.QuestionContent = WebUtility.HtmlEncode(question.QuestionContent);
                var isUpdated = await _questionServices.UpdateQuestion(question);
                if (isStay)
                {
                    return RedirectToAction("GetQuestionDetail", new { isStay = isStay, classId = question.ClassId, subjectId = question.SubjectId, topicId = question.TopicId, unitId = question.UnitId });
                }
                return RedirectToAction("Index", new { unitId = question.UnitId, subjectId = question.SubjectId, classId = question.ClassId, topicId = question.TopicId, questionType = (int)question.QuestionType });
            }
            return View("GetQuestionDetail", question);
        }
        [HttpGet]
        [Route("[Controller]/type/{questionType}")]
        public async Task<IActionResult> GetQuestionByType(QuestionType questionType)
        {
            return ViewComponent("QuestionAnswer", new { questionType = questionType });
        }

        #region Unuse
        //[HttpGet]
        //[Route("[Controller]/type/{questionType:int}/Detail/{questionId:int?}")]
        //public async Task<IActionResult> GetQuestionByType(QuestionType questionType, int? questionId)
        //{
        //    var question = new Question();
        //    question.QuestionDetail = new List<QuestionDetail>();

        //    if (questionId.HasValue && questionId.Value > 0)
        //    {
        //        question = await _questionServices.GetQuestionDetail(questionId.Value);
        //    }
        //    switch (questionType)
        //    {
        //        case QuestionType.MultipleChoice:
        //            return RenderMultupleChoiceQuesion(question.QuestionDetail, questionType);

        //        case QuestionType.ReadingComprehension:
        //            return RenderReadingComprehensionQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.FillInTheBlank:
        //            return RenderFillInTheBlankQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.RewriteTheSentences:
        //            return RenderRewriteTheSentences(question.QuestionDetail, questionType);

        //        case QuestionType.ClozeTest:
        //            return RenderClozeTest(question.QuestionDetail, questionType);

        //        case QuestionType.Matching:
        //            return RenderMatchingQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.Arrange:
        //            return RenderArrangeQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.DragAndDrop:
        //            return RenderDragAndDropQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.Groups:
        //            return RenderGroupQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.Underline:
        //            return RenderUnderlineQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.UnderlineAndRewrite:
        //            return RenderUnderlineAndRewriteQuestion(question.QuestionDetail, questionType);

        //        case QuestionType.DeleteCharacter:
        //            return RenderDeleteCharacter(question.QuestionDetail, questionType);

        //        default:
        //            return RenderMultupleChoiceQuesion(question.QuestionDetail, questionType);
        //    }
        //}

        //private IActionResult RenderKeyPairValueQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    ViewBag.Type = questionType;
        //    return PartialView("_RenderKeyPairValueQuestion", questionDetail);
        //}

        //#region Key Pair Value Question Group

        //private IActionResult RenderDeleteCharacter(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderUnderlineAndRewriteQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderArrangeQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderMatchingQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderRewriteTheSentences(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderFillInTheBlankQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderKeyPairValueQuestion(questionDetail, questionType);
        //}

        //#endregion Key Pair Value Question Group

        //private IActionResult RenderSelectChoiceQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    ViewBag.Type = questionType;
        //    return PartialView("_RenderSelectChoiceQuestion", questionDetail);
        //}

        //#region Select Choice Question Group

        //private IActionResult RenderUnderlineQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderSelectChoiceQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderGroupQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderSelectChoiceQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderClozeTest(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderSelectChoiceQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderReadingComprehensionQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderSelectChoiceQuestion(questionDetail, questionType);
        //}

        //private IActionResult RenderMultupleChoiceQuesion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    return RenderSelectChoiceQuestion(questionDetail, questionType);
        //}

        //#endregion Select Choice Question Group

        //private IActionResult RenderDragAndDropQuestion(IList<QuestionDetail> questionDetail, QuestionType questionType)
        //{
        //    ViewBag.Type = questionType;
        //    return PartialView("_RenderDragAndDropQuestion", questionDetail);
        //}

        //[HttpGet]
        //[Route("[Controller]/dropdown/topic")]
        //public async Task<IActionResult> GetTopicByClassAndSubject([FromQuery] int? subjectId, [FromQuery]int? classId)
        //{
        //    var topics = await _services.GetAllTopics(null, subjectId, classId, null, null);

        //    ViewBag.Topics = topics.Topics.Select(u => new SelectListItem
        //    {
        //        Value = u.Id.ToString(),
        //        Text = u.TopicName
        //    });

        //    return PartialView("_GetTopicByClassAndSubject");
        //}

        //[HttpGet]
        //[Route("[Controller]/dropdown/unit")]
        //public async Task<IActionResult> GetUnitByClassAndSubject([FromQuery] int? subjectId, [FromQuery]int? classId, [FromQuery] int? topicId)
        //{
        //    var units = await _services.GetAllUnits(null, subjectId, classId, topicId, null, null);

        //    ViewBag.UnitId = units.Units.Select(u => new SelectListItem
        //    {
        //        Value = u.Id.ToString(),
        //        Text = u.NameUnit
        //    });

        //    return PartialView("_GetUnitByClassAndSubject");
        //}
        #endregion
    }
}