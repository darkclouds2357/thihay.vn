using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class ClassController : Controller
    {
        private readonly IClassServices _classServices;

        public ClassController(IClassServices classServices)
        {
            _classServices = classServices;
        }

        // GET: Class
        public async Task<IActionResult> Index(string searchKey)
        {
            var classes = await _classServices.GetClass(searchKey);
            return View(classes.Select(c => new Class
            {
                ClassName = WebUtility.HtmlDecode(c.ClassName),
                Id = c.Id,
                IsDeleted = c.IsDeleted
            }));
        }

        // POST: Class/Update
        [HttpPost]
        public async Task<IActionResult> Update(Class classDto)
        {
            var isUpdated = await _classServices.UpdateClass(classDto);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(int classId, bool isDelete)
        {
            var isChanged = await _classServices.ChangeClassStatus(classId, isDelete);
            return RedirectToAction("Index");
        }
    }
}