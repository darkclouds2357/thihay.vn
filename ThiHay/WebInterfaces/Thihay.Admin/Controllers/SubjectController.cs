using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.Controllers
{
    public class SubjectController : Controller
    {
        private readonly ISubjectServices _services;

        public SubjectController(ISubjectServices services)
        {
            _services = services;
        }

        // GET: Class
        public async Task<IActionResult> Index([FromQuery]string searchKey)
        {
            var subjects = await _services.GetSubject(searchKey);
            return View(subjects);
        }

        // GET: Class/UpdateClass
        [HttpPost]
        public async Task<IActionResult> Update(Subject subject)
        {
            var isUpdated = await _services.UpdateSubject(subject);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus(int subjectId, bool isDelete)
        {
            var isChanged = await _services.ChangeSubjectStatus(subjectId, isDelete);
            return RedirectToAction("Index");
        }
    }
}