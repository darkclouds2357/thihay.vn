using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Thihay.API.Client;
using System.IO;
using Thihay.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using ST.Library.Common.Constants;

namespace Thihay.Admin.Controllers
{
    public class UploaderController : Controller
    {
        private readonly string _uploadedUrl;
        private readonly IHostingEnvironment _hostingEnvironment;
        public UploaderController(IOptionsSnapshot<AppSettings> settings, IHostingEnvironment environment)
        {
            _uploadedUrl = settings.Value.UploadUrl;
            _hostingEnvironment = environment;
        }
        [Route("/uploader/brower")]
        public IActionResult Index()
        {

            var rootPath = _hostingEnvironment.WebRootPath;
            string filepath = rootPath + _uploadedUrl;

            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath);

            var directory = new DirectoryInfo(filepath);

            var folderDirectory = Helper.GetDirectoryFolder(directory, filepath);
            return View(folderDirectory);
        }


        [Route("/uploader/images")]
        public IActionResult GetImages([FromQuery] string folderUrl)
        {
            return ViewComponent("ImageManager", folderUrl);
        }

        [Route("/uploader/uploadFile")]
        [HttpPost]
        public IActionResult Upload(IFormFile fileToUpload, string uploadUrl)
        {
            var rootPath = _hostingEnvironment.WebRootPath;
            string uploadPath = rootPath + _uploadedUrl + uploadUrl;


            if (Directory.Exists(uploadPath) && (fileToUpload.ContentType == MimeType.ImageJpeg || fileToUpload.ContentType == MimeType.ImagePng))
            {
                using (var uploadStream = fileToUpload.OpenReadStream())                    
                {
                    var filePath = $"{uploadPath}\\{fileToUpload.FileName}";
                    int fileNo = 1;
                    while (System.IO.File.Exists(filePath))
                    {
                        var extension = fileToUpload.FileName.Split('.').LastOrDefault();
                        var fileName = $"{fileToUpload.FileName.Replace(extension, "")}{fileNo}.{extension}";
                        filePath = $"{uploadPath}\\{fileName}";
                        fileNo++;
                    }
                    using (var fileStream = System.IO.File.Create(filePath))
                    {
                        uploadStream.Seek(0, SeekOrigin.Begin);
                        uploadStream.CopyTo(fileStream);
                    }
                }
            }
            return Ok();
        }
    }
}