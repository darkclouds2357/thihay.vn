﻿// Write your Javascript code.
$(document).ready(function () {
    $(".discard-class-update").hide();
    $(".discard-subject-update").hide();

    ConvertCKE();
    $(".search").on("click", function () {
        $(".page-to-move").val(null);
        $(this).closest('form').submit();
    });

    $(document).on('click', 'input[type=checkbox][class=answer-checked]', function () {
        $(this).parent().find('input[type=hidden][class=isCorrectAnswers]').val(this.checked);
    });

    $(document).on('click', 'input[type=checkbox][class=isStay-checked]', function () {
        $(this).parent().find('input[type=hidden][class=isStay]').val(this.checked);
    });

});

function getTopicDropdownList(classId, subjectId) {
    var url = '';
    if (classId && !subjectId) {
        url = '/api/Dropdown/topic?classId=' + classId;
    }
    if (classId && subjectId) {
        url += '/api/Dropdown/topic?classId=' + classId + '&subjectId=' + subjectId;
    }
    if (!classId && subjectId) {
        url = '/api/Dropdown/topic?subjectId=' + subjectId;
    }
    $(".topic-dropdown").load(url, function () {

    });
}

function getAnswerEditor(questionType) {
    var url = '/question/type/' + questionType;
    $(".question-answer").load(url, function () {
        ConvertCKE();
    });
}

function getUnitDropdownList(classId, subjectId, topicId) {
    var url = '/api/Dropdown/unit';

    url = classId && url.indexOf("?") > -1 ? url += '&classId=' + classId : url;
    url = classId && url.indexOf("?") === -1 ? url += '?classId=' + classId : url;

    url = topicId && url.indexOf("?") > -1 ? url += '&topicId=' + topicId : url;
    url = topicId && url.indexOf("?") === -1 ? url += '?topicId=' + topicId : url;

    url = subjectId && url.indexOf("?") > -1 ? url += '&subjectId=' + subjectId : url;
    url = subjectId && url.indexOf("?") === -1 ? url += '?subjectId=' + subjectId : url;

    $(".unit-dropdown").load(url);
}

function editClass(classId, className) {
    $("#class-id").val(classId);
    $("#new-class-name").val(className);
    $(".discard-class-update").show();
}

function changeClassStaus(classId, isDeleted) {
    var status = (isDeleted === 'True') || (isDeleted === 'true');
    $("#class-id-status").val(classId);
    $("#class-status").val(!status);
    $("#change-class-status-form").submit();
}

$(".discard-class-update").click(function () {
    $("#class-id").val("");
    $("#new-class-name").val("");
    $(".discard-class-update").hide();
});

/* Subject */
function editSubject(subjectId, subjectName) {
    $("#subject-id").val(subjectId);
    $("#new-subject-name").val(subjectName);
    $(".discard-subject-update").show();
}

function changeSubjectStaus(subjectId, isDeleted) {
    var status = (isDeleted === 'True') || (isDeleted === 'true');
    $("#subject-id-status").val(subjectId);
    $("#subject-status").val(!status);
    $("#change-subject-status-form").submit();
}

$(".discard-subject-update").click(function () {
    $("#subject-id").val("");
    $("#new-subject-name").val("");
    $(".discard-subject-update").hide();
});

/* Topic */
function changeTopicStaus(topicId, isDeleted) {
    var status = (isDeleted === 'True') || (isDeleted === 'true');
    $("#topic-id-status").val(topicId);
    $("#topic-status").val(!status);
    $("#change-topic-status-form").submit();
}

/* Unit */
function changeUnitStaus(unitId, isDeleted) {
    var status = (isDeleted === 'True') || (isDeleted === 'true');
    $("#unit-id-status").val(unitId);
    $("#unit-status").val(!status);
    $("#change-unit-status-form").submit();
}

function moveToPreviousPage(currentPage, formId) {
    moveToPage(currentPage - 1, formId);
}
function moveToNextPage(currentPage, formId) {
    moveToPage(currentPage + 1, formId);
}
function moveToPage(page, formId) {
    $(".page-to-move").val(page);
    $("#" + formId).submit();
}

/* Search */

//$('.question-type-partial').load('/Question/type');
/* CK Editor */
function ConvertCKE() {
    var groupmatch = $("textarea[class='thihay-textarea']");
    groupmatch.each(function () {
        if ($(this).is(":visible")) {
            var editor = CKEDITOR.instances[$(this).attr('id')];
            if (editor) { editor.destroy(true); }
            if ($(this).attr('id') === 'name') {
                CKEDITOR.replace(
                    $(this).attr('id'),
                    {
                        toolbar: [
                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                            ['Bold', 'Italic', 'Underline'],
                            ['Image'],
                            ['EqnEditor']
                        ],
                        //skin: 'office2013', height: 60, width: 900
                    }
                );
            }
            else {
                CKEDITOR.replace(
                    $(this).attr('id'),
                    {
                        toolbar: [
                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                            ['Font', 'FontSize', 'Bold', 'Italic', 'Underline'],
                            ['Image'],
                            ['EqnEditor']
                        ],
                        //skin: 'office2013', height: 200, width: 900
                    }
                );
            }
        }
    });
}

/* Question */
function changeQuestionStaus(questionId, isDeleted) {
    var status = (isDeleted === 'True') || (isDeleted === 'true');
    $("#question-id-status").val(questionId);
    $("#question-status").val(!status);
    $("#change-question-status-form").submit();
}
/**
 * Multiple Choice
 */
var alphabetCharacter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

function removeRow($td) {
    var lastRow = $($td).closest('table').find('tbody:first').children().last();
    lastRow.remove();
}

function addMultipleChoiceAnswers($elm, questionOrder) {
    var nextAnswerIndex = getCurrentAnswerIndex($elm) + 1;
    var url = '/question/addmultiplechoice/answer';
    url += '?questionOrder=' + questionOrder;
    url += '&index=' + nextAnswerIndex;
    $.get(url, function (data) {
        //$(".answer-detail").append(data);
        $($elm).closest('table').find('.answer-detail:first').append(data);
        ConvertCKE();
    });
}

function getCurrentAnswerIndex($td) {
    var currentAnswerIndex = [];
    $($td).closest('table').find('tbody:first').find("input[type=hidden][class=answer-index]").each(function () {
        var index = parseInt($(this).val().trim());
        currentAnswerIndex.push(index);
    });
    currentAnswerIndex.sort();
    return currentAnswerIndex.length - 1;
}

function addMultipleChoiceQuestion() {
    var currentQuestionIndex = getCurrentQuestionIndex();
    var url = '/question/addmultiplechoice/question';
    url += '?questionOrder=' + currentQuestionIndex;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
    });
}

function getCurrentQuestionIndex() {
    var currentQuestionIndex = 0;
    $('.question-id').each(function () {
        currentQuestionIndex++;
    });
    return currentQuestionIndex;
}

function getQuestionBrace() {
    var questionContent = CKEDITOR.instances['QuestionContent'].getData();
    var regex = /{}/g;
    var result = [];
    var match;
    while (match = regex.exec(questionContent)) {
        result.push(match.index);
    }
    return result;
}

/**
 * Fill In Blank
 */

function fetchFillInBlankQuestion() {
    var result = getQuestionBrace();
    var matchCnt = result.length;

    var currentQuestionCnt = 0;
    if ($('.tr-question'))
        currentQuestionCnt = $('.tr-question').length;

    if (!matchCnt) {
        alert("Phải có {} ở nơi cần điền vào chỗ trống trong câu hỏi.");
    } else if (matchCnt > currentQuestionCnt) {
        var addCnt = matchCnt - currentQuestionCnt;
        var questionOrder = getCurrentQuestionOrder();
        addFillInBlankAnswer(questionOrder, addCnt, 0);
    }
    else if (matchCnt < currentQuestionCnt) {
        var removeCnt = currentQuestionCnt - matchCnt;
        for (var i = removeCnt; i > 0; i--) {
            var lastTd = $(".question-body tr").last()
            lastTd.remove();
        }
    }
}

function addFillInBlankAnswer(questionOrder, addCount, added) {
    if (added === addCount) {
        return;
    }
    var url = '/question/addfillintheblank/question';
    url += '?questionOrder=' + questionOrder;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
        questionOrder++;
        added++;
        if (added < addCount)
            addFillInBlankAnswer(questionOrder, addCount, added);
    });
}

function getCurrentQuestionOrder() {
    var orders = [];
    $('.question-order').each(function () {
        orders.push($(this).val());
    });
    return orders.length;

}


/**
 * Cloze Test
 */

function fetchClozeTestQuestion() {
    var result = getQuestionBrace();
    var matchCnt = result.length;
    var currentQuestionCnt = 0;
    if ($('.tr-question'))
        currentQuestionCnt = $('.tr-question').length;

    if (!matchCnt) {
        alert("Phải có {} ở nơi cần điền vào chỗ trống trong câu hỏi.");
    } else if (matchCnt > currentQuestionCnt) {
        var addCnt = matchCnt - currentQuestionCnt;
        var questionOrder = getCurrentQuestionOrder();
        addClozeTestQuestion(questionOrder, addCnt, 0);
    }
    else if (matchCnt < currentQuestionCnt) {
        var removeCnt = currentQuestionCnt - matchCnt;
        for (var i = removeCnt; i > 0; i--) {
            var lastTd = $(".question-body tr").last()
            lastTd.remove();
        }
    }
}
function addClozeTestQuestion(questionOrder, addCount, added) {
    if (added === addCount) {
        return;
    }
    var url = '/question/addclozetest/question';
    url += '?questionOrder=' + questionOrder;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
        questionOrder++;
        added++;
        if (added < addCount)
            addClozeTestQuestion(questionOrder, addCount, added);
    });
}

function addMatchingQuestion() {
    var currentQuestionIndex = getCurrentQuestionIndex();
    var url = '/question/addmatching/question';
    url += '?questionOrder=' + currentQuestionIndex;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
    });
}


function addArrangeQuestion() {
    var currentQuestionIndex = getCurrentQuestionIndex();
    var url = '/question/addarrange/question';
    url += '?questionOrder=' + currentQuestionIndex;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
    });
}


function addDrapAndDropChoice() {
    var questionContent = CKEDITOR.instances['QuestionContent'].getData();
    var regex = /{[0-9]+}/g;
    var result = [];
    var match;
    while (match = regex.exec(questionContent)) {
        var content = parseInt(match["0"].replace("{", "").replace("}", ""));
        if (content && result.indexOf(content) === -1)
            result.push(content);
    }
    var maxCurrentAnswerIndex = Math.max.apply(null, result);
    var currentAnswerCount = getCurrentQuestionIndex();

    if (maxCurrentAnswerIndex > currentAnswerCount) {
        var needToAdd = maxCurrentAnswerIndex - currentAnswerCount;
        addMoreDrapAndDropAnswer(currentAnswerCount, needToAdd, 0);
    } else {
        addMoreDrapAndDropAnswer(currentAnswerCount, 1, 0);
    }
}

function removeDrapAndDropChoice($td) {
    var questionContent = CKEDITOR.instances['QuestionContent'].getData();
    var regex = /{[0-9]+}/g;
    var result = [];
    var match;
    while (match = regex.exec(questionContent)) {
        var content = parseInt(match["0"].replace("{", "").replace("}", ""));
        if (content && result.indexOf(content) === -1)
            result.push(content);
    }
    var maxCurrentAnswerIndex = Math.max.apply(null, result);
    var currentAnswerCount = getCurrentQuestionIndex();
    if (maxCurrentAnswerIndex <= currentAnswerCount) {
        removeRow($td);
    }
}

function addMoreDrapAndDropAnswer(currentAnswerCount, addCount, added) {
    if (added === addCount) {
        return;
    }
    var url = '/question/adddrapanddrop/question';
    url += '?questionOrder=' + currentAnswerCount;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
        currentAnswerCount++;
        added++;
        if (added < addCount)
            addMoreDrapAndDropAnswer(currentAnswerCount, addCount, added);
    });
}


/*
 * Group
 */

function addGroupQuestion() {
    var currentQuestionIndex = getCurrentQuestionIndex();
    var url = '/question/addgroup/question';
    url += '?questionOrder=' + currentQuestionIndex;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
    });
}


function addGroupItem($elm, questionOrder) {
    var nextAnswerIndex = getCurrentAnswerIndex($elm) + 1;
    var url = '/question/addgroup/answer';
    url += '?questionOrder=' + questionOrder;
    url += '&index=' + nextAnswerIndex;
    $.get(url, function (data) {
        // $(".answer-detail").append(data);
        $($elm).closest('table').find('.answer-detail:first').append(data);
        ConvertCKE();
    });
}

function fetchUnderlineQuestion() {
    var currentQuestionIndex = getCurrentQuestionIndex();
    var questionContent = CKEDITOR.instances['QuestionContent'].getData();
    var regex = /{(.*?)}/g;
    var result = [];
    var match;
    while (match = regex.exec(questionContent)) {
        var content = match["0"].replace("{", "").replace("}", "").trim();
        if (content && result.indexOf(content) === -1)
            result.push(content);
    }
    var matchCnt = result.length;
    if (matchCnt === 0) {
        alert("Câu hỏi hợp lệ phải là câu được đặt trong {}. Ví dụ {This is a test website}.");
    } else if (matchCnt > currentQuestionIndex) {
        var addCnt = matchCnt - currentQuestionIndex;
        var questionOrder = getCurrentQuestionOrder();
        addUnderlineQuestion(result, currentQuestionIndex, addCnt, 0);
    }
    else if (matchCnt < currentQuestionIndex) {
        //var removeCnt = currentQuestionIndex - matchCnt;
        //for (var i = removeCnt; i > 0; i--) {
        //    var lastTd = $(".question-body .tr-question").last()
        //    lastTd.remove();
        //}
        $(".question-body .tr-question").each(function () {
            $(this).remove();
        });
        fetchUnderlineQuestion();
    }
}

function addUnderlineQuestion(questions, index, addCount, added) {
    if (added === addCount) {
        return;
    }
    var url = '/question/addUnderlineQuestion/question';
    url += '?questionContent=' + questions[index];
    url += '&questionOrder=' + index;
    $.get(url, function (data) {
        $(".question-body").append(data);
        ConvertCKE();
        index++;
        added++;
        if (added < addCount) {
            addFillInBlankAnswer(questions, index, addCount, added);
        }
    });
}

/*****************/

function RenderServerFolderTree() {
    var treeView = '<div style="overflow:scroll; height:300px"><ul><li>';
    treeView += '<a href="#" data-url="\\" class = "chooes-server-folder"><span><i class="fa fa-lg fa-folder-open"></i> Root</span></a>';
    var url = '';
    for (var item in serverFormSaveFolderJson) {
        if (item === "directory") {
            treeView += '<ul>';
            treeView += DisplayFolder(serverFormSaveFolderJson[item], url, true);
            treeView += '</ul>';
        }
    }
    treeView += '</li></ul></div>';

    $(".tree").html("");
    $(".tree").append(treeView);

}

function DisplayFolder(folder, url, isDisplay) {
    var li = '';
    for (var item in folder) {
        if (item !== "directory" && item !== "file") {
            var childUrl = url + '\\' + item;
            var isHasChildFolder = false;
            for (var dir in folder[item]) {
                if (dir === "directory") {
                    for (var childDir in folder[item][dir]) {
                        if (folder[item][dir][childDir] !== "directory" && folder[item][dir][childDir] !== "file") {
                            isHasChildFolder = true;
                            break;
                        }
                    }
                }
            }
            if (isDisplay) {
                li += '<li>';
            } else {
                li += '<li hidden="hidden">';
            }
            li += '<span>';
            if (isHasChildFolder) {
                li += '<a href="#" class = "extend-folder" ><i class="fa fa-lg fa-plus-circle"></i></a> ';
                //li += '<a href="#" data-url = "' + childUrl + '" class = "chooes-server-folder" data-folder = "' + item + '"><span><i class="fa fa-lg fa-folder"></i> ' + item + '</span></a>';
                li += '<a href="#" data-url = "' + childUrl + '" class = "chooes-server-folder" data-folder = "' + item + '"><i class="fa fa-hand-o-right"></i> ' + item + '</a>';
            }
            else {
                li += '<a href="#" data-url = "' + childUrl + '" class = "chooes-server-folder" data-folder = "' + item + '"><i class="fa fa-lg fa-folder"></i> ' + item + '</a>';
            }
            li += '</span>';
            for (var dir in folder[item]) {
                if (dir === "directory") {
                    li += '<ul>';
                    li += DisplayFolder(folder[item][dir], childUrl, false);
                    li += '</ul>';
                }
            }
            li += '</li>';
        }
    }
    return li;
}