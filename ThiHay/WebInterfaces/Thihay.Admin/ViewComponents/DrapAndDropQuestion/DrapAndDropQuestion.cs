﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.Admin.ViewComponents.DrapAndDropQuestion
{
    [ViewComponent(Name = "DrapAndDropQuestion")]
    public class DrapAndDropQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder)
        {
            ViewBag.AnswerOrder = questionOrder;
            return View();
        }
    }
}
