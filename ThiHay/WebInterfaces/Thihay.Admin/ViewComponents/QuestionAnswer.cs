﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "QuestionAnswer")]
    public class QuestionAnswer : ViewComponent
    {
        private readonly IChuyenDeServices _service;

        public QuestionAnswer(IChuyenDeServices service)
        {
            _service = service;
        }

        public async Task<IViewComponentResult> InvokeAsync(QuestionType questionType, IList<QuestionDetail> questionAnswer)
        {
            ViewBag.QuestionType = questionType;
            questionAnswer = questionAnswer == null ? new List<QuestionDetail>() : questionAnswer;
            questionAnswer = questionAnswer.Select(e =>
            {
                var answers = e.Answers.OrderBy(a => a.AnswerIndex).ToList();
                e.Answers = answers;
                return e;
            }).ToList();

            if (questionAnswer.All(qd => qd.Answers.Count == 1))
            {
                questionAnswer = questionAnswer.OrderBy(qd => qd.Answers.FirstOrDefault().AnswerIndex).ToList();
            }
            switch (questionType)
            {
                case QuestionType.MultipleChoice:

                    return MultipleChoiceViewComponent(questionAnswer);

                case QuestionType.ReadingComprehension:
                    return ReadingComprehensionViewComponent(questionAnswer);

                case QuestionType.FillInTheBlank:
                    return FillInTheBlankViewComponent(questionAnswer);

                case QuestionType.RewriteTheSentences:
                    return RewriteTheSentencesViewComponent(questionAnswer);

                case QuestionType.ClozeTest:
                    return ClozeTestViewComponent(questionAnswer);

                case QuestionType.Matching:
                    return MatchingViewComponent(questionAnswer);

                case QuestionType.Arrange:
                    return ArrangeViewComponent(questionAnswer);

                case QuestionType.DragAndDrop:
                    return DragAndDropViewComponent(questionAnswer);

                case QuestionType.Groups:
                    return GroupsViewComponent(questionAnswer);

                case QuestionType.Underline:
                    return UnderlineViewComponent(questionAnswer);

                case QuestionType.UnderlineAndRewrite:
                    return UnderlineAndRewriteViewComponent(questionAnswer);

                case QuestionType.DeleteCharacter:
                    return DeleteCharacterViewComponent(questionAnswer);

                default:
                    return View();
            }
        }

        private IViewComponentResult MultipleChoiceViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("MultipleChoice", questionAnswer);
        }

        private IViewComponentResult ReadingComprehensionViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return MultipleChoiceViewComponent(questionAnswer);
        }

        private IViewComponentResult FillInTheBlankViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("FillInTheBlank", questionAnswer);
        }

        private IViewComponentResult RewriteTheSentencesViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return FillInTheBlankViewComponent(questionAnswer);
        }

        private IViewComponentResult ClozeTestViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("ClozeTest", questionAnswer);
        }

        private IViewComponentResult MatchingViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("Matching", questionAnswer);
        }

        private IViewComponentResult ArrangeViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("Arrange", questionAnswer);
        }

        private IViewComponentResult DragAndDropViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            var answers = questionAnswer.Where(q => q.Answers.Count > 0).SelectMany(q => q.Answers).OrderBy(a => a.AnswerIndex).ToList();
            return View("DragAndDrop", answers);
        }

        private IViewComponentResult GroupsViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("Groups", questionAnswer);
        }

        private IViewComponentResult UnderlineViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View("Underline", questionAnswer);
        }

        private IViewComponentResult UnderlineAndRewriteViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View();
        }

        private IViewComponentResult DeleteCharacterViewComponent(IEnumerable<QuestionDetail> questionAnswer)
        {
            return View();
        }
    }
}
