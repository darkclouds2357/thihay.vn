﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.Admin.ViewComponents.ClozeTestQuestion
{
    [ViewComponent(Name = "ClozeTestQuestion")]
    public class ClozeTestQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder)
        {
            ViewBag.QuestionOrder = questionOrder;
            return View();
        }
    }
}
