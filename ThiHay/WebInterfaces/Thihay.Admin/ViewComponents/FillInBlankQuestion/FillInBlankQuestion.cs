﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.Admin.ViewComponents.FillInBlankQuestion
{
    [ViewComponent(Name = "FillInBlankQuestion")]
    public class FillInBlankQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder)
        {
            ViewBag.QuestionOrder = questionOrder;
            return View();
        }
    }
}
