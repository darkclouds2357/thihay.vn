﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.Admin.ViewComponents.MatchingQuestion
{
    [ViewComponent(Name = "MatchingQuestion")]
    public class MatchingQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder)
        {
            ViewBag.QuestionOrder = questionOrder;
            return View();
        }
    }
}
