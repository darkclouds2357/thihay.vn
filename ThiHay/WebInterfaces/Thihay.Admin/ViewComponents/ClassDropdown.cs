﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "ClassDropdown")]
    public class ClassDropdown : ViewComponent
    {
        private readonly IClassServices _classServices;

        public ClassDropdown(IClassServices classService)
        {
            _classServices = classService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? classId)
        {
            var classes = await _classServices.GetClass(string.Empty);
            var result = classes.Where(c => !c.IsDeleted).Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = WebUtility.HtmlDecode(c.ClassName),
                Selected = classId.HasValue && c.Id == classId
            });
            return View(result);
        }
    }
}
