﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.Admin.ViewComponents.MultipleChoiceQuestion
{
    [ViewComponent(Name = "GroupAnswer")]
    public class GroupAnswer : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder, int index, bool isCorrectAnswer, string answerContent)
        {
            return View(new Answer
            {
                AnswerContent = WebUtility.HtmlDecode(answerContent),
                AnswerIndex = index,
                QuestionOrder = questionOrder
            });
        }
    }
}
