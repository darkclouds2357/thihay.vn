﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thihay.Admin.ViewComponents.MultipleChoiceQuestion
{
    [ViewComponent(Name = "GroupQuestion")]
    public class GroupQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(int questionOrder)
        {
            ViewBag.QuestionOrder = questionOrder;
            return View();
        }
    }
}
