﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "UnitDropdown")]
    public class UnitDropdown : ViewComponent
    {
        private readonly IChuyenDeServices _services;

        public UnitDropdown(IChuyenDeServices services)
        {
            _services = services;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? classId, int? subjectId, int? topicId, int? unitId)
        {
            var units = await _services.GetAllUnits(string.Empty, subjectId, classId, topicId, null, null);
            var result = units?.Units?.Where(c => !c.IsDeleted).Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = WebUtility.HtmlDecode(c.NameUnit),
                Selected = unitId.HasValue && c.Id == unitId
            });
            return View(result);
        }
    }
}
