﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "QuestionTypeDropdown")]
    public class QuestionTypeDropdown : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(QuestionType? questionType)
        {
            var result = await Task.Run(() => Enum.GetValues(typeof(QuestionType)).Cast<QuestionType>().Where(t => t != QuestionType.NoType).Select(t => new SelectListItem
            {
                Value = ((int)t).ToString(),
                Text = t.ToString(),
                Selected = questionType == t
            }));
            return View(result);
        }
    }
}
