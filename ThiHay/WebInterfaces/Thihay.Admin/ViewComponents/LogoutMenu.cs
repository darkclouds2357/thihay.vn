﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "LogoutMenu")]
    public class LogoutMenu : ViewComponent
    {
        private readonly IIdentityService _identityService;
        private readonly string _token;
        public LogoutMenu(IIdentityService identityService, ITokenModel token)
        {
            _token = token.AccessToken;
            _identityService = identityService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!string.IsNullOrWhiteSpace(_token))
            {
                var userInfo = await _identityService.GetLoginUserInfo();
                if (userInfo != null)
                {
                    return View(userInfo);
                }
            }
            return View("NotLogin");
        }
    }
}
