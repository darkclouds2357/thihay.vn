﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "SubjectDropdown")]
    public class SubjectDropdown : ViewComponent
    {
        private readonly ISubjectServices _subjectService;

        public SubjectDropdown(ISubjectServices subjectService)
        {
            _subjectService = subjectService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? subjectId)
        {
            var subjects = await _subjectService.GetSubject(string.Empty);
            var result = subjects.Where(c => !c.IsDeleted).Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = WebUtility.HtmlDecode(c.SubjectName),
                Selected = subjectId.HasValue && c.Id == subjectId
            });
            return View(result);
        }
    }
}
