﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client;
using Thihay.API.Client.Models;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "ViewQuestionDetail")]
    public class ViewQuestionDetail : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(QuestionType questionType, IList<QuestionDetail> questionAnswer)
        {
            ViewBag.QuestionType = questionType;
            //IList<QuestionDetail> questionDetail = questionAnswer.Select(qd =>
            //{
            //    var answers = qd.Answers.Select(a =>
            //    {
            //        a.AnswerContent = WebUtility.HtmlDecode(a.AnswerContent);
            //        return a;
            //    }).OrderBy(a => a.AnswerIndex).ToList();
            //    qd.Answers = answers;
            //    qd.QuestionDetailContent = WebUtility.HtmlDecode(qd.QuestionDetailContent);
            //    return qd;
            //}).ToList();
            //if (questionDetail.All(qd => qd.Answers.Count == 1))
            //{
            //    questionDetail = questionDetail.OrderBy(qd => qd.Answers.FirstOrDefault().AnswerIndex).ToList();
            //}
            questionAnswer = questionAnswer.OrderBy(q => q.Order).ToList();
            if (questionType == QuestionType.DragAndDrop)
            {
                var answers = questionAnswer.Where(q => q.Answers.Count > 0).SelectMany(q => q.Answers).Select(a => { a.AnswerContent = WebUtility.HtmlDecode(a.AnswerContent); return a; }).ToList();
                return View("DrapAndDropDetail", answers);
            }

            return View(questionAnswer);
        }

    }
}
