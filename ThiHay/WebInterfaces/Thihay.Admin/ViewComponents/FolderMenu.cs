﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.Common;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "FolderMenu")]
    public class FolderMenu : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(IEnumerable<Folder> folderList, int tabindex, bool isRootFolder = false)
        {
            ViewBag.TabIndex = tabindex;
            ViewBag.IsRootFolder = isRootFolder;
            return View(folderList);
        }
    }
}
