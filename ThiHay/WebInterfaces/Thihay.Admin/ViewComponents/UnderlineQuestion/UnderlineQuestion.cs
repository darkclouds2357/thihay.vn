﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.Admin.ViewComponents.UnderlineQuestion
{
    [ViewComponent(Name = "UnderlineQuestion")]
    public class UnderlineQuestion : ViewComponent
    {

        public async Task<IViewComponentResult> InvokeAsync(string questionContent, int questionOrder, Guid? questionDetailId, List<int> correctAnswerIndexs)
        {
            var answerContents = questionContent.Split(' ').Where(a => !string.IsNullOrWhiteSpace(a)).ToArray();
            var answers = new List<Answer>();

            for (int i = 0; i < answerContents.Length; i++)
            {
                answers.Add(new Answer
                {
                    AnswerContent = answerContents[i],
                    AnswerIndex = i,
                    IsCorrectAnswer = correctAnswerIndexs != null ? correctAnswerIndexs.Contains(i) : false
                });
            }

            var questionDetail = new QuestionDetail()
            {
                Order = questionOrder,
                QuestionDetailId = questionDetailId.HasValue ? questionDetailId.Value : Guid.NewGuid(),
                QuestionDetailContent = questionContent,
                Answers = answers
            };
            return View(questionDetail);
        }
    }
}
