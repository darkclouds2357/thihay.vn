﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "TopicDropdown")]
    public class TopicDropdown : ViewComponent
    {
        private readonly IChuyenDeServices _services;

        public TopicDropdown(IChuyenDeServices services)
        {
            _services = services;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? classId, int? subjectId, int? topicId)
        {
            var topics = await _services.GetAllTopics(string.Empty, subjectId, classId, null, null);
            var result = topics?.Topics?.Where(c => !c.IsDeleted).Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = WebUtility.HtmlDecode(c.TopicName),
                Selected = topicId.HasValue && c.Id == topicId
            });
            return View(result);
        }
    }
}
