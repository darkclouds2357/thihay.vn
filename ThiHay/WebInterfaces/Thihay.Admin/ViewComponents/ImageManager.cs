﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Thihay.API.Client;
using Thihay.Common;

namespace Thihay.Admin.ViewComponents
{
    [ViewComponent(Name = "ImageManager")]
    public class ImageManager : ViewComponent
    {
        private readonly string[] _allowExtension = new string[] { ".png", ".jpg" };
        private readonly string _uploadedUrl;
        private readonly IHostingEnvironment _hostingEnvironment;
        public ImageManager(IOptionsSnapshot<AppSettings> settings, IHostingEnvironment environment)
        {
            _uploadedUrl = settings.Value.UploadUrl;
            _hostingEnvironment = environment;
        }
        public async Task<IViewComponentResult> InvokeAsync(string url)
        {
            var rootPath = _hostingEnvironment.WebRootPath;
            string filepath = rootPath + _uploadedUrl + url;
            ViewBag.CurrentFolder = url;
            if (Directory.Exists(filepath))
            {
                var directory = new DirectoryInfo(filepath);
                var files = directory.EnumerateFiles().Where(f => _allowExtension.Contains(f.Extension)).Select(f => new ImgFile
                {
                    FileName = f.Name,
                    FileExtension = f.Extension,
                    FilePath = f.FullName.Replace(rootPath, "").Replace("\\", "/")
                });

                return View(files);
            }
            return View(new List<ImgFile>());
        }
    }
}
