﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.IO;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;

namespace Thihay.API.Client.Services
{
    public class SupportServices : ISupportServices
    {
        private readonly string _defaultUploadUrl;
        private readonly IOptionsSnapshot<AppSettings> _settings;

        public SupportServices(IOptionsSnapshot<AppSettings> setting)
        {
            _settings = setting;
            _defaultUploadUrl = setting.Value.UploadUrl;
        }

        public async Task<string> UploadImageAsync(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string imagePath = string.Empty;
            string message = string.Empty;
            string output = string.Empty;

            try
            {
                if (upload != null && upload.Length > 0)
                {
                    var fileName = DateTime.Now.ToString("yyyyMMdd-HHMMssff") + " - " + ContentDispositionHeaderValue.Parse(upload.ContentDisposition).FileName.Trim('"');
                    var folderPath = _defaultUploadUrl;

                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                    imagePath = Path.Combine(folderPath, fileName);
                    using (var fileStream = new FileStream(imagePath, FileMode.Create))
                    {
                        await upload.CopyToAsync(fileStream);
                    }
                    message = "The file uploaded successfully.";
                }
            }
            catch (Exception e)
            {
                message = "There was an issue uploading:" + e.Message;
            }
            return output = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imagePath + "\", \"" + message + "\");</script></body></html>";
        }
    }
}