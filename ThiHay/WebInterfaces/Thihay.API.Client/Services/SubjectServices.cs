﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Services
{
    public class SubjectServices : ThihayBaseService, ISubjectServices
    {
        private readonly string _subjectUrl;

        public SubjectServices(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _subjectUrl = settings.Value.SubjectUrl;
        }

        public async Task<IEnumerable<Subject>> GetSubject(string searchKey)
        {
            var url = Infrastructure.API.SubjectAPI.GetAllSubject(_subjectUrl, searchKey);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Subject>>(dataString) ?? new List<Subject>();

            return response;
        }

        public async Task<bool> UpdateSubject(Subject subject)
        {
            try
            {
                var url = Infrastructure.API.SubjectAPI.UpdateSubject(_subjectUrl);
                var result = await PostAsync(url, subject);
                result.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeSubjectStatus(int subjectId, bool isDelete)
        {
            try
            {
                var url = Infrastructure.API.SubjectAPI.ChangeSubjectStatus(_subjectUrl, subjectId, isDelete);
                var result = await GetStringAsync(url);

                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}