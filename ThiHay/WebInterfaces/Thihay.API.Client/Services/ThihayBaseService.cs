﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using System.Net.Http;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Services
{
    public abstract class ThihayBaseService
    {
        private readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        private IHttpContextAccessor _httpContextAccesor;
        private string _authorizationToken;

        public ThihayBaseService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token)
        {
            _settings = settings;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
            _authorizationToken = token.AccessToken;
        }

        //public void SetAuthentication(string token)
        //{
        //    _authorizationToken = token;
        //}

        protected async Task<string> GetStringAsync(string url)
        {
            return await _apiClient.GetStringAsync(url, _authorizationToken);
        }

        protected async Task<HttpResponseMessage> PostAsync<T>(string url, T item)
        {
            return await _apiClient.PostAsync<T>(url, item, _authorizationToken);
        }
    }
}