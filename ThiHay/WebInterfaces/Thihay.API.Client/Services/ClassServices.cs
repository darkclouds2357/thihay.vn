﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Services
{
    public class ClassServices : ThihayBaseService, IClassServices
    {
        private readonly string _classUrl;

        public ClassServices(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _classUrl = settings.Value.ClassUrl;
        }

        public async Task<IEnumerable<Class>> GetClass(string searchKey)
        {
            var url = Infrastructure.API.ClassAPI.GetAllClass(_classUrl, searchKey);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Class>>(dataString) ?? new List<Class>();

            return response;
        }

        public async Task<bool> UpdateClass(Class classDto)
        {
            try
            {
                var url = Infrastructure.API.ClassAPI.UpdateClass(_classUrl);
                var result = await PostAsync(url, classDto);
                result.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeClassStatus(int classId, bool isDelete)
        {
            try
            {
                var url = Infrastructure.API.ClassAPI.ChangeClassStatus(_classUrl, classId, isDelete);
                var result = await GetStringAsync(url);

                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}