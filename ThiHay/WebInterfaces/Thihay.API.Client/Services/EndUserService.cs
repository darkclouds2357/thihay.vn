﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;
using Thihay.User.ViewModel;
using System;
using System.Linq;

namespace Thihay.API.Client.Services
{
    public class EndUserService : ThihayBaseService, IEndUserService
    {
        private readonly string _endUserUrl;

        public EndUserService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _endUserUrl = settings.Value.EndUserUrl;
        }

        public async Task<IEnumerable<SummaryModel>> GetSummary()
        {
            var url = Infrastructure.API.EndUser.GetSummary(_endUserUrl);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<SummaryModel>>(dataString) ?? new List<SummaryModel>();

            return response;
        }

        public async Task<IEnumerable<Class>> GetClasses(int subjectId)
        {
            var url = Infrastructure.API.EndUser.GetClasses(_endUserUrl, subjectId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Class>>(dataString) ?? new List<Class>();

            return response;
        }

        public async Task<IEnumerable<Topic>> GetTopics(int subjectId, int classId)
        {
            var url = Infrastructure.API.EndUser.GetTopics(_endUserUrl, subjectId, classId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Topic>>(dataString) ?? new List<Topic>();

            return response;
        }

        public async Task<IEnumerable<Subject>> GetAllSubject()
        {
            var url = Infrastructure.API.EndUser.GetAllSubjects(_endUserUrl);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Subject>>(dataString) ?? new List<Subject>();

            return response;
        }

        public async Task<Unit> GetUnit(int unitId)
        {
            var url = Infrastructure.API.EndUser.GetUnit(_endUserUrl, unitId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<Unit>(dataString) ?? new Unit();

            return response;
        }

        public async Task<Question> GetRandomQuestion(int unitId, int userId)
        {
            var url = Infrastructure.API.EndUser.GetRandomQuestion(_endUserUrl, unitId, userId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<Question>(dataString) ?? new Question();

            return response;
        }

        public async Task<UserProfile> GetUserProfile(int? subjectId)
        {
            var url = Infrastructure.API.EndUser.GetUserProfile(_endUserUrl, subjectId);

            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<Mark>>(dataString) ?? new List<Mark>();

            return new UserProfile(response);

        }

        public async Task<ExecutionResult<int>> SubmitAnswer(Question question)
        {
            var result = new ExecutionResult<int>()
            {
                Result = 0,
                Success = false
                
            };
            try
            {
                var url = Infrastructure.API.EndUser.SubmitAnswer(_endUserUrl);
                var response = await PostAsync(url, question);
                response.EnsureSuccessStatusCode();
                var data = await response.Content.ReadAsStringAsync();
                var responseResult = JsonConvert.DeserializeObject<ExecutionResult<int>>(data) ?? result;
                if(responseResult.Success)
                {
                    string message = "Trả lời sai mất rồi.";
                    if(responseResult.Result > 0 && question.QuestionDetail.Count() > 1 && responseResult.Result < question.QuestionDetail.Count())
                    {
                        message = $"Bạn đã trả lời được {responseResult.Result} trên {question.QuestionDetail.Count()} mục";
                    }else if(responseResult.Result > 0 && question.QuestionDetail.Count() > 1 && responseResult.Result == question.QuestionDetail.Count())
                    {
                        message = $"Bạn đã trả lời được đúng tất cả các mục";
                    }else if(responseResult.Result > 0 && question.QuestionDetail.Count() == 1)
                    {
                        message = $"Bạn đã đáp đúng rồi.";
                    }
                    responseResult.Messages = new string[] { message };
                }
                return responseResult;
            }
            catch (Exception ex)
            {
                result = new ExecutionResult<int>()
                {
                    Result = 0,
                    Success = false,
                    Error = ex.Message
                };
            }

            return result;
        }
    }
}