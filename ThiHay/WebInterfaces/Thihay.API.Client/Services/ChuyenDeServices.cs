﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Services
{
    public class ChuyenDeServices : ThihayBaseService, IChuyenDeServices
    {
        private readonly string _chuyendeUrl;

        public ChuyenDeServices(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _chuyendeUrl = settings.Value.ChuyenDeUrl;
        }

        public async Task<TopicModel> GetAllTopics(string searchKey, int? subjectId, int? classId, int? page, int? pageSize)
        {
            var url = Infrastructure.API.ChuyenDeAPI.GetAllTopics(_chuyendeUrl, searchKey, subjectId, classId, page, pageSize);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<TopicModel>(dataString) ?? new TopicModel();

            return response;
        }

        public async Task<Topic> GetTopicDetail(int topicId)
        {
            var url = Infrastructure.API.ChuyenDeAPI.GetTopicDetail(_chuyendeUrl, topicId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<Topic>(dataString) ?? new Topic();

            return response;
        }

        public async Task<bool> UpdateTopic(Topic topic)
        {
            try
            {
                var url = Infrastructure.API.ChuyenDeAPI.UpdateTopic(_chuyendeUrl);
                var result = await PostAsync(url, topic);
                result.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeTopicStatus(int topicId, bool isDelete)
        {
            try
            {
                var url = Infrastructure.API.ChuyenDeAPI.ChangeTopicStatus(_chuyendeUrl, topicId, isDelete);
                var result = await GetStringAsync(url);

                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<UnitModel> GetAllUnits(string searchKey, int? subjectId, int? classId, int? topicId, int? page, int? pageSize)
        {
            var url = Infrastructure.API.ChuyenDeAPI.GetAllUnits(_chuyendeUrl, searchKey, subjectId, classId, topicId, page, pageSize);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<UnitModel>(dataString) ?? new UnitModel();

            return response;
        }

        public async Task<Unit> GetUnitDetail(int unitId)
        {
            var url = Infrastructure.API.ChuyenDeAPI.GetUnitDetail(_chuyendeUrl, unitId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<Unit>(dataString) ?? new Unit();

            return response;
        }

        public async Task<bool> UpdateUnit(Unit unit)
        {
            try
            {
                var url = Infrastructure.API.ChuyenDeAPI.UpdateUnit(_chuyendeUrl);
                var result = await PostAsync(url, unit);
                result.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateUnitOrder(int unitId, int newOrder)
        {
            var url = Infrastructure.API.ChuyenDeAPI.UpdateUnitOrder(_chuyendeUrl, unitId, newOrder);
            var dataString = await GetStringAsync(url);
            return Convert.ToBoolean(dataString);
        }

        public async Task<bool> ChangeUnitStatus(int unitId, bool isDelete)
        {
            var url = Infrastructure.API.ChuyenDeAPI.ChangeUnitStatus(_chuyendeUrl, unitId, isDelete);
            var dataString = await GetStringAsync(url);
            return Convert.ToBoolean(dataString);
        }
    }
}