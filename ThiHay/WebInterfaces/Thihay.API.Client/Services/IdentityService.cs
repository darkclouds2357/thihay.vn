﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;
using Thihay.Common;
using System;
using System.Collections.Generic;

namespace Thihay.API.Client.Services
{
    public class IdentityService : ThihayBaseService, IIdentityService
    {

        private readonly string _identityUrl;

        public IdentityService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _identityUrl = settings.Value.IdentityUrl;
        }

        

        public async Task<ExecutionResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var url = Infrastructure.API.IdentityAPI.ChangePassword(_identityUrl);

                var response = await PostAsync(url, model);
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();


                return JsonConvert.DeserializeObject<ExecutionResult>(result) ?? new ExecutionResult { Success = false };
            }
            catch (Exception)
            {
                return new ExecutionResult
                {
                    Success = false,
                    Messages = new string[] { "Thông tin đăng nhập không chính xác." }
                };
            }

        }

        public async Task<string> FacebookLogin(string facebookToken)
        {
            var url = Infrastructure.API.IdentityAPI.FacebookLogin(_identityUrl);

            var response = await _apiClient.LoginAsync(url, new Dictionary<string, string>
            {
                ["token"] = facebookToken
            });

            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            var accessToken = JsonConvert.DeserializeObject<TokenModel>(result);

            return accessToken.AccessToken;
        }

        public async Task<IThihayUser> GetLoginUserInfo()
        {
            var url = Infrastructure.API.IdentityAPI.GetUserInfo(_identityUrl);

            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<ThihayUser>(dataString);

            return response;

        }

        public async Task<string> GoogleLogin(string googletoken)
        {
            var url = Infrastructure.API.IdentityAPI.GoogleLogin(_identityUrl);
            var response = await _apiClient.LoginAsync(url, new Dictionary<string, string>
            {
                ["token"] = googletoken
            });
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            var accessToken = JsonConvert.DeserializeObject<TokenModel>(result);

            return accessToken.AccessToken;
        }        

        public async Task<ExecutionResult<TokenModel>> Login(string username, string password, bool isAdminLogin = false)
        {
            try
            {
                var url = Infrastructure.API.IdentityAPI.Login(_identityUrl);
                var response = await _apiClient.LoginAsync(url, new Dictionary<string, string>()
                {
                    ["username"] = username,
                    ["password"] = password,
                    ["isAdminLogin"] = isAdminLogin.ToString()
                });
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                var accessToken = JsonConvert.DeserializeObject<TokenModel>(result);
                if (accessToken != null && !string.IsNullOrWhiteSpace(accessToken.AccessToken))
                    return new ExecutionResult<TokenModel>
                    {
                        Success = true,
                        Result = accessToken
                    };
                else
                    return new ExecutionResult<TokenModel>()
                    {
                        Success = false,
                        Result = null
                    };
            }
            catch
            {
                return new ExecutionResult<TokenModel>()
                {
                    Success = false
                };
            }

        }

        public async Task<ExecutionResult<string>> RegisterNewUser(RegisterModel model)
        {
            try
            {
                var url = Infrastructure.API.IdentityAPI.RegisterNewUser(_identityUrl);
                var response = await PostAsync(url, model);
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ExecutionResult<string>>(result) ?? new ExecutionResult<string> { Success = false, Result = string.Empty };
            }
            catch (Exception ex)
            {

                return new ExecutionResult<string>
                {
                    Error = ex.Message,
                    Success = false
                };
            }
        }
    }
}