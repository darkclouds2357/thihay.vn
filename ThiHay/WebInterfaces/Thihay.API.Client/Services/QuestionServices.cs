﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Resilience.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Services
{
    public class QuestionServices : ThihayBaseService, IQuestionServices
    {
        private readonly string _questionUrl;

        public QuestionServices(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient, ITokenModel token) : base(settings, httpContextAccesor, httpClient, token)
        {
            _questionUrl = settings.Value.QuestionUrl;
        }

        public async Task<Question> GetQuestionDetail(int questionId)
        {
            var url = Infrastructure.API.QuestionAPI.GetQuestionDetail(_questionUrl, questionId);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<Question>(dataString) ?? new Question();

            return response;
        }

        public async Task<QuestionModel> GetAllQuestion(string searchKey, int? unitId, int? subjectId, int? classId, int? topicId, QuestionType? questionType, int? page, int? pageSize)
        {
            var url = Infrastructure.API.QuestionAPI.GetAllQuestion(_questionUrl, searchKey, subjectId, classId, topicId, unitId, questionType, page, pageSize);
            var dataString = await GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<QuestionModel>(dataString) ?? new QuestionModel();

            return response;
        }

        public async Task<bool> UpdateQuestion(Question questionDto)
        {
            try
            {
                var url = Infrastructure.API.QuestionAPI.UpdateQuestion(_questionUrl);
                var result = await PostAsync(url, questionDto);
                result.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> ChangeQuestionStatus(int questionId, bool isDelete)
        {
            try
            {
                var url = Infrastructure.API.QuestionAPI.ChangeQuestionStatus(_questionUrl, questionId, isDelete);
                var result = await GetStringAsync(url);

                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}