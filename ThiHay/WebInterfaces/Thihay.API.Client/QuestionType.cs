﻿namespace Thihay.API.Client
{
    public enum QuestionType
    {
        NoType=0,
        MultipleChoice = 1,
        ReadingComprehension = 2,
        FillInTheBlank = 3,
        RewriteTheSentences = 4,
        ClozeTest = 5,
        Matching = 6,
        Arrange = 7,
        DragAndDrop = 8,
        Groups = 9,
        Underline = 10,
        UnderlineAndRewrite = 11,
        DeleteCharacter = 12
    }
}