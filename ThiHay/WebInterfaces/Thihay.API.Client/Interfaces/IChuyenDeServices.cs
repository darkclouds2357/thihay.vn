﻿using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Interfaces
{
    public interface IChuyenDeServices
    {
        Task<TopicModel> GetAllTopics(string searchKey, int? subjectId, int? classId, int? page, int? pageSize);

        Task<Topic> GetTopicDetail(int topicId);

        Task<bool> UpdateTopic(Topic topic);

        Task<bool> ChangeTopicStatus(int topicId, bool isDelete);

        Task<UnitModel> GetAllUnits(string searchKey, int? subjectId, int? classId, int? topicId, int? page, int? pageSize);

        Task<Unit> GetUnitDetail(int unitId);

        Task<bool> UpdateUnit(Unit unit);

        Task<bool> UpdateUnitOrder(int unitId, int newOrder);

        Task<bool> ChangeUnitStatus(int unitId, bool isDelete);

        //void SetAuthentication(string token);
    }
}