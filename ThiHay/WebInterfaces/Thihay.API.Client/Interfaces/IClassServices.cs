﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Interfaces
{
    public interface IClassServices
    {
        Task<IEnumerable<Class>> GetClass(string searchKey);

        Task<bool> UpdateClass(Class classDto);

        Task<bool> ChangeClassStatus(int classId, bool isDelete);

        //void SetAuthentication(string token);
    }
}