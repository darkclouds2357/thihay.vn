﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Models;
using Thihay.User.ViewModel;

namespace Thihay.API.Client.Interfaces
{
    public interface IEndUserService
    {
        Task<IEnumerable<SummaryModel>> GetSummary();

        Task<IEnumerable<Class>> GetClasses(int subjectId);

        Task<IEnumerable<Topic>> GetTopics(int subjectId, int classId);

        Task<IEnumerable<Subject>> GetAllSubject();

        Task<Unit> GetUnit(int id);

        Task<Question> GetRandomQuestion(int unitId, int userId);
        Task<UserProfile> GetUserProfile(int? subjectId);
        Task<ExecutionResult<int>> SubmitAnswer(Question question);

        // void SetAuthentication(string token);
    }
}