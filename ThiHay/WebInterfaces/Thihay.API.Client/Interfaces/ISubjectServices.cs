﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Interfaces
{
    public interface ISubjectServices
    {
        Task<IEnumerable<Subject>> GetSubject(string searchKey);

        Task<bool> UpdateSubject(Subject subject);

        Task<bool> ChangeSubjectStatus(int subjectId, bool isDelete);

        // void SetAuthentication(string token);
    }
}