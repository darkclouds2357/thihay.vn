﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thihay.API.Client.Interfaces
{
    public interface ITokenModel
    {
        string AccessToken { get; set; }
        bool IsAdmin { get; set; }
        void UpdateToken(string token, bool isAdmin = false);
    }
}
