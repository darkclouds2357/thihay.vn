﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thihay.API.Client.Models;
using Thihay.Common;

namespace Thihay.API.Client.Interfaces
{
    public interface IIdentityService
    {

        Task<ExecutionResult<TokenModel>> Login(string username, string password, bool isAdminLogin = false);
        Task<IThihayUser> GetLoginUserInfo();
        Task<ExecutionResult> ChangePassword(ChangePasswordModel model);
        Task<string> FacebookLogin(string facebookToken);
        Task<string> GoogleLogin(string googleToken);
        Task<ExecutionResult<string>> RegisterNewUser(RegisterModel model);
    }
}