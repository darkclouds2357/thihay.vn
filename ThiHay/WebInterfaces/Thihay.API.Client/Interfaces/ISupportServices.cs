﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Thihay.API.Client.Interfaces
{
    public interface ISupportServices
    {
        Task<string> UploadImageAsync(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode);
    }
}