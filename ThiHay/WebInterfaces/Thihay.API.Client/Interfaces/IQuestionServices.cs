﻿using System.Threading.Tasks;
using Thihay.API.Client.Models;

namespace Thihay.API.Client.Interfaces
{
    public interface IQuestionServices
    {
        Task<Question> GetQuestionDetail(int questionId);
        
        Task<bool> UpdateQuestion(Question questionDto);
        Task<QuestionModel> GetAllQuestion(string searchKey, int? unitId, int? subjectId, int? classId, int? topicId, QuestionType? questionType, int? page, int? pageSize);
        Task<bool> ChangeQuestionStatus(int questionId, bool isDelete);
        //void SetAuthentication(string token);
    }
}