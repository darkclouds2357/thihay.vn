﻿namespace Thihay.API.Client
{
    public class AppSettings
    {
        public string ChuyenDeUrl { get; set; }
        public string ClassUrl { get; set; }
        public string QuestionUrl { get; set; }
        public string SubjectUrl { get; set; }
        public string EndUserUrl { get; set; }

        public string IdentityUrl { get; set; }

        public string UploadUrl { get; set; }
    }
}