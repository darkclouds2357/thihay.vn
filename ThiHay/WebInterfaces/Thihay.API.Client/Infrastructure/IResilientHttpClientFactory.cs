﻿using Microsoft.Resilience.Http;

namespace Thihay.API.Client.Infrastructure
{
    public interface IResilientHttpClientFactory
    {
        ResilientHttpClient CreateResilientHttpClient();
    }
}