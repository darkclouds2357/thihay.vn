﻿namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public static class QuestionAPI
        {
            public static string GetQuestionDetail(string baseUri, int questionId)
            {
                return $"{baseUri}/{questionId}";
            }

            public static string GetAllQuestion(string baseUri, string searchKey, int? subjectId, int? classId, int? topicId, int? unitId, QuestionType? questionType, int? page, int? pageSize)
            {
                var url = $"{baseUri}";

                url = !string.IsNullOrWhiteSpace(searchKey) ? $"{url}?searchKey={searchKey}" : url;

                url = subjectId.HasValue && url.Contains("?") ?
                    $"{url}&subjectId={subjectId.Value}" : url;
                url = subjectId.HasValue && !url.Contains("?") ?
                    $"{url}?subjectId={subjectId.Value}" : url;

                url = classId.HasValue && url.Contains("?") ?
                    $"{url}&classId={classId.Value}" : url;
                url = classId.HasValue && !url.Contains("?") ?
                    $"{url}?classId={classId.Value}" : url;

                url = topicId.HasValue && url.Contains("?") ?
                   $"{url}&topicId={topicId.Value}" : url;
                url = topicId.HasValue && !url.Contains("?") ?
                    $"{url}?topicId={topicId.Value}" : url;

                url = unitId.HasValue && url.Contains("?") ?
                   $"{url}&unitId={unitId.Value}" : url;
                url = unitId.HasValue && !url.Contains("?") ?
                    $"{url}?unitId={unitId.Value}" : url;

                url = questionType.HasValue && url.Contains("?") ?
                   $"{url}&questionType={questionType.Value}" : url;
                url = questionType.HasValue && !url.Contains("?") ?
                    $"{url}?questionType={questionType.Value}" : url;

                url = page.HasValue && url.Contains("?") ?
                    $"{url}&page={page.Value}" : url;
                url = page.HasValue && !url.Contains("?") ?
                    $"{url}?page={page.Value}" : url;

                url = pageSize.HasValue && url.Contains("?") ?
                   $"{url}&pageSize={pageSize.Value}" : url;
                url = pageSize.HasValue && !url.Contains("?") ?
                    $"{url}?pageSize={pageSize.Value}" : url;

                return url;
            }

            public static string UpdateQuestion(string baseUri)
            {
                return $"{baseUri}/update";
            }

            public static string ChangeQuestionStatus(string baseUri, int questionId, bool isDelete)
            {
                return $"{baseUri}/{questionId}/status/{isDelete}";
            }
        }
    }
}