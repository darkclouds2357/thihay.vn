﻿namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public static class SubjectAPI
        {
            public static string GetAllSubject(string baseUri, string searchKey)
            {
                if (!string.IsNullOrWhiteSpace(searchKey))
                {
                    baseUri = $"{baseUri}?searchKey={searchKey}";
                }
                return baseUri;
            }

            public static string ChangeSubjectStatus(string baseUri, int subjectId, bool isDelete)
            {
                return $"{baseUri}/{subjectId}/status/{isDelete}";
            }

            public static string UpdateSubject(string baseUri)
            {
                return $"{baseUri}/update";
            }
        }
    }
}