﻿namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public static class ChuyenDeAPI
        {
            public static string GetAllTopics(string baseUri, string searchKey, int? subjectId, int? classId, int? page, int? pageSize)
            {
                var url = $"{baseUri}/topic";

                url = !string.IsNullOrWhiteSpace(searchKey) ? $"{url}?searchKey={searchKey}" : url;

                url = subjectId.HasValue && url.Contains("?") ?
                    $"{url}&subjectId={subjectId.Value}" : url;
                url = subjectId.HasValue && !url.Contains("?") ?
                    $"{url}?subjectId={subjectId.Value}" : url;

                url = classId.HasValue && url.Contains("?") ?
                    $"{url}&classId={classId.Value}" : url;
                url = classId.HasValue && !url.Contains("?") ?
                    $"{url}?classId={classId.Value}" : url;

                url = page.HasValue && url.Contains("?") ?
                    $"{url}&page={page.Value}" : url;
                url = page.HasValue && !url.Contains("?") ?
                    $"{url}?page={page.Value}" : url;

                url = pageSize.HasValue && url.Contains("?") ?
                   $"{url}&pageSize={pageSize.Value}" : url;
                url = pageSize.HasValue && !url.Contains("?") ?
                    $"{url}?pageSize={pageSize.Value}" : url;

                return url;
            }

            public static string GetTopicDetail(string baseUri, int topicId)
            {
                return $"{baseUri}/topic/{topicId}";
            }

            public static string UpdateTopic(string baseUri)
            {
                return $"{baseUri}/topic/update";
            }

            public static string ChangeTopicStatus(string baseUri, int topicId, bool isDelete)
            {
                return $"{baseUri}/topic/{topicId}/status/{isDelete}";
            }

            public static string GetAllUnits(string baseUri, string searchKey, int? subjectId, int? classId, int? topicId, int? page, int? pageSize)
            {
                var url = $"{baseUri}/unit";

                url = !string.IsNullOrWhiteSpace(searchKey) ? $"{url}?searchKey={searchKey}" : url;

                url = subjectId.HasValue && url.Contains("?") ?
                    $"{url}&subjectId={subjectId.Value}" : url;
                url = subjectId.HasValue && !url.Contains("?") ?
                    $"{url}?subjectId={subjectId.Value}" : url;

                url = classId.HasValue && url.Contains("?") ?
                    $"{url}&classId={classId.Value}" : url;
                url = classId.HasValue && !url.Contains("?") ?
                    $"{url}?classId={classId.Value}" : url;

                url = topicId.HasValue && url.Contains("?") ?
                   $"{url}&topicId={topicId.Value}" : url;
                url = topicId.HasValue && !url.Contains("?") ?
                    $"{url}?topicId={topicId.Value}" : url;

                url = page.HasValue && url.Contains("?") ?
                    $"{url}&page={page.Value}" : url;
                url = page.HasValue && !url.Contains("?") ?
                    $"{url}?page={page.Value}" : url;

                url = pageSize.HasValue && url.Contains("?") ?
                   $"{url}&pageSize={pageSize.Value}" : url;
                url = pageSize.HasValue && !url.Contains("?") ?
                    $"{url}?pageSize={pageSize.Value}" : url;

                return url;
            }

            public static string GetUnitDetail(string baseUri, int unitId)
            {
                return $"{baseUri}/unit/{unitId}";
            }

            public static string UpdateUnit(string baseUri)
            {
                return $"{baseUri}/unit/update";
            }

            public static string UpdateUnitOrder(string baseUri, int unitId, int newOrder)
            {
                return $"{baseUri}/unit/{unitId}/order/{newOrder}";
            }

            public static string ChangeUnitStatus(string baseUri, int unitId, bool isDelete)
            {
                return $"{baseUri}/unit/{unitId}/status/{isDelete}";
            }
        }
    }
}