﻿namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public static class ClassAPI
        {
            public static string GetAllClass(string baseUri, string searchKey)
            {
                if (!string.IsNullOrWhiteSpace(searchKey))
                {
                    baseUri = $"{baseUri}?searchKey={searchKey}";
                }
                return baseUri;
            }

            public static string ChangeClassStatus(string baseUri, int classId, bool isDelete)
            {
                return $"{baseUri}/{classId}/status/{isDelete}";
            }

            public static string UpdateClass(string baseUri)
            {
                return $"{baseUri}/update";
            }
        }
    }
}