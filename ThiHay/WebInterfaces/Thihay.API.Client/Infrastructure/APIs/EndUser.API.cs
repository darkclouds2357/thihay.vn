﻿using System;

namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public static class EndUser
        {
            public static string GetSummary(string baseUri)
            {
                return $"{baseUri}/summary";
            }

            public static string GetClasses(string baseUri, int subjectId)
            {
                return $"{baseUri}/{subjectId}/class";
            }

            public static string GetAllSubjects(string baseUri)
            {
                return $"{baseUri}/subject";
            }

            public static string GetTopics(string baseUri, int subjectId, int classId)
            {
                return $"{baseUri}/{subjectId}/class/{classId}/topics";
            }

            public static string GetUnit(string baseUri, int unitId)
            {
                return $"{baseUri}/unit/{unitId}";
            }

            public static string GetRandomQuestion(string baseUri, int unitId, int userId)
            {
                return $"{baseUri}/unit/{unitId}/randomQuestion?userId={userId}";
            }

            public static string GetUserProfile(string baseUri, int? subjectId)
            {
                var url = $"{baseUri}/userProfile";
                if (subjectId.HasValue)
                {
                    url += $"?subjectId={subjectId.Value}";
                }
                return url;
            }

            public static string SubmitAnswer(string baseUri)
            {
                return $"{baseUri}/submitAnswer";
            }
        }
    }
}