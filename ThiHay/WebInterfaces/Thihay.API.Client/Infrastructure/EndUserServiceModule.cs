﻿using Autofac;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Services;

namespace Thihay.API.Client.Infrastructure
{
    public class EndUserServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EndUserService>()
                .As<IEndUserService>();
            builder.RegisterType<IdentityService>()
                .As<IIdentityService>();
        }
    }
}