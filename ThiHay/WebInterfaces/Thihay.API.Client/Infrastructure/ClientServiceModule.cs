﻿using Autofac;
using Thihay.API.Client.Interfaces;
using Thihay.API.Client.Services;

namespace Thihay.API.Client.Infrastructure
{
    public class ClientServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ChuyenDeServices>()
                .As<IChuyenDeServices>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ClassServices>()
                .As<IClassServices>()
                .InstancePerLifetimeScope();
            builder.RegisterType<QuestionServices>()
                .As<IQuestionServices>()
                .InstancePerLifetimeScope();
            builder.RegisterType<SubjectServices>()
                .As<ISubjectServices>()
                .InstancePerLifetimeScope();
            builder.RegisterType<IdentityService>()
               .As<IIdentityService>();
        }
    }
}