﻿using System;

namespace Thihay.API.Client.Infrastructure
{
    public static partial class API
    {
        public class IdentityAPI
        {
            public static string Login(string identityUrl)
            {
//                var credentials = @"
//grant_type=password
//&client_id=099153c2625149bc8ecb3e85e03f0022
//&username=
//&password=

//;
//headers.append('Content-Type', 'application/x-www-form-urlencoded');";
                return $"{identityUrl}/oauth2/token";
            }
            
            public static string GetUserInfo(string identityUrl)
            {
                return $"{identityUrl}/api/UserInfo";
            }

            public static string ChangePassword(string identityUrl)
            {
                return $"{identityUrl}/api/UserInfo/changepassword";
            }

            public static string FacebookLogin(string identityUrl)
            {
                return $"{identityUrl}/oauth2/sign-in-facebook";
            }

            public static string GoogleLogin(string identityUrl)
            {
                return $"{identityUrl}/oauth2/sign-in-google";
            }

            public static string RegisterNewUser(string identityUrl)
            {
                return $"{identityUrl}/api/UserInfo/newUser";
            }
        }
    }
}