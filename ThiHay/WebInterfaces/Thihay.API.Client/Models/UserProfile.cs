﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thihay.API.Client.Models
{
    public class UserProfile
    {
        public UserProfile(IList<Mark> marks)
        {
            Marks = marks;
            SubjectId = Marks.FirstOrDefault()?.SubjectId ?? 0;
        }

        public int SubjectId { get; set; }

        public IList<Mark> Marks { get; set; }
        public double AvgPoint => Marks.Count > 0 ? Math.Round(TotalPoint / Marks.Count, 2, MidpointRounding.AwayFromZero) : 0;
        public double TotalPoint => Marks.Sum(m => m.Point);
        public int QuestionCount => Marks.Sum(m => m.TotalAnswerd);
        public int QuestionsPassed => Marks.Sum(m => m.QuestionsPassed);
        public double AnswerRate => QuestionCount > 0 ? Math.Round(QuestionsPassed * 100.00 / QuestionCount, 2, MidpointRounding.AwayFromZero) : 0;
    }

    public class Mark
    {
        [JsonProperty(PropertyName = "unitId")]
        public int UnitId { get; set; }
        [JsonProperty(PropertyName = "unitName")]
        public string UnitName { get; set; }
        [JsonProperty(PropertyName = "questionsPassed")]
        public int QuestionsPassed { get; set; }
        [JsonProperty(PropertyName = "totalAnswerd")]
        public int TotalAnswerd { get; set; }
        [JsonProperty(PropertyName = "practiceTime")]
        public int PracticeTime { get; set; }
        [JsonProperty(PropertyName = "mark")]
        public double Point { get; set; }
        [JsonProperty(PropertyName = "classId")]
        public int ClassId { get; set; }
        [JsonProperty(PropertyName = "className")]
        public string ClassName { get; set; }
        [JsonProperty(PropertyName = "subjectId")]
        public int SubjectId { get; set; }
        [JsonProperty(PropertyName = "subjectName")]
        public string SubjectName { get; set; }
        public int InCorrectQuestion => TotalAnswerd - QuestionsPassed;
    }
}
