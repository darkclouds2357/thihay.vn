﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Thihay.API.Client.Models
{
    public class LoginModel
    {
        [Required]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
        [Required]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "lastName")]
        [Required]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "userName")]
        [Required]
        public string UserName { get; set; }
        [JsonProperty(PropertyName = "password")]
        [Required]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [Required]
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [Required]
        [JsonProperty(PropertyName = "confirmPassword")]
        public string ConfirmPassword { get; set; }

        [JsonProperty(PropertyName = "classGrade")]
        [Range(1, 12, ErrorMessage = "Trình độ học vấn từ 1 đến 12")]
        public byte? ClassGrade { get; set; }
    }

    public class ChangePasswordModel
    {
        [JsonProperty(PropertyName = "currentPassword")]
        public string CurrentPassword { get; set; }
        [JsonProperty(PropertyName = "newPassword")]
        public string NewPassword { get; set; }
        [JsonProperty(PropertyName = "confirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}
