﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thihay.API.Client.Models
{
    public class TopicModel
    {
        [JsonProperty(PropertyName = "totalPage")]
        public int TotalPage { get; set; }

        [JsonProperty(PropertyName = "topics")]
        public List<Topic> Topics { get; set; }
    }
}