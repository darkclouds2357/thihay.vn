﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thihay.API.Client.Models
{
    public class UnitModel
    {
        [JsonProperty(PropertyName = "totalPage")]
        public int TotalPage { get; set; }

        [JsonProperty(PropertyName = "units")]
        public List<Unit> Units { get; set; }
    }
}