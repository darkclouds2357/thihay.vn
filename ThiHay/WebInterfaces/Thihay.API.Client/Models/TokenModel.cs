﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Thihay.API.Client.Interfaces;

namespace Thihay.API.Client.Models
{
    public class TokenModel : ITokenModel
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "isAdmin")]
        public bool IsAdmin { get; set; }

        public void UpdateToken(string token, bool isAdmin = false)
        {
            AccessToken = token;
            IsAdmin = isAdmin;
        }
    }
}
