﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thihay.API.Client.Models
{
    public class QuestionModel
    {
        [JsonProperty(PropertyName = "totalPage")]
        public int TotalPage { get; set; }

        [JsonProperty(PropertyName = "questions")]
        public List<Question> Questions { get; set; }
    }
}