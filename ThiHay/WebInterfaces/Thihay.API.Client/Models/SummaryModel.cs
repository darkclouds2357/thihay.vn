﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thihay.User.ViewModel
{
    public class SummaryModel
    {
        [JsonProperty(PropertyName = "classId")]
        public int ClassId { get; set; }

        [JsonProperty(PropertyName = "className")]
        public string ClassName { get; set; }

        [JsonProperty(PropertyName = "subjectSummary")]
        public IList<SummaryDetailModel> SubjectSummary { get; set; }
    }
}