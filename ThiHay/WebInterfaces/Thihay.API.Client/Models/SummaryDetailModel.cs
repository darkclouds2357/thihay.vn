﻿using Newtonsoft.Json;

namespace Thihay.User.ViewModel
{
    public class SummaryDetailModel
    {
        [JsonProperty(PropertyName = "subjectId")]
        public int SubjectId { get; set; }

        [JsonProperty(PropertyName = "subjectName")]
        public string SubjectName { get; set; }

        [JsonProperty(PropertyName = "totalUnit")]
        public int TotalUnit { get; set; }
    }
}