﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Authentication.API.Model
{
    public class IdentityModel
    {
        public ClaimsIdentity Identity { get; set; }
        public bool IsAdmin { get; set; }
    }
}
