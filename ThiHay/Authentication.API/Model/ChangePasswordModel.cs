﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.API.Model
{
    public class ChangePasswordModel
    {
        [JsonProperty(PropertyName = "currentPassword")]
        public string CurrentPassword { get; set; }
        [JsonProperty(PropertyName = "newPassword")]
        public string NewPassword { get; set; }
        [JsonProperty(PropertyName = "confirmPassword")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterModel
    {
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "confirmPassword")]
        public string ConfirmPassword { get; set; }
        [JsonProperty(PropertyName = "classGrade")]
        public byte? ClassGrade { get; set; }
    }    
}
