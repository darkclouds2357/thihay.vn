﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.API.Model
{
    public class SerectModel
    {
        public int? UserId { get; set; }
        public string ClientId { get; set; }
        public string Serect { get; set; }
    }
}
