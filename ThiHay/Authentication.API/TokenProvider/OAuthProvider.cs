﻿using Authentication.API.Model;
using Authentication.API.Services;
using Authentication.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Thihay.Common;

namespace Authentication.API.TokenProvider
{
    public class OAuthProvider
    {
        private readonly IOAuthService _oauthService;
        //private DateTime _unixTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public OAuthProvider(IOAuthService oauthService)
        {
            _oauthService = oauthService;
        }

        /////// <summary>
        /////// Resolves a user identity given a username and password.
        /////// </summary>
        public async Task<IdentityModel> IdentityResolver(string userName, string password, bool isAdminLogin = false)
        {
            var authenticationResult = await Task.Run(() => _oauthService.Authenticate(userName, password, isAdminLogin));
            if (authenticationResult != null)
            {
                var result = new IdentityModel()
                {
                    Identity = SetClaimsIdentity(authenticationResult),
                    IsAdmin = authenticationResult.IsSystemUser
                };
                return result;
            }
            return null;
        }

        public ClaimsIdentity SetClaimsIdentity(IThihayUser data)
        {

            var identity = new ClaimsIdentity("JWT");
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "" + data.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Name, "" + data.FullName));
            identity.AddClaim(new Claim(ClaimTypes.GivenName, "" + data.FirstName));
            identity.AddClaim(new Claim(ClaimTypes.Surname, "" + data.LastName));
            identity.AddClaim(new Claim(ClaimTypes.PrimarySid, "" + data.UserId));
            identity.AddClaim(new Claim("GOOGLE_ID", "" + data.GoogleId));
            identity.AddClaim(new Claim("FACEBOOK_ID", "" + data.GoogleId));
            identity.AddClaim(new Claim(ClaimTypes.Email, "" + data.Email));
            identity.AddClaim(new Claim(ClaimTypes.StreetAddress, "" + data.Address));
            identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, "" + data.DateOfBirth.ToString("dd MMM yyyy")));
            identity.AddClaim(new Claim(ClaimTypes.MobilePhone, "" + data.Phone));
            identity.AddClaim(new Claim("PPROVINCE_ID", "" + data.ProvinceId));
            identity.AddClaim(new Claim(ClaimTypes.StateOrProvince, "" + data.ProvinceName));
            identity.AddClaim(new Claim("SCHOOL_ID", "" + data.SchoolId));
            identity.AddClaim(new Claim("SCHOOL_NAME", "" + data.SchoolName));
            identity.AddClaim(new Claim("VILLAGE_ID", "" + data.VillageId));
            identity.AddClaim(new Claim("VILLAGE_NAME", "" + data.VillageName));
            identity.AddClaim(new Claim("IS_SYSTEM_USER", data.IsSystemUser ? "true" : "false"));
            identity.AddClaim(new Claim("VIP_DAY_LEFT", "" + data.VipDayLeft));
            identity.AddClaim(new Claim("LOGIN_TIME_UNIX_TIMESTAMPS", "" + ToUnixEpochDate(DateTime.Now)));
            identity.AddClaim(new Claim("CREATE_DATE", "" + data.CreatedDate.ToString("dd MMM yyyy")));
            return identity;
        }

        public async Task<ClaimsIdentity> GoogleIdentityResolver(HttpContext context)
        {
            IThihayUser user = await Task.Run(() => _oauthService.GoogleAuthenticate(context));

            if (user != null)
            {
                return SetClaimsIdentity(user);
            }
            return null;
        }

        public async Task<ClaimsIdentity> FacebookIdentityResolver(HttpContext context)
        {
            IThihayUser user = await Task.Run(() => _oauthService.FacebookAuthenticate(context));
            if (user != null)
            {
                return SetClaimsIdentity(user);
            }
            return null;
        }


        /// <summary>
        /// Get this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        public long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}