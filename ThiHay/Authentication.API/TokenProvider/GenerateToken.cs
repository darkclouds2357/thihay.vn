﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Authentication.API.TokenProvider
{
    public static class GenerateToken
    {
        public static string Token(string userName, string clientId, string issuer, TimeSpan expiration, SigningCredentials signingCredentials, List<Claim> claims)
        {

            var expires = DateTime.UtcNow.Add(expiration);
            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: issuer,
                audience: _credential.ClientId,
                claims: claims,
                notBefore: now,
                expires: expires,
                signingCredentials: _credential.SigningCredentials);
            return new JwtSecurityTokenHandler().WriteToken(jwt);


        }
    }
}
