﻿using Authentication.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Thihay.Common;
using Authentication.API.Model;
using Thihay.API.Client.Models;

namespace Authentication.API.Services
{
    public interface IOAuthService
    {
        IThihayUser Authenticate(string userName, string password, bool isAdminLogin = false);
        Task<string> ResetPassword(int? userId);
        IThihayUser GoogleAuthenticate(HttpContext context);
        IThihayUser FacebookAuthenticate(HttpContext context);
        Task<ExecutionResult> ChangePassword(ChangePasswordModel model);
        Task<ExecutionResult<IThihayUser>> CreateNewUser(RegisterModel model);
    }
}