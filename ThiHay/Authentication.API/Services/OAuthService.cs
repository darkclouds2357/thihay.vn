﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authentication.Domain.Entities;
using ST.Library.Base.UnitOfWork;
using ST.Library.Common.EncryptedPassword;
using Thihay.Common;
using Microsoft.AspNetCore.Http;
using Authentication.API.Model;
using Thihay.API.Client.Models;
using Microsoft.Extensions.Options;
using Authentication.API.TokenProvider;
using Thihay.Common.TokenProvider;
using System.Text.RegularExpressions;

namespace Authentication.API.Services
{
    public class OAuthService : IOAuthService
    {
        private const int PASSWORD_LENGTH = 8;
        private const int NON_ALPHAL_CHARACTER = 0;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IThihayUser _user;
        private readonly Credential _credential;

        public OAuthService(IUnitOfWork unitOfWork, IThihayUser user, Credential credential)
        {
            _unitOfWork = unitOfWork;
            _user = user;
            _credential = credential;
        }
        public IThihayUser Authenticate(string userName, string password, bool isAdminLogin = false)
        {
            var authUser = _unitOfWork.GetRepository<AuthUser>().Find(u => u.UserName == userName);
            if (authUser != null && HashEncrypted.IsVerifyStringMatch(password, authUser.Password, authUser.Salt))
            {
                return new ThihayUser(authUser.UserType > 0 && isAdminLogin)
                {
                    Address = authUser.Address,
                    DateOfBirth = authUser.DateOfBirth ?? DateTime.Now,
                    Email = authUser.Email,
                    FacebookId = authUser.FacebookId,
                    FirstName = authUser.FirstName,
                    FullName = authUser.FullName,
                    GoogleId = authUser.GoogleId,
                    LastName = authUser.LastName,
                    Phone = authUser.Phone,
                    ProvinceId = authUser.ProvinceId ?? default(int),
                    ProvinceName = authUser.ProvinceName,
                    SchoolId = authUser.SchoolId,
                    SchoolName = authUser.SchoolName,
                    Id = Guid.NewGuid(),
                    UserId = authUser.AuthUserId,
                    UserName = authUser.UserName,
                    VillageId = authUser.VillageId ?? default(int),
                    VillageName = authUser.VillageName,
                    CreatedDate = authUser.CreatedOn ?? DateTime.Now
                };
            }
            return null;
        }


        public async Task<ExecutionResult<IThihayUser>> CreateNewUser(RegisterModel model)
        {
            var result = new ExecutionResult<IThihayUser>();
            try
            {
                var error = ValidatePassword(model.Password, model.ConfirmPassword);
                error.AddRange(ValidateModel(model));
                var userRepository = _unitOfWork.GetRepository<AuthUser>();
                if (userRepository.GetAll().Any(u => u.UserName.ToLower() == model.UserName.ToLower() || u.Email.ToLower() == model.Email.ToLower()))
                {
                    error.Add("Thông tin đã được tồn tại");
                }
                if (error.Count == 0)
                {
                    var salt = HashEncrypted.GenerateSalt(32);
                    var passwordEncrypted = HashEncrypted.EncryptedHash(model.Password, salt);

                    var newAuth = new AuthUser
                    {
                        LastName = model.LastName,
                        FirstName = model.FirstName,
                        FullName = $"{model.FirstName} {model.LastName}",
                        Email = model.Email,
                        Phone = model.Phone,
                        UserName = model.UserName,
                        Password = passwordEncrypted,
                        Salt = salt,
                        UserType = 0,
                        ClassGrade = model.ClassGrade
                    };
                    await userRepository.InsertAsync(newAuth);
                    var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;

                    if (isSuccess)
                    {
                        result.Result = Authenticate(model.UserName, model.Password); ; /// Generate new token
                        result.Success = true;
                        return result;
                    }

                }
                result.Success = false;
                result.Messages = error.ToArray();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
            }
            return result;
        }

        private List<string> ValidateModel(RegisterModel model)
        {
            var error = new List<string>();
            if (model.UserName == model.Password)
            {
                error.Add("Tài khoản đăng nhập và mật khẩu không được trùng nhau");
            }
            if (model.UserName.Any(x => char.IsWhiteSpace(x)))
            {
                error.Add("Tài khoản đăng nhập không được có khoảng trắng");
            }
            if (model.UserName.Length < 5 || model.UserName.Length > 20)
            {
                error.Add("Tài khoản đăng nhập không được ngắn hơn 5 ký tự và nhiều hơn 20 ký tự");
            }
            if(!Regex.IsMatch(model.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                error.Add("Email không hợp lệ");
            }
            //if(!model.FirstName.All(char.IsLetterOrDigit) || !model.LastName.All(char.IsLetterOrDigit) || !model.UserName.All(char.IsLetterOrDigit))
            //{
            //    error.Add("Không được phép sử dụng các ký tự đặc biệt");
            //}
            return error;
        }

        public async Task<ExecutionResult> ChangePassword(ChangePasswordModel model)
        {
            var result = new ExecutionResult();
            try
            {
                var currentUser = _unitOfWork.GetRepository<AuthUser>().Find(_user.UserId);
                if (currentUser != null)
                {
                    var error = ValidatePassword(model.NewPassword, model.ConfirmPassword);
                    if(model.NewPassword == currentUser.UserName)
                    {
                        error.Add("Tài khoản đăng nhập và mật khẩu không được trùng nhau");
                    }
                    /// for the login with facebook and google that dont have password
                    /// validate current password for case user login with thihay
                    if (!string.IsNullOrWhiteSpace(currentUser.Password) && !string.IsNullOrWhiteSpace(currentUser.Salt))
                    {
                        if (!HashEncrypted.IsVerifyStringMatch(model.CurrentPassword, currentUser.Password, currentUser.Salt))
                        {
                            error.Add("Mật Khẩu Không Chính Xác.");
                        }
                    }

                    if (error.Count == 0)
                    {
                        var salt = HashEncrypted.GenerateSalt(32);
                        var passwordEncrypted = HashEncrypted.EncryptedHash(model.NewPassword, salt);

                        currentUser.Password = passwordEncrypted;
                        currentUser.Salt = salt;
                        _unitOfWork.GetRepository<AuthUser>().Update(currentUser);
                        result.Success = await _unitOfWork.SaveChangesAsync() > 0;
                        return result;
                    }
                    result.Messages = error.ToArray();
                }

                result.Success = false;

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
            }
            return result;
        }

        private List<string> ValidatePassword(string password, string confrimPassword)
        {
            var errors = new List<string>();
            if (!password.Equals(confrimPassword))
            {
                errors.Add("Mật Khẩu Xác Nhận Không Đúng");
            }
            if (password.Length < 8)
            {
                errors.Add("Mật khẩu phải từ 8 ký tự trở lên");
            }
            return errors;
        }

        public IThihayUser FacebookAuthenticate(HttpContext context)
        {
            var tokenString = context.Request.Form["token"];



            throw new NotImplementedException();
        }

        public IThihayUser GoogleAuthenticate(HttpContext context)
        {
            var tokenString = context.Request.Form["token"];


            throw new NotImplementedException();
        }

        public async Task<string> ResetPassword(int? userId)
        {
            try
            {
                var newPassword = HashEncrypted.GenerateStrongPassword(PASSWORD_LENGTH, NON_ALPHAL_CHARACTER);
                var authenUserRepository = _unitOfWork.GetRepository<AuthUser>();
                if (userId.HasValue)
                {
                    var authenUser = authenUserRepository.Find(userId);
                    if (authenUser != null)
                    {
                        var salt = HashEncrypted.GenerateSalt(32);
                        var passwordEncrypt = HashEncrypted.EncryptedHash(newPassword, salt);
                        authenUser.Salt = salt;
                        authenUser.Password = passwordEncrypt;
                        authenUserRepository.Update(authenUser);
                    }
                }
                else
                {
                    var allUser = authenUserRepository.GetAll().ToList();
                    foreach (var authenUser in allUser)
                    {
                        var salt = HashEncrypted.GenerateSalt(32);
                        var passwordEncrypt = HashEncrypted.EncryptedHash(newPassword, salt);
                        authenUser.Salt = salt;
                        authenUser.Password = passwordEncrypt;
                    }
                    authenUserRepository.Update(allUser);
                }
                if (await _unitOfWork.SaveChangesAsync() > 0)
                    return newPassword;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }


    }
}
