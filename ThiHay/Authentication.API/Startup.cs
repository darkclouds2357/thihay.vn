﻿using Authentication.API.Services;
using Authentication.API.TokenProvider;
using Authentication.Domain;
using Authentication.Domain.Infrastructure;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ST.Library.Common;
using System;
using System.Security.Claims;
using Thihay.Common;
using Thihay.Common.Securities;
using Thihay.Common.TokenProvider;

namespace Authentication.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("keys.json", optional: false, reloadOnChange: true)
                .AddJsonFile("social.keys.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddAuthenticationContext(Configuration["ConnectionString"]);
            services.AddUnitOfWork<AuthenticationContext>();
            services.AddScoped<ICurrentUser, ThihayUser>();
            services.AddTransient<IAudienceStore, AudiencesStore>(sp =>
            {
                return new AudiencesStore(Configuration["ClientId"], Configuration["Secret"]);
            });
            services.AddTransient<IOAuthService, OAuthService>();
            services.AddTransient<OAuthProvider>();

            services.AddTransient<Credential>(sp =>
            {
                return new Credential(sp.GetRequiredService<IAudienceStore>(), Configuration["ClientId"], Configuration["Name"]);
            });

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Thihay HTTP Authentication API",
                    Version = "v1",
                    Description = "The Thihay Authentication API",
                    TermsOfService = "Terms Of Service"
                });
            });

            services.AddMvc();

            var container = new ContainerBuilder();
            container.Populate(services);
            container.Register(c => c.Resolve<ICurrentUser>() as IThihayUser);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            ConfigureAuth(app);
            app.UseMvc();
        }

        private void ConfigureAuth(IApplicationBuilder app)
        {
            var provider = app.ApplicationServices.GetService<OAuthProvider>();
            var credential = app.ApplicationServices.GetService<Credential>();
            app.UseTokenProvider(new TokenProviderOptions
            {
                Path = "/oauth2/token",
                //Audience = "ExampleAudience",
                //Issuer = "ExampleIssuer",
                Provider = provider,
                Credential = credential
                //SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                //IdentityResolver = GetIdentity
            });
            //app.UseFacebookTokenProvider(new TokenProviderOptions
            //{
            //    Path = "/oauth2/sign-in-facebook",
            //    Provider = provider,
            //    Credential = credential
            //});
            //app.UseGoogleTokenProvider(new TokenProviderOptions
            //{
            //    Path = "/oauth2/sign-in-google",
            //    Provider = provider,
            //    Credential = credential
            //});


            //app.UseFacebookAuthentication(new FacebookOptions
            //{
            //    AppId = Configuration["facebook:appid"],
            //    AppSecret = Configuration["facebook:appsecret"],
            //    //Scope = { "email" },
            //    //Fields = { "name", "email" },
            //    SaveTokens = true,
            //    SignInScheme = "Cookies"
            //});

            //app.UseGoogleAuthentication(new GoogleOptions()
            //{
            //    ClientId = Configuration["google:clientid"],
            //    ClientSecret = Configuration["google:clientsecret"],
            //    SaveTokens = true,
            //    SignInScheme = "Cookies"

            //});


            app.UseSwagger()
              .UseSwaggerUI(c =>
              {
                  c.SwaggerEndpoint("/swagger/v1/swagger.json", "Thihay Authentication API V1");
              });


            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = credential.SecurityKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = credential.Issuer,
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = credential.ClientId,
                // Validate the token expiry
                ValidateLifetime = false,
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters,
                AuthenticationScheme = JwtBearerDefaults.AuthenticationScheme
            });

            app.Use(async (ctx, next) =>
            {
                var user = ctx.RequestServices.GetRequiredService<IThihayUser>();
                user.UpdateIdentity(ctx.User.Identity as ClaimsIdentity);
                await next.Invoke();
            });
        }
    }
}