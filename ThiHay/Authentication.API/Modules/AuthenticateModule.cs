﻿using Thihay.Common.Securities;
using Authentication.API.TokenProvider;
using Autofac;
using Microsoft.Extensions.Options;
using Thihay.Common.TokenProvider;

namespace Authentication.API.Modules
{
    public class AuthenticateModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AudiencesStore>().As<IAudienceStore>()
                .PropertiesAutowired();

            builder.RegisterType<OAuthProvider>().PropertiesAutowired();

            builder.Register(b =>
            {
                var audiencesStore = b.Resolve<IAudienceStore>();
                var option = b.Resolve<IOptions<AppSetting>>();
                return new Credential(audiencesStore, option.Value.ClientId, option.Value.Secret);
            }).PropertiesAutowired();

            base.Load(builder);
        }
    }
}