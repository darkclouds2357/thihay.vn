using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thihay.Common.Securities;
using Authentication.API.Model;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Newtonsoft.Json;
using Authentication.API.Services;

namespace Authentication.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Audience")]
    public class AudienceController : Controller
    {
        private readonly IAudienceStore _audienceStore;
        private IHostingEnvironment _hostingEnvironment;
        private readonly IOAuthService _service;
        public AudienceController(IAudienceStore audienceStore, IHostingEnvironment environment, IOAuthService service)
        {
            _audienceStore = audienceStore;
            _hostingEnvironment = environment;
            _service = service;
        }

        [HttpPost]
        [Route("")]
        public IActionResult GenerateAudience([FromBody]AudienceModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var authUser = _service.Authenticate(model.UserName, model.Password);
            if (authUser != null)
            {
                Audience audience = _audienceStore.AddAudience("ThihayAuthenticationServer.Api.v1");

                var rootPath = _hostingEnvironment.ContentRootPath;
                var serectJson = Path.Combine(rootPath, "keys.json");                
                using (StreamWriter file = System.IO.File.CreateText(serectJson))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, audience);
                }
                return Ok(audience);
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("resetDefaultPassword/")]
        public async Task<IActionResult> ResetPassword([FromBody]SerectModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_audienceStore.FindAudience(model.ClientId) != null && _audienceStore.FindAudience(model.ClientId).Secret == model.Serect)
            {
                var password = await _service.ResetPassword(model.UserId);
                if (!string.IsNullOrWhiteSpace(password))
                    return Ok(new { NewPassword = password });
                else
                    return BadRequest();
            }
            return BadRequest();
        }
    }
}