using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Authentication.API.Services;
using Thihay.Common;
using Authentication.API.Model;
using Microsoft.AspNetCore.Authorization;
using Authentication.API.TokenProvider;
using Microsoft.Extensions.Options;
using Thihay.Common.TokenProvider;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Thihay.API.Client.Models;

namespace Authentication.API.Controllers
{
    [Produces("application/json")]
    [Route("api/UserInfo")]
    public class UserInfoController : Controller
    {
        private readonly IOAuthService _service;
        private readonly IThihayUser _currentUser;
        private readonly OAuthProvider _provider;
        private readonly Credential _credential;
        public UserInfoController(IOAuthService service, IThihayUser currentUser, Credential credential, OAuthProvider provider)
        {
            _service = service;
            _currentUser = currentUser;
            _provider = provider;
            _credential = credential;

        }


        [HttpPost]
        [AllowAnonymous]
        [Route("newUser")]
        public async Task<IActionResult> CreateNewUser([FromBody]RegisterModel model)
        {
            var result = new ExecutionResult<string>();
            try
            {
                var newUser = await _service.CreateNewUser(model);
                if (newUser.Success && newUser.Result != null)
                {

                    var token = _provider.SetClaimsIdentity(newUser.Result);

                    var claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, model.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, await Task.FromResult(Guid.NewGuid().ToString())),
                        new Claim(JwtRegisteredClaimNames.Iat,  new DateTimeOffset(DateTime.Now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
                    };
                    claims.AddRange(token.Claims);

                    var expires = DateTime.Now.Add(TimeSpan.FromMinutes(60));
                    // Create the JWT and write it to a string
                    var jwt = new JwtSecurityToken(
                        issuer: _credential.Issuer,
                        audience: _credential.ClientId,
                        claims: claims,
                        notBefore: DateTime.Now,
                        expires: expires,
                        signingCredentials: _credential.SigningCredentials);
                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);


                    return Ok(new ExecutionResult<string>
                    {
                        Success = true,
                        Result = encodedJwt
                    });

                }
                return Ok(new ExecutionResult<string>
                {
                    Success = false,
                    Messages = newUser.Messages
                });
            }
            catch (Exception)
            {
                return BadRequest();
            }
            
           

        }


        [HttpPost]
        [Authorize]
        [Route("changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordModel model)
        {
            if (_currentUser != null)
            {
                var result = await _service.ChangePassword(model);
                return Ok(result);
            }
            return NotFound();

        }

        [HttpGet]
        [Route("")]
        public IActionResult GetCurrentUserInfo()
        {
            if (_currentUser != null)
            {
                return Ok(_currentUser);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("logout")]
        public IActionResult Logout()
        {
            return Ok();
        }
    }
}