﻿namespace Authentication.API
{
    public class AppSetting
    {
        public string ClientId { get; set; }
        public string Secret { get; set; }
    }
}