﻿using Microsoft.EntityFrameworkCore;
using ST.Library.Base.Repository;
using System;
using System.Data;
using System.Threading.Tasks;

namespace ST.Library.Base.UnitOfWork
{
    public interface IUnitOfWork : IDisposable, IRepositoryFactory
    {
        IDisposable BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        void RollbackTransaction();

        void CommitTransaction();

        DbContext GetContext();

        int SaveChanges(bool ensureAutoHistory = false);

        Task<int> SaveChangesAsync(bool ensureAutoHistory = false);
    }
}