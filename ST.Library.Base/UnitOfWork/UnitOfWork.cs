﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using ST.Library.Base.Entity;
using ST.Library.Base.Enum;
using ST.Library.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ST.Library.Base.UnitOfWork
{
    public partial class UnitOfWork<TCtx> : IUnitOfWork, IDisposable where TCtx : DbContext
    {
        private readonly ILogger<UnitOfWork<TCtx>> _log;
        private readonly TCtx _context;
        private IDbContextTransaction _transaction;
        private readonly ICurrentUser _currentUser;
        private bool _disposed = false;
        private Dictionary<Type, object> _repositories;
        private IEnumerable<PermissionRuleItem> _entityRules;

        public UnitOfWork(TCtx context, ILogger<UnitOfWork<TCtx>> log, ICurrentUser currentUser, IEnumerable<Type> instanceRepositories, IEnumerable<PermissionRuleItem> entityRules)
        {
            _context = context;
            _currentUser = currentUser;
            _log = log;
            _entityRules = entityRules;
            if (instanceRepositories != null || instanceRepositories.Count() > 0)
            {
                _repositories = instanceRepositories.Distinct().ToDictionary(e => e, e =>
                {
                    var entityRule = _entityRules?.FirstOrDefault(r => r.EntityType.Equals(e));
                    Type genericRepositoryType = typeof(Repository<>);
                    Type repositoryType = genericRepositoryType.MakeGenericType(e);
                    return Activator.CreateInstance(repositoryType, new object[] { _context, _currentUser, entityRule, EFTracking.NoTracking });
                });
            }
        }

        public DbContext GetContext() => _context;

        public IDisposable BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (_context.Database.GetDbConnection().State != ConnectionState.Open)
            {
                _context.Database.OpenConnection();
            }
            _transaction = _context.Database.BeginTransaction(isolationLevel);
            //_context.Database.UseTransaction(_transaction);
            return _transaction;
        }

        public void RollbackTransaction()
        {
            if (_transaction == null)
            {
                throw new Exception("Cannot roll back a transaction while there is no transaction running.");
            }

            _transaction.Rollback();
            ReleaseTransaction(_transaction);
        }

        private void ReleaseTransaction(IDbContextTransaction transaction)
        {
            transaction?.Dispose();
            _transaction = null;
        }

        public void CommitTransaction()
        {
            if (_transaction == null)
            {
                throw new Exception("Cannot commit a transaction while there is no transaction running.");
            }
            try
            {
                IEnumerable<object> itemChanges = GetItemsChanges();

                // Try validator exception here instead of catch DbEntityValidationException
                var validationResults = new List<ValidationResult>();
                foreach (var entity in itemChanges)
                {
                    if (!Validator.TryValidateObject(entity, new ValidationContext(entity), validationResults))
                    {
                        // Log validation error here
                        throw new ValidationException();
                    }
                }
                Save();
                _transaction.Commit();

                if (_currentUser != null)
                {
                    foreach (var item in itemChanges)
                    {
                        _currentUser.KeepHistory(item, true);
                    }
                    _currentUser.CommitHistory(_context);
                }
                ReleaseTransaction(_transaction);
            }
            catch (Exception ex)
            {
                _log.LogError("Transaction error.", ex);
                RollbackTransaction();
                throw;
            }
        }

        private IEnumerable<object> GetItemsChanges()
        {
            return this.GetItemsAdded().Union(this.GetItemsModified()).Union(this.GetItemsDeleted());
        }

        private IEnumerable<object> GetItemsDeleted()
        {
            foreach (var entry in _context.ChangeTracker.Entries().Where(item => item.State == EntityState.Deleted))
            {
                yield return entry.Entity as object;
            }
        }

        private IEnumerable<object> GetItemsModified()
        {
            foreach (var entry in _context.ChangeTracker.Entries().Where(item => item.State == EntityState.Modified))
            {
                yield return entry.Entity as object;
            }
        }

        private IEnumerable<object> GetItemsAdded()
        {
            foreach (var entry in _context.ChangeTracker.Entries().Where(item => item.State == EntityState.Added))
            {
                yield return entry.Entity as object;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // clear repositories
                    if (_repositories != null)
                    {
                        _repositories.Clear();
                    }
                    // dispose the db context.
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<Type, object>();
            }
            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type))
            {
                var entityRule = _entityRules?.FirstOrDefault(e => e.EntityType.Equals(type));
                _repositories[type] = new Repository<TEntity>(_context, _currentUser, entityRule);
            }
            return (IRepository<TEntity>)_repositories[type];
        }

        // AutoHistory get from https://github.com/Arch/AutoHistory.git
        public int SaveChanges(bool ensureAutoHistory = false)
        {
            if (ensureAutoHistory)
            {
                _context.EnsureAutoHistory();
            }

            return Save();
        }

        // AutoHistory get from https://github.com/Arch/AutoHistory.git
        public async Task<int> SaveChangesAsync(bool ensureAutoHistory = false)
        {
            if (ensureAutoHistory)
            {
                _context.EnsureAutoHistory();
            }
            return await SaveAsync();
        }

        private int Save() => _context.SaveChanges();

        private async Task<int> SaveAsync() => await _context.SaveChangesAsync();
    }
}