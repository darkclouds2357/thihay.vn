﻿namespace ST.Library.Base.Enum
{
    public enum EntityAction
    {
        View = 1,
        Insert = 2,
        Update = 4,
        Delete = 8
    }
}