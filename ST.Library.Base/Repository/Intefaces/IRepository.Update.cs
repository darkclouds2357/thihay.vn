﻿using ST.Library.Base.Entity;
using System.Collections.Generic;

namespace ST.Library.Base
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        void Update(TEntity entity);

        void Update(params TEntity[] entities);

        void Update(IEnumerable<TEntity> entities);
    }
}