﻿using ST.Library.Base.Entity;
using ST.Library.Common.Paging;
using ST.Library.Common.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ST.Library.Base
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity Find(Expression<Func<TEntity, bool>> criteria);

        TDto Find<TDto>(Expression<Func<TEntity, bool>> criteria);

        TEntity GetFirst();

        TEntity Find(object id);

        TEntity Find(params object[] ids);

        IQueryable<TEntity> GetAll(bool includeHidden = false);

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false);

        IQueryable<TDto> GetAll<TDto>(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false);

        IQueryable<TDto> GetAll<TDto>(bool includeHidden = false);

        IQueryable<TEntity> GetUnSecuredQuery(Expression<Func<TEntity, bool>> criteria = null, bool includeHidden = false);

        IQueryable<TDto> GetPaging<TDto>(List<SortExpression<TEntity>> sortExpressions, out int totalRecord, Expression<Func<TEntity, bool>> predicate = null, string[] includePaths = null, int? page = 0, int? pageSize = default(int?));

        IQueryable<TEntity> GetPaging(List<SortExpression<TEntity>> sortExpressions, out int totalRecord, Expression<Func<TEntity, bool>> predicate = null, string[] includePaths = null, int? page = 0, int? pageSize = default(int?));

        PagedListResult<TEntity> Search(SearchQuery<TEntity> searchQuery, bool includeHidden = false, bool isUnSecuredQuery = false);
    }
}