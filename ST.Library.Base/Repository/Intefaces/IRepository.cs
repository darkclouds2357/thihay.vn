﻿using ST.Library.Base.Entity;
using ST.Library.Base.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ST.Library.Base
{
    public partial interface IRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// By Default Query return with AsNoTracking();
        /// </summary>
        EFTracking Tracking { get; set; }

        Expression<Func<TEntity, bool>> SecuredCondition { get; }

        bool HasChange();

        IQueryable<TEntity> ExecWithFunction(string sqlQuery, params object[] parameters);

        int ExecWithStoreProcedure(string query, params object[] parameters);

        Task<int> ExecWithStoreProcedureAsync(string query, CancellationToken cancellationToken = default(CancellationToken), params object[] parameters);

        IList<TEntity> ExecWithStoreProcedureWithCommand(string query, params object[] parameters);
    }
}