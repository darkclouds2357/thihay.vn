﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ST.Library.Base.Entity;
using ST.Library.Base.Enum;
using ST.Library.Common;
using ST.Library.Common.Extensions;
using ST.Library.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace ST.Library.Base
{
    public partial class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        #region private variable

        private readonly Type ENTITY_TYPE = typeof(TEntity);
        private readonly string TABLE_NAME;

        private DbContext _context;
        private readonly DbSet<TEntity> _dbSet;
        private ICurrentUser _currentUser;
        private readonly TEntity _simpleInstance;
        private readonly string _connectionString;
        private readonly PermissionRuleItem _entityRule;
        private readonly bool _canSoftDelete;

        private IQueryable<TEntity> _set
        {
            get
            {
                IQueryable<TEntity> set;
                if (Tracking == EFTracking.NoTracking)
                {
                    set = _dbSet.AsNoTracking();
                }
                else
                {
                    set = _dbSet;
                }
                return set;
            }
        }

        private int ActionMask
        {
            get
            {
                return
                    (_entityRule.CanView ? (int)EntityAction.View : 0)
                    |
                    (_entityRule.CanInsert ? (int)EntityAction.Insert : 0) |
                    (_entityRule.CanUpdate ? (int)EntityAction.Update : 0) |
                    (_entityRule.CanDelete ? (int)EntityAction.Delete : 0);
            }
        }

        #endregion private variable

        #region Properties

        //private ILogger<Repository<TEntity>> _log;
        public virtual Expression<Func<TEntity, bool>> SecuredCondition { get; private set; }

        public EFTracking Tracking { get; set; }

        public IQueryable<TEntity> SecuredTable
        {
            get
            {
                if (_securedTable == null)
                {
                    _securedTable = SecuredCondition == null || _currentUser?.IsSystemUser == true ? _set : _set.Where(SecuredCondition);
                    /// Always return empty list when don't have view permission
                    /// BTW, may be I think i'm a bit crazy to check permission for view in base class,
                    /// but.... Ok, I still check it, because world still in peace
                    /// (or maybe still in war in some place) and I'm still poor
                    _securedTable = ValidateRules(nameof(EntityAction.View)) ? _securedTable : _set.Where(s => false);
                }
                return _securedTable;
            }
        }

        private IQueryable<TEntity> _securedTable;

        #endregion Properties

        public Repository(DbContext context, ICurrentUser currentUser, PermissionRuleItem entityRule, EFTracking tracking = EFTracking.NoTracking)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = _context.Set<TEntity>();
            _simpleInstance = Activator.CreateInstance<TEntity>();
            _currentUser = currentUser;

            /// If not Permission Rule implement then set default to all entity has all permission
            _entityRule = entityRule ?? new PermissionRuleItem(entityType: ENTITY_TYPE, canView: true, canInsert: true, canDelete: true, canUpdate: true);

            if (ENTITY_TYPE.GetInterfaces()?.Any(i => i == typeof(ISecuredCondition<TEntity>)) == true)
            {
                SecuredCondition = (_simpleInstance as ISecuredCondition<TEntity>).SecuredCondition;
            }

            //_log = log;
            _canSoftDelete = _simpleInstance.EntityHasIsDeleted;//IsSoftDelete();
            Tracking = tracking;
            try
            {
                _connectionString = _context.Database.GetDbConnection().ConnectionString;
            }
            catch
            {
                _connectionString = null;
            }
            TABLE_NAME = ENTITY_TYPE.Name;//_context.Model.FindEntityType(ENTITY_TYPE)?.Name;
        }

        #region Internal Methods

        private bool ValidateRules([CallerMemberName]string calledAction = "")
        {
            /// hardcode check for calledMethod is DeleteRange and change it to Delete
            if (calledAction == nameof(HardDelete))
            {
                calledAction = nameof(Delete);
            }
            /// same with insert
            if (calledAction == nameof(InsertAsync) ||
                calledAction == nameof(BulkInsert) ||
                calledAction == nameof(BulkInsertAsync))
            {
                calledAction = nameof(Insert);
            }
            if (System.Enum.TryParse(calledAction, out EntityAction requestAction))
            {
                return Convert.ToBoolean((ActionMask) & (int)requestAction);
            }
            else
                return false;
        }

        public TEntity SetInsertEntity(TEntity entity)
        {
            entity.CreatedOn = DateTime.Now;
            entity.CreatedBy = _currentUser?.Id ?? default(Guid);
            entity.IsDeleted = false;
            return entity;
        }

        public TEntity SetUpdateEntity(TEntity entity)
        {
            entity.UpdatedBy = _currentUser?.Id ?? default(Guid);
            entity.UpdatedOn = DateTime.Now;
            return entity;
        }

        public TEntity SetSoftDeleteEntity(TEntity entity)
        {
            entity = SetUpdateEntity(entity);
            entity.IsDeleted = true;
            return entity;
        }

        private IQueryable<TEntity> ManageFilters(SearchQuery<TEntity> searchQuery, IQueryable<TEntity> sequence)
        {
            if (searchQuery.Filters != null && searchQuery.Filters.Count > 0)
            {
                foreach (var filterClause in searchQuery.Filters)
                {
                    sequence = sequence.AsExpandable().Where(filterClause);
                }
            }
            return sequence;
        }

        private IQueryable<TEntity> ManageIncludeProperties(SearchQuery<TEntity> searchQuery, IQueryable<TEntity> sequence)
        {
            if (!string.IsNullOrWhiteSpace(searchQuery.IncludeProperties))
            {
                var properties = searchQuery.IncludeProperties.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var includeProperty in properties)
                {
                    sequence = sequence.AsExpandable().Include(includeProperty);
                }
            }
            return sequence;
        }

        private IQueryable<TEntity> ManageSortCriterias(SearchQuery<TEntity> searchQuery, IQueryable<TEntity> sequence)
        {
            if (searchQuery.SortCriterias != null && searchQuery.SortCriterias.Count > 0)
            {
                var sortCriteria = searchQuery.SortCriterias[0];
                var orderedSequence = sortCriteria.ApplyOrdering(sequence, false);

                if (searchQuery.SortCriterias.Count > 1)
                {
                    for (var i = 1; i < searchQuery.SortCriterias.Count; i++)
                    {
                        var sc = searchQuery.SortCriterias[i];
                        orderedSequence = sc.ApplyOrdering(orderedSequence, true);
                    }
                }
                sequence = orderedSequence;
            }
            else
            {
                sequence = ((IOrderedQueryable<TEntity>)sequence).OrderBy(x => (true));
            }
            return sequence;
        }

        private PagedListResult<TEntity> GetTheResult(SearchQuery<TEntity> searchQuery, IQueryable<TEntity> sequence)
        {
            //Counting the total number of object.
            var resultCount = sequence.AsExpandable().Count();

            var result =
                sequence.AsExpandable()
                    .Skip((searchQuery.Skip - 1) * searchQuery.Take)
                    .Take(searchQuery.Take);

            return new PagedListResult<TEntity>
            {
                Entities = result.ToList(),
                Count = resultCount
            };
        }

        #endregion Internal Methods

        public virtual bool HasChange() => _context.ChangeTracker.Entries<TEntity>()
                    .Where(entity => entity.State == EntityState.Added || entity.State == EntityState.Modified || entity.State == EntityState.Deleted)
                    .Select(entry => entry.Entity)
                    .Any();

        public virtual IQueryable<TEntity> ExecWithFunction(string sqlQuery, params object[] parameters) => _dbSet.FromSql(sqlQuery, parameters);

        public virtual int ExecWithStoreProcedure(string query, params object[] parameters) => _context.Database.ExecuteSqlCommand(query, parameters);

        public virtual Task<int> ExecWithStoreProcedureAsync(string query, CancellationToken cancellationToken = default(CancellationToken), params object[] parameters) => _context.Database.ExecuteSqlCommandAsync(query, cancellationToken, parameters);

        public virtual IList<TEntity> ExecWithStoreProcedureWithCommand(string query, params object[] parameters) => _dbSet.FromSql(query, parameters).ToList();

        #region unused

        public Action<TEntity> OnChange { get; set; }

        public virtual void Attach(TEntity entity, EntityState state = EntityState.Unchanged)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<string> GetEntityColumnsName()
        {
            var modelEntityType = _context.Model.FindEntityType(ENTITY_TYPE);
            /// a dirty hack here, if property contain foreignKey
            /// then get the property has dependent entity
            /// eg Class City()
            /// {
            ///     ...
            ///     public vitural Country Country { get; set; }
            ///     public long CountryId { get; set; }
            ///     ...
            /// }
            /// Then in properties we have 2 foreign key of CountryId
            /// 1st it has DependentToPrincipal from Country.Id = City.CountryId and PrincipalToDependent is null
            /// 2nd It has DependentToPrincipal is null and PrincipalToDependent has Country.ICollection[City] point to List[City]
            return modelEntityType.GetProperties().Where(e => e.ValueGenerated == ValueGenerated.Never).Where(e => e.GetContainingForeignKeys().Any(f => f.DependentToPrincipal != null && f.PrincipalToDependent == null) || e.GetContainingForeignKeys().Count() == 0).Select(e => e.Relational().ColumnName); // found in internet if SQLServer we can select(e=> e.SqlServer().ColumnName,
        }

        public virtual void BulkInsert(IEnumerable<TEntity> items, bool keepIdentity = false, string identityFieldName = "Id")
        {
            throw new NotImplementedException();
        }

        public virtual void BulkInsert(DataTable table, string tableName)
        {
            throw new NotImplementedException();
        }

        public virtual void BulkUpdateOneField(IEnumerable<TEntity> entities, string fieldName, string fieldValue)
        {
            throw new NotImplementedException();
        }

        #endregion unused
    }
}