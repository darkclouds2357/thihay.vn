﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using ST.Library.Base.Entity;
using ST.Library.Common.Extensions;
using ST.Library.Common.Paging;
using ST.Library.Common.Search;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ST.Library.Base
{
    public partial class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        #region FindOne

        public virtual TEntity Find(Expression<Func<TEntity, bool>> criteria)
        {
            return SecuredTable.Where(criteria).AsExpandable().FirstOrDefault();
        }

        public virtual TDto Find<TDto>(Expression<Func<TEntity, bool>> criteria)
        {
            var obj = SecuredTable.Where(criteria).Select<TEntity, TDto>().FirstOrDefault();
            return obj;
        }

        public virtual TEntity GetFirst()
        {
            return SecuredTable.FirstOrDefault();
        }

        public virtual TEntity Find(object id)
        {
            var typeInfo = typeof(TEntity).GetTypeInfo();
            var key = _context.Model.FindEntityType(typeof(TEntity)).FindPrimaryKey().Properties.FirstOrDefault();
            var property = typeInfo.GetProperty(key?.Name);
            if (property != null)
            {
                // input parameter "entity"
                var xParameter = Expression.Parameter(ENTITY_TYPE, "entity");
                // key property "entity.Id"
                var xKey = Expression.Property(xParameter, property);
                // const id
                var xConstId = Expression.Constant(id);
                // equal expression entity.Id == id
                var xEqual = Expression.Equal(xKey, xConstId);
                // lamda entity => entity.Id == id
                var xLamda = Expression.Lambda<Func<TEntity, bool>>(xEqual, xParameter);
                return Find(xLamda);
            }
            else
            {
                return _dbSet.Find(id);
            }
        }

        public virtual TEntity Find(params object[] ids)
        {
            var typeInfo = typeof(TEntity).GetTypeInfo();
            var keys = _context.Model.FindEntityType(typeInfo.Name).FindPrimaryKey().Properties;
            if (keys.Count > 0)
            {
                Expression<Func<TEntity, bool>> criteria = null;
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    var property = typeInfo.GetProperty(key?.Name);
                    if (property != null)
                    {
                        // input parameter "entity"
                        var xParameter = Expression.Parameter(ENTITY_TYPE, "entity");
                        // key property "entity.Key[i]"
                        var xKey = Expression.Property(xParameter, property);
                        // const "id[i]"
                        var xConstId = Expression.Constant(ids[i]);
                        // equal expression "entity.Key[i] == id[i]"
                        var xEqual = Expression.Equal(xKey, xConstId);
                        // lamda "entity => entity.Key[i] == id[i]"
                        var xLamda = Expression.Lambda<Func<TEntity, bool>>(xEqual, xParameter);

                        // AND lamda "entity => entity.Key[i] == id[i] && entity.Key[j] == id[j]"
                        if (criteria != null)
                        {
                            criteria = criteria.And(xLamda);
                        }
                        else
                        {
                            criteria = xLamda;
                        }
                    }
                }
                if (criteria != null)
                {
                    return Find(criteria);
                }
            }
            return _dbSet.Find(ids);
        }

        #endregion FindOne

        #region GetAll

        public virtual IQueryable<TEntity> GetAll(bool includeHidden = false)
        {
            var query = !includeHidden && _simpleInstance.EntityHasIsDeleted ? SecuredTable.Where(x => !x.IsDeleted) : SecuredTable;
            return query;
        }

        public virtual IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false)
        {
            return GetAll(includeHidden).Where(criteria);
        }

        public virtual IQueryable<TDto> GetAll<TDto>(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false)
        {
            var query = GetAll(includeHidden).Where(criteria).Select<TEntity, TDto>();
            return query;
        }

        public virtual IQueryable<TDto> GetAll<TDto>(bool includeHidden = false)
        {
            var query = GetAll(includeHidden).Select<TEntity, TDto>();
            return query;
        }

        public virtual IQueryable<TEntity> GetUnSecuredQuery(Expression<Func<TEntity, bool>> criteria = null, bool includeHidden = false)
        {
            var query = criteria == null ? _set : _set.Where(criteria);
            return !includeHidden && _simpleInstance.EntityHasIsDeleted ? query.Where(x => x.IsDeleted == false) : query;
        }

        #endregion GetAll

        #region GetPaging

        public virtual IQueryable<TEntity> GetPaging(List<SortExpression<TEntity>> sortExpressions, out int totalRecord, Expression<Func<TEntity, bool>> predicate = null, string[] includePaths = null, int? page = 0, int? pageSize = default(int?))
        {
            IQueryable<TEntity> query = GetAll();

            if (predicate != null)
            {
                query = query.AsExpandable().Where(predicate);
            }
            totalRecord = query.AsExpandable().Count();

            if (includePaths != null)
            {
                for (var i = 0; i < includePaths.Count(); i++)
                {
                    query = query.Include(includePaths[i]);
                }
            }

            if (sortExpressions != null)
            {
                IOrderedQueryable<TEntity> orderedQuery = null;

                for (var i = 0; i < sortExpressions.Count(); i++)
                {
                    if (i == 0)
                    {
                        orderedQuery = sortExpressions[i].SortDirection == ListSortDirection.Ascending ? query.OrderBy(sortExpressions[i].SortBy) : query.OrderByDescending(sortExpressions[i].SortBy);
                    }
                    else
                    {
                        if (orderedQuery != null)
                            orderedQuery = sortExpressions[i].SortDirection == ListSortDirection.Ascending ? orderedQuery.ThenBy(sortExpressions[i].SortBy) : orderedQuery.ThenByDescending(sortExpressions[i].SortBy);
                    }
                }

                if (page != null)
                {
                    if (pageSize != null)
                        query = orderedQuery.Skip(((int)page - 1) * (int)pageSize);
                }
            }

            if (pageSize != null)
            {
                query = query.Take((int)pageSize);
            }
            return query;
        }

        public virtual IQueryable<TDto> GetPaging<TDto>(List<SortExpression<TEntity>> sortExpressions, out int totalRecord, Expression<Func<TEntity, bool>> predicate = null, string[] includePaths = null, int? page = 0, int? pageSize = default(int?))
        {
            return GetPaging(sortExpressions, out totalRecord, predicate, includePaths, page, pageSize).Select<TEntity, TDto>();
        }

        public virtual PagedListResult<TEntity> Search(SearchQuery<TEntity> searchQuery, bool includeHidden = false, bool isUnSecuredQuery = false)
        {
            IQueryable<TEntity> sequence = GetAll(includeHidden);

            //Applying filters
            sequence = ManageFilters(searchQuery, sequence);

            //Include Properties
            sequence = ManageIncludeProperties(searchQuery, sequence);

            //Resolving Sort Criteria
            //This code applies the sorting criterias sent as the parameter
            sequence = ManageSortCriterias(searchQuery, sequence);

            return GetTheResult(searchQuery, sequence);
        }

        #endregion GetPaging
    }
}