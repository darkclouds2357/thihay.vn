﻿using FastMember;
using ST.Library.Base.Constant;
using ST.Library.Base.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ST.Library.Base
{
    public partial class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private const int DEFAULT_BATCHSIZE = 5000;

        private const int DEFAULT_MAX_ENTITY_ATTACHED = 2000; // If commit entites more than 2000 record then use bulkCopy

        public virtual void BulkInsert(IEnumerable<TEntity> items)
        {
            if (ValidateRules())
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(_connectionString))
                    {
                        throw new Exception("No connection string");
                    }
                    string[] entityProperties = GetEntityColumnsName().ToArray();
                    using (var sqlCopy = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.TableLock))
                    {
                        sqlCopy.BatchSize = DEFAULT_BATCHSIZE;
                        sqlCopy.DestinationTableName = TABLE_NAME;
                        foreach (var item in entityProperties)
                        {
                            sqlCopy.ColumnMappings.Add(item, item);
                        }
                        using (var objectReader = ObjectReader.Create(items, entityProperties))
                        {
                            sqlCopy.WriteToServer(objectReader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public virtual async Task BulkInsertAsync(IEnumerable<TEntity> entities)
        {
            if (ValidateRules())
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(_connectionString))
                    {
                        throw new Exception("No connection string");
                    }
                    string[] entityProperties = GetEntityColumnsName().ToArray();
                    using (var sqlCopy = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.TableLock))
                    {
                        sqlCopy.BatchSize = DEFAULT_BATCHSIZE;
                        sqlCopy.DestinationTableName = TABLE_NAME;
                        foreach (var item in entityProperties)
                        {
                            sqlCopy.ColumnMappings.Add(item, item);
                        }
                        using (var objectReader = ObjectReader.Create(entities, entityProperties))
                        {
                            await sqlCopy.WriteToServerAsync(objectReader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public virtual void Insert(TEntity entity)
        {
            if (ValidateRules())
                _dbSet.Add(SetInsertEntity(entity));
        }

        public virtual void Insert(IEnumerable<TEntity> entities)
        {
            if (ValidateRules())
            {
                var entityCnt = entities.Count();
                if (entityCnt < ConstantValue.COMMIT_COUNT_TO_RECREATED_CONTEXT)
                {
                    _dbSet.AddRange(entities.Select(entity => SetInsertEntity(entity)));
                }
                //else if (entityCnt < DEFAULT_MAX_ENTITY_ATTACHED)
                //{
                //    int cnt = 0;
                //    var insertEntities = entities.Take(COMMIT_COUNT_TO_RECREATED_CONTEXT);

                //    while (entityCnt > COMMIT_COUNT_TO_RECREATED_CONTEXT * cnt)
                //    {
                //        cnt++;
                //        _dbSet.AddRange(insertEntities);
                //        _context.SaveChanges();

                //        _context.Dispose();
                //        _context = Activator.CreateInstance(_context.GetType()) as DbContext;
                //        _context.ChangeTracker.AutoDetectChangesEnabled = false;

                //        insertEntities = entities.Take(COMMIT_COUNT_TO_RECREATED_CONTEXT).Skip(COMMIT_COUNT_TO_RECREATED_CONTEXT);
                //    }
                //    _context.Dispose();
                //    _context = Activator.CreateInstance(_context.GetType()) as DbContext;
                //}
                else
                {
                    BulkInsert(entities.Select(entity => SetInsertEntity(entity)));
                }
            }
        }

        public virtual void Insert(params TEntity[] entities)
        {
            if (ValidateRules())
                _dbSet.AddRange(entities.Select(entity => SetInsertEntity(entity)).ToArray());
        }

        /// <summary>
        /// Inserts a new entity asynchronously.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>A <see cref="Task"/> that represents the asynchronous insert operation.</returns>
        public virtual async Task InsertAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (ValidateRules())
                await _dbSet.AddAsync(SetInsertEntity(entity), cancellationToken);
        }

        /// <summary>
        /// Inserts a range of entities asynchronously.
        /// </summary>
        /// <param name="entities">The entities to insert.</param>
        /// <returns>A <see cref="Task" /> that represents the asynchronous insert operation.</returns>
        public virtual async Task InsertAsync(params TEntity[] entities)
        {
            if (ValidateRules()) await _dbSet.AddRangeAsync(entities.Select(entity => SetInsertEntity(entity)).ToArray());
        }

        public virtual async Task InsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (ValidateRules())
            {
                var entityCnt = entities.Count();
                if (entityCnt < ConstantValue.COMMIT_COUNT_TO_RECREATED_CONTEXT)
                {
                    await _dbSet.AddRangeAsync(entities.Select(entity => SetInsertEntity(entity)), cancellationToken);
                }
                //else
                //{
                //    return _dbSet.AddRangeAsync(entities, cancellationToken);
                //}
                else
                {
                    await BulkInsertAsync(entities.Select(entity => SetInsertEntity(entity)));
                }
            }
        }
    }
}