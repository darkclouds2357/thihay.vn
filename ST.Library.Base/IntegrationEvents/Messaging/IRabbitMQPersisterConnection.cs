﻿using RabbitMQ.Client;
using System;

namespace ST.Library.Base.Messaging
{
    public interface IRabbitMQPersisterConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}