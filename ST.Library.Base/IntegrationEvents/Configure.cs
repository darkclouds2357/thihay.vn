﻿using EventBus.Abstractions;
using EventBus.Event;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace ST.Library.Base.IntegrationEvents
{
    public static class Configure
    {
        public static void ConfigureEventBus<T, TH>(IApplicationBuilder app)
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventHandler = app.ApplicationServices.GetService<IIntegrationEventHandler<T>>();
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<T, TH>(() => app.ApplicationServices.GetRequiredService<TH>());
        }
    }
}