﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ST.Library.Base.Entity
{
    public interface IIncludeProperty<TEntity> where TEntity: BaseEntity
    {
        Expression<Func<TEntity, TProperty>> Include { get; }
    }
}
