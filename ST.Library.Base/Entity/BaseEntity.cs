﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace ST.Library.Base.Entity
{
    public abstract partial class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
        }

        [Key]
        public virtual Guid Id { get; set; }

        public virtual DateTime? CreatedOn { get; set; }
        public virtual Guid? CreatedBy { get; set; }
        public virtual DateTime? UpdatedOn { get; set; }
        public virtual Guid? UpdatedBy { get; set; }
        public virtual bool IsDeleted { get; set; }

        // check IsDeleted Column is NotMapped to database then return false.
        private bool? _entityHasIsDeleted;

        public virtual bool EntityHasIsDeleted
        {
            get
            {
                if (!_entityHasIsDeleted.HasValue)
                {
                    Type entityType = this.GetType();
                    var isDeleteProp = entityType.GetProperty(nameof(IsDeleted));
                    if (isDeleteProp != null && isDeleteProp.GetCustomAttribute<NotMappedAttribute>() == null)
                    {
                        _entityHasIsDeleted = true;
                    }
                    else
                    {
                        _entityHasIsDeleted = false;
                    }
                }
                return _entityHasIsDeleted ?? false;
            }
        }

        //public static readonly Dictionary<Type, string[]> SanitizeIgnoreProperties = new Dictionary<Type, string[]> { };
        //public static void Sanitize(object entity)
        //{
        //    var type = entity.GetType();
        //    if (type.GetTypeInfo().BaseType != null && type.Namespace == "System.Data.Entity.DynamicProxies")
        //        type = type.GetTypeInfo().BaseType;
        //    bool ignore = SanitizeIgnoreProperties.ContainsKey(type);
        //    foreach (var p in type.GetProperties().Where(x => x.PropertyType == typeof(string) && x.SetMethod != null))
        //    {
        //        string value = p.GetValue(entity) as string;
        //        if (string.IsNullOrWhiteSpace(value) == false)
        //        {
        //            if (ignore && SanitizeIgnoreProperties[type].Contains(p.Name))
        //            {
        //                Common.Helper.HtmlHelper.CleanScriptTags(value, false);
        //            }
        //            /*else
        //            {
        //                ////value = HttpUtility.HtmlEncode(value); // Just encode HTML in output.
        //                //value = Common.Helper.HtmlHelper.Clean(value, false);
        //                ///Common.Helper.HtmlHelper.CleanScriptTags(value, false);
        //            }*/
        //            p.SetValue(entity, value.Trim());
        //        }
        //    }
        //}

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Clean removes any HTML Tags, Entities (and optionally any punctuation) from
        /// a string
        /// </summary>
        /// <remarks>
        /// Encoded Tags are getting decoded, as they are part of the content!
        /// </remarks>
        /// <param name="HTML">The Html to clean</param>
        /// <param name="RemovePunctuation">A flag indicating whether to remove punctuation</param>
        /// <returns>The cleaned up string</returns>
        /// -----------------------------------------------------------------------------
        /*public static string Clean(string HTML, bool RemovePunctuation)
        {
            if (string.IsNullOrWhiteSpace(HTML))
            {
                return string.Empty;
            }

            if (HTML.Contains("&lt;"))
            {
                // Fix when it is a double-encoded document
                HTML = HttpUtility.HtmlDecode(HTML);
            }

            //First remove any HTML Tags ("<....>")
            HTML = StripTags(HTML, true);

            //Second replace any HTML entities (&nbsp; &lt; etc) through their char symbol
            HTML = HttpUtility.HtmlDecode(HTML);

            //Thirdly remove any punctuation
            if (RemovePunctuation)
            {
                HTML = StripPunctuation(HTML, true);
                // When RemovePunctuation is false, HtmlDecode() would have already had removed these
                //Finally remove extra whitespace
                HTML = StripWhiteSpace(HTML, true);
            }

            return HTML;
        }
        */
    }

    public class PermissionRuleItem
    {
        public PermissionRuleItem(Type entityType,
            bool canView = true,
            bool canInsert = false,
            bool canDelete = false,
            bool canUpdate = false)
        {
            EntityType = entityType;
            CanView = canView;
            CanInsert = canInsert;
            CanDelete = canDelete;
            CanUpdate = canUpdate;
        }

        public Type EntityType { get; }
        public bool CanView { get; }
        public bool CanInsert { get; }
        public bool CanUpdate { get; }
        public bool CanDelete { get; }
    }
}