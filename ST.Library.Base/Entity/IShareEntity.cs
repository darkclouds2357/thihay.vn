﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ST.Library.Base.Entity
{
    /// <summary>
    /// Entity that be the main of service and share data to other service
    /// </summary>
    public interface IShareEntity
    {
        Guid CorrelationId { get; }
    }
}
