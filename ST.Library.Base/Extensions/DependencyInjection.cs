﻿using EventBus;
using EventBus.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using ST.Library.Base.Entity;
using ST.Library.Base.IntegrationEvents.Messaging;
using ST.Library.Base.Messaging;
using ST.Library.Base.UnitOfWork;
using ST.Library.Common;
using ST.Library.Common.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DIExtensions
    {
        public static void SetCacheOption(this IConfigurationRoot Configuration)
        {
            string cacheTimeConfig = Configuration[$"{CacheOption.CACHE_KEY_CONFIG}:{CacheOption.CACHE_TIME_CONFIG}"];
            string cacheTypeConfig = Configuration[$"{CacheOption.CACHE_KEY_CONFIG}:{CacheOption.CACHE_DATA_TYPE}"];

            if (!string.IsNullOrWhiteSpace(cacheTimeConfig) && int.TryParse(cacheTimeConfig, out int cacheTime))
            {
                CacheOption.Instance.CacheTime = cacheTime;
            }
            //if (!string.IsNullOrWhiteSpace(cacheTypeConfig) && int.TryParse(cacheTypeConfig, out int cacheType) && Enum.IsDefined(typeof(CacheDataType), cacheType))
            //{
            //    CacheOption.Instance.Type = (CacheDataType)cacheType;
            //}
        }

        public static IServiceCollection AddCache(this IServiceCollection services, RedisOption redisOption)
        {
            if (redisOption?.IsExist == true)
            {
                services.AddDistributedRedisCache(op =>
                {
                    op.Configuration = redisOption.Configuration;
                    op.InstanceName = redisOption.InstanceName;
                });
            }
            else
            {
                services.AddDistributedMemoryCache();
            }
            return services;
        }

        public static IServiceCollection AddUnitOfWork<TContext>(this IServiceCollection services, IEnumerable<Type> repositoryTypes = null) where TContext : DbContext
        {
            if (repositoryTypes != null && repositoryTypes.Count() > 0)
            {
                services.AddScoped(sp => repositoryTypes);
            }

            //services.AddScoped<IRepositoryFactory, UnitOfWork<TContext>>();
            services.AddScoped<IUnitOfWork, UnitOfWork<TContext>>(sp =>
            {
                var dbContext = sp.GetRequiredService<TContext>();
                var currentUser = sp.GetRequiredService<ICurrentUser>();
                var log = sp.GetRequiredService<ILogger<UnitOfWork<TContext>>>();
                var instanceType = sp.GetRequiredService<IEnumerable<Type>>();
                var permissionEntities = sp.GetRequiredService<IEnumerable<PermissionRuleItem>>();
                return new UnitOfWork<TContext>(dbContext, log, currentUser, instanceType, permissionEntities);
            });
            return services;
        }

        public static IServiceCollection RegisterServiceBus(this IServiceCollection services)
        {
            services.AddSingleton<IRabbitMQPersisterConnection>(sp =>
            {
                var settings = sp.GetRequiredService<IOptions<RabbitSetting>>().Value;
                var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();
                var factory = new ConnectionFactory()
                {
                    Uri = new Uri(settings.Uri)
                };
                return new DefaultRabbitMQPersistentConnection(factory, logger);
            });

            services.AddSingleton<IEventBus, EventBusRabbitMQ>();
            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            return services;
        }

        /// TODO instance for EntityChangedIntegrationEventHandler
        // instance: Dictionary<entityName:string, (Type entityType, Dictionary<source:string, dest:string> columns)>
    }
}