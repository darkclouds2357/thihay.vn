﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptedPassword
{
    class Program
    {
        static void Main(string[] args)
        {
            string password = "Pa55w0rd";
            string password1 = "Pa55w0rd1";
            // We should generate salt has size larger 32
            string salt = HashEncrypted.GenerateSalt(32);

            string encrypted = HashEncrypted.EncryptedHash(password, salt);

            Console.WriteLine(HashEncrypted.EncryptedHash(password, salt));
            Console.WriteLine(HashEncrypted.EncryptedHash(password1, salt));
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine(HashEncrypted.IsVerifyStringMatch(password, encrypted, salt));
            Console.ReadLine();

        }
    }
}
