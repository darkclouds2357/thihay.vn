﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptedPassword
{
    public static class HashEncrypted
    {
        private static readonly UTF8Encoding Encoding = new UTF8Encoding();

        public static string EncryptedHash(string password, string salt)
        {
            byte[] bSalt = Encoding.GetBytes(salt);
            return HashSHA(password, bSalt);
        }
        
        public static bool IsVerifyStringMatch(string original, string hashString, string salt)
        {
            byte[] byteSalts = Encoding.GetBytes(salt);
            string hashedOriginal = HashSHA(original, byteSalts);
            return string.Equals(hashedOriginal, hashString);
        }
        
        private static string HashSHA(string message, byte[] salt)
        {   
            byte[] messageBytes = Encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(salt))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        public static string GenerateSalt(int size)
        {
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] buff = new byte[size];
                rng.GetNonZeroBytes(buff);
                return Convert.ToBase64String(buff);
            }   
        }

        public static byte[] GenerateSaltBytes(int size)
        {
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] buff = new byte[size];
                rng.GetNonZeroBytes(buff);
                return buff;
            }
        }

    }
}
