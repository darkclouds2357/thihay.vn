﻿var UserInfo = {
    pk_user_id: 0,
    fk_school_id: 0,
    fk_village_id: 0,
    fk_province_id: 0,
    s_school_name: '',
    s_village_name: '',
    s_province_name: '',
    s_username: '',
    s_fullname: '',
    s_first_name: '',
    s_last_name: '',
    d_bith: 'yyyy/mm/dd',
    s_phone: '',
    s_email: ''
};
function Register() {
    var firstname = $("input[name='firstname']").val();
    var lastname = $("input[name='lastname']").val();
    var username = $(".control").find("input[name='username']").val();
    var password = $(".control").find("input[name='password']").val();
    var confirm_password = $(".control").find("input[name='confirm_password']").val();
    var email = $(".control").find("input[name='email']").val();
    var phone = $(".control").find("input[name='phone']").val();
    var patt = /hack/gi;
    if (Trim(firstname, ' ') == "" || Trim(firstname, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Họ,tên đệm");
        return;
    }
    if (ValidName(Trim(firstname, ' ')) == false) {
        AlertNoRedirect("Họ và tên đệm chỉ có thể là các ký tự a-z, A-Z và khoảng trắng");
        return;
    }
    if (Trim(lastname, ' ') == "" || Trim(lastname, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Tên");
        return;
    }
    if (ValidName(Trim(lastname, ' ')) == false) {
        AlertNoRedirect("Tên chỉ có thể là các ký tự a-z, A-Z và khoảng trắng.");
        return;
    }
    if (Trim(username, ' ') == "" || Trim(username, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Tên tài khoản");
        return;
    }
    if (ValidUserName(username) == false) {
        AlertNoRedirect("Tên đăng nhập chỉ có thể là các ký tự a-z, các chữ số 0-9 và dấu _");
        return;
    }
    if (username.length < 7) {
        AlertNoRedirect("Tên đăng nhập phải lớp hơn 6 ký tự");
        return;
    }
    if (Trim(password, ' ') == "" || Trim(password, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Mật khẩu");
        return;
    }
    if (password.length < 7) {
        AlertNoRedirect("Mật khẩu phải lớp hơn 6 ký tự");
        return;
    }
    if (Trim(confirm_password, ' ') == "" || Trim(confirm_password, ' ') == null) {
        AlertNoRedirect("Vui lòng Gõ lại mật khẩu");
        return;
    }
    if (confirm_password != password) {
        AlertNoRedirect("Mật khẩu gõ lại không khớp.");
        return;
    }
    if (Trim(email, ' ') == "" || Trim(email, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Email");
        return;
    }
    if (Trim(phone, ' ') == "" || Trim(phone, ' ') == null) {
        AlertNoRedirect("Vui lòng nhập Phone");
        return;
    }
    if (Trim(email, ' ').length > 0) {
        if (ValidEmail(email) == false) {
            AlertNoRedirect("Địa chỉ Email không hợp lệ");
            return;
        }
    }
    if (localStorage['captcha'] != 1) {
        AlertNoRedirect("Vui lòng xác thực reCAPTCHA");
        return;
    }
    var Userinfo = { "firstname": firstname, "lastname": lastname, "username": username, "password": password, "email": email, "phone": phone };
    $.ajax({
        type: "POST",
        url: "/Register.aspx",
        data: '{"type":"register","Userinfo":' + JSON.stringify(Userinfo) + '}',
        dataType: "text",
        //beforeSend: function () {
        //    $('#popup').addClass('overlay');
        //},
        success: function (text) {
            if (text == 0)
            {
                AlertNoRedirect("Tài khoản đã tồn tại");
                return;
            }
            if(text==-1)
            {
                AlertNoRedirect("Email đã tồn tại");
                return;
            }
            if (text == -2) {
                AlertNoRedirect("Đăng ký thất bại");
                return;
            }
            AlertNoRedirect("Đăng ký thành công. Hãy vui lòng đăng nhập để trải nghiệm.");
            window.location.href = "/Default.aspx";
            return;
        }
    });
}
function ValidName(strString) {
    var strValidChars = "abcdefghijklmnopqrstuvwxyzđàáảãạăằắẳặâầấẩậẫìíỉĩịèéẻẽẹêềếểễệùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợỳýỷỹỵ";
    var strChar;
    var blnResult = true;
    strString = strString.toLowerCase();
    if (strString.length == 0) return false;
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strChar != " ")
            if (strValidChars.indexOf(strChar) == -1) {
                blnResult = false;
            }
    }
    return blnResult;
}
function LTrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
function RTrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
function Trim(str, chars) {
    return LTrim(RTrim(str, chars), chars);
}
function AlertNoRedirect(text) {
    $.jAlert({
        'title': 'THÔNG BÁO',
        'content': '<p class ="front-alert"><i class="fa fa-exclamation-triangle fa-4x"></i>'+text+'</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
        'btns': { 'text': 'OK' }
    });
    //$.fn.jAlert.defaults.showAnimation = 'flash';
    ////$.fn.jAlert.defaults.hideAnimation = 'rollOut';
    return false;
}
function AlertRedirect(text, url) {
    $.jAlert({
        'title': 'THÔNG BÁO',
        'content': '<p class ="front-alert"><i class="fa fa-exclamation-triangle fa-4x"></i>' + text + '</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
        'btns': {
            'text': 'OK',
            'onClick': function (e, btn) {
                window.location.href = url;
            }
        }
    });
    //$.fn.jAlert.defaults.showAnimation = 'flash';
    ////$.fn.jAlert.defaults.hideAnimation = 'rollOut';
    return false;
}
function ValidName(strString) {
    var strValidChars = "abcdefghijklmnopqrstuvwxyzđàáảãạăằắẳặâầấẩậẫìíỉĩịèéẻẽẹêềếểễệùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợỳýỷỹỵ";
    var strChar;
    var blnResult = true;
    strString = strString.toLowerCase();
    if (strString.length == 0) return false;
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strChar != " ")
            if (strValidChars.indexOf(strChar) == -1) {
                blnResult = false;
            }
    }
    return blnResult;
}
function ValidUserName(strString) {
    var strValidChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
    var strChar;
    var blnResult = true;
    if (strString.length == 0) return false;
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1) {
            blnResult = false;
        }
    }
    return blnResult;
}
function ValidPassword(strString) {
    var strValidChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%^&*()";
    var strChar;
    var blnResult = true;
    if (strString.length == 0) return false;
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1) {
            blnResult = false;
        }
    }
    return blnResult;
}
function ValidEmail(str) {
    var at = "@";
    var dot = ".";
    var lat = str.indexOf(at);
    var lstr = str.length;
    var ldot = str.indexOf(dot);
    if (str.indexOf(at) == -1) {
        return false;
    }
    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        return false;
    }
    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        return false;
    }
    if (str.indexOf(at, (lat + 1)) != -1) {
        return false;
    }
    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        return false;
    }
    if (str.indexOf(dot, (lat + 2)) == -1) {
        return false;
    }
    if (str.indexOf(" ") != -1) {
        return false;
    }
    return true;
}
function Login() {
    var str1 = /select/gi;
    var str2 = /insert/gi;
    var str3 = /update/gi;
    var str4 = /delete/gi;
    var str5 = /drop/gi;
    var str6 = /union/gi;
    var str7 = /information_schemal/gi;
    var username = $('.line').find("[name='username']").val();
    var password = $('.line').find("[name='password']").val();
    if (username == "" || password == "") {
        AlertNoRedirect("Bạn phải điền đầy đủ tên truy cập và mật khẩu");
        return;
    }
    else if (username.match(str1) || username.match(str2) || username.match(str3) || username.match(str4) || username.match(str5) || username.match(str6) || username.match(str7)) {
        AlertNoRedirect("Không đăng nhập tên đăng nhập với những từ union,select,insert,update,delete,drop,information_schemal");
        return;
    }
    else if (password.match(str1) || password.match(str2) || password.match(str3) || password.match(str4) || password.match(str5) || password.match(str6) || password.match(str7)) {
        AlertNoRedirect("Không đăng nhập mật khẩu với những từ union,select,insert,update,delete,drop,information_schemal");
        return;
    }
    else
        var Userinfo = { "username": username, "password": password };
        $.ajax({
            beforeSend: function () {
                WaitingBox('Đang đăng nhập vào hệ thống ...');
            },
            type: "POST",
            dataType: "text",
            url: "/Ajax/Acount.ashx",
            data: '{"type":"login","Userinfo":' + JSON.stringify(Userinfo) + '}',
            success: function (text) {
                $('.jAlert').closeAlert(true, function () { console.log('the alert is now closed'); });
                var arrstr = new Array();
                arrstr = text.split(',');
                switch (arrstr[0]) {
                    case "0":
                        {
                            window.location.href = '/Default.aspx';
                        }
                        break;
                    case "1":
                        {
                            AlertNoRedirect("Tên truy cập '<b>" + username + "</b>' không tồn tại, bạn hãy kiểm tra lại tài khoản");
                        }
                        break;
                    case "2":
                        AlertNoRedirect("Mật khẩu không đúng, bạn hãy kiểm tra lại!");
                        break;
                    case "-3":
                        AlertNoRedirect("Hệ thống đăng ký đang gặp sự cố, mong bạn vui lòng quay lại sau");
                        break;
                    case "10":
                        AlertNoRedirect("Tài khoản của bạn đã bị khóa!");
                        break;
                    default:
                        break;
                }

                $('#td_vilage').html(text);
            }
        });
}
function WaitingBox(text) {
    $.jAlert({
        'content': '<p class ="front-alert"><i class="fa fa-spinner fa-4x"></i>' + text + '</p>',
        'theme': 'default',
        'size': 'md',
        'closeBtn': false,
    });
    return false;
}
function Logout() {
    $.ajax({
        beforeSend: function () {
            WaitingBox('Đang thoát khỏi hệ thống ...');
        },
        type: "POST",
        dataType: "text",
        url: "/Ajax/Acount.ashx",
        data: '{"type":"logout"}',
        success: function (text) {
            $('.jAlert').closeAlert(true, function () { console.log('the alert is now closed'); });
            if (text == "1") {
                window.location.href = 'Default.aspx';
            }
        }
    });
}
function Load_District(city_id) {
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "UserInfo.aspx",
        data: "load_city_id=" + city_id,
        success: function (text) {
            $("[name='district']").html(text);
        }
    });
}
function Load_School(Vilage_id) {
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "UserInfo.aspx",
        data: "load_vilage_id=" + Vilage_id,
        success: function (text) {
            $("[name='school']").html(text);
        }
    });
}
function UpdateInfo() {
    var city = $("select[name='city']").val();
    var district = $("select[name='district']").val();
    var school = $("select[name='school']").val();
    var scity = $("select[name='city'] option:selected").text();
    var sdistrict = $("select[name='district'] option:selected").text();
    var sschool = $("select[name='school'] option:selected").text();
    var day = $('#ddl_day').val();
    var month = $('#ddl_month').val();
    var year = $('#ddl_year').val();
    if (city == 0 || district == 0 || school == 0) {
        AlertNoRedirect("Vui lòng chọn trường bạn đang học.");
        return;
    }
    if (day == "0" || month == "0" || year == "0") {
        AlertNoRedirect("Bạn cần nhập Ngày sinh");
        return;
    }
    if (!isDay(day, month, year)) {
        AlertNoRedirect("Bạn cần nhập đúng Ngày sinh");
        return;
    }
    UserInfo.fk_school_id = school;
    UserInfo.fk_province_id = city;
    UserInfo.fk_village_id = district;
    UserInfo.s_school_name = sschool;
    UserInfo.s_province_name = scity;
    UserInfo.s_village_name = sdistrict;
    UserInfo.d_bith = year + '/' + month + '/' + day;
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "UserInfo.aspx",
        data: "update_info=" + JSON.stringify(UserInfo),
        success: function (text) {
            if (text == 1) {
                AlertNoRedirect("Cập nhật Thông tin tài khoản thành công.");
                return;
            }
            else {
                AlertNoRedirect("Cập nhật thất bại.");
                return;
            }
        }
    });
}
function LoadDayDDL() {
    var Account = {
        BindOptionData: function (start, end, dvalue, dtext, atext, dd) {
            var sb = "";
            if (dd)
                sb = "<option value='" + dvalue + "'>" + dtext + "</option>";
            for (var i = start; i < end; i++) {
                sb += "<option value='" + i + "'>" + atext + i + "</option>";
            }
            return sb;
        },
        Binday: function (itemid) {
            var sb = Account.BindOptionData(1, 32, 0, "-- Ngày", "", true);
            $("select[name='" + itemid + "']").html(sb);

        },
        BindMonth: function (itemid) {
            var sb = Account.BindOptionData(1, 13, 0, "-- Tháng", "", true);
            $("select[name='" + itemid + "']").html(sb);
        },
        BindYear: function (itemid) {
            var cYear = new Date().getFullYear();
            var sb = Account.BindOptionData(cYear - 25, cYear - 3, 0, "-- Năm", "", true);
            $("select[name='" + itemid + "']").html(sb);
        }
    }
    Account.Binday('ddl_day');
    Account.BindMonth('ddl_month');
    Account.BindYear('ddl_year');
}
function LoadDayDDL1() {
    var Account = {
        BindOptionData: function (start, end, dvalue, dtext, atext, dd) {
            var sb = "";
            if (dd)
                sb = "<option value='" + dvalue + "'>" + dtext + "</option>";
            for (var i = start; i < end; i++) {
                sb += "<option value='" + i + "'>" + atext + i + "</option>";
            }
            return sb;
        },
        Binday: function (itemid) {
            var sb = Account.BindOptionData(1, 32, 0, "-- Ngày", "", true);
            $("select[name='" + itemid + "']").html(sb);

        },
        BindMonth: function (itemid) {
            var sb = Account.BindOptionData(1, 13, 0, "-- Tháng", "", true);
            $("select[name='" + itemid + "']").html(sb);
        },
        BindYear: function (itemid) {
            var cYear = new Date().getFullYear();
            var sb = Account.BindOptionData(2016, cYear+1, 0, "-- Năm", "", true);
            $("select[name='" + itemid + "']").html(sb);
        }
    }
    Account.Binday('ddl_day');
    Account.BindMonth('ddl_month');
    Account.BindYear('ddl_year');
}
window.onload = function () {
    if (window.location.href.indexOf('UserInfo') > 0) {
        LoadDayDDL();
        LoadCity();
        UserInfo_active();
    }
    if (window.location.href.indexOf('profile') > 0) {
        LoadDayDDL1();
        Profile_active();
        Gradebook(1);
    }
    if (window.location.href.indexOf('English.aspx') > 0) {
        English_active();
    }
    if (window.location.href.indexOf('Math.aspx') > 0) {
        English_active();
    }
    if (window.location.href.indexOf('Toan.aspx') > 0) {
        English_active();
    }
    if (window.location.href.indexOf('Play.aspx') > 0) {
        NextQuestion();
        $("[class='block-21']").find("li").find("a").html(localStorage['class']);
        $("[class='block-21']").find("li").find("a").attr("href", localStorage['url']);
        $("#nameunit").html(localStorage['unit']);
        TimeUp();
        $('body').disableTextSelect();
    }
    if (window.location.href.indexOf('Log.aspx') > 0) {
        LogHeader();
        localStorage['No'] = 0;
        LoadLog();
        $("#postback").attr("href", localStorage['logUrl']);
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                LoadLog();
            }
        });
    }
    if (window.location.href.indexOf('Register.aspx?type=loginG') > 0) {
        RegisterG();
    }
    $("#sortable").sortable();
    $("#sortable").disableSelection();
};
function isDay(dtDay, dtMonth, dtYear) {
    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}
function LoadCity() {
    var i = $("[name='hdCity']").val();
    $("[name='city']").val(i);
    var j = $("[name='hdBith']").val();
    if(j!="1/1/0001 12:00:00 AM")
    {
        var arr = j.split('/');
        var year = arr[2].split(' ');
        $("[name='ddl_day']").val(arr[1]);
        $("[name='ddl_month']").val(arr[0]);
        $("[name='ddl_year']").val(year[0]);
    }
    
}
function UserInfo_active() {
    group = $("[class='nav-dashboard']").find("li").find("a");
    i = 1;
    group.each(function () {
        if (window.location.href.indexOf('info') > 0&& i==1) {
            $(this).addClass("active");
            return false;
        }
        if (window.location.href.indexOf('vip') > 0 && i == 2) {
            $(this).addClass("active");
            return false;
        }
        if (window.location.href.indexOf('profile') > 0 && i == 3) {
            $(this).addClass("active");
            return false;
        }
        if (window.location.href.indexOf('repass') > 0 && i == 4) {
            $(this).addClass("active");
            return false;
        }
        i++;
    });
    
}
function English_active() {
    var url = window.location.href;
    var arr = url.split('=');
    group = $("[class='block-14']").find("ul").find("li").find("a");
    i = 1;
    group.removeClass("active");
    group.each(function () {
        if (arr[1] == i) {
            $(this).addClass("active");
        }
        i++;
    });
}
function LoadQuestion(iQuestion)
{
    $("#explanation").hide();
    $.ajax({
        beforeSend: function () {
            WaitingBox('Đang tải câu hỏi ...');
        },
        type: "POST",
        dataType: "text",
        url: "/Ajax/Play.ashx",
        data: '{"iQuestion":"'+iQuestion+'"}',
        success: function (text) {
            $('.jAlert').closeAlert(true);
            $("#exams").html(text);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, "step1"]);
        }
    });
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "step1"]);
}
function NextQuestion() {
    var text = $("[name='listID']").val();
    var arr = text.split(';');
    text = "";
    if (arr[0] == null||arr[0]=="") {
        //url = "/English.aspx?G=" + $("[name='grade']").val();
        url = localStorage['url'];
        AlertRedirect("Bạn đã hoàn thành xong chủ đề này", url);
        return;
    }
    else {
        LoadQuestion(arr[0]);
        localStorage['idQuestion'] = arr[0];
        for (i = 1; i < arr.length - 1; i++) {
            text += arr[i] + ";";
        }
        $("#submit").html("Nộp bài");
        $("[name='listID']").val(text);
    }
}
function setSession(e,url) {
    value = $(e).find("span").text();
    localStorage['class'] = $("h2[class='title color-01']").text();
    localStorage['unit'] = value;
    localStorage['url'] = window.location.href;
    window.location.href = url;
}
function TimeUp()
{
    sec = parseInt($("#seconds").text());
    min = parseInt($("#minutes").text());
    hour = parseInt($("#hours").text());
    sec = sec + 1;
    if (sec == 60)
    {
        min += 1;
        sec = 0;
    }
    if (min == 60)
    {
        hour += 1;
        min = 0;
    }
    if (sec < 10) { vsec = '0' + sec; } else { vsec = sec; }
    if (min < 10) { vmin = '0' + min; } else { vmin = min; }
    if (hour < 10) { vhour = '0' + hour; } else { vhour = hour; }
    $("#seconds").text(vsec);
    $("#minutes").text(vmin);
    $("#hours").text(vhour);
    setTimeout("TimeUp()", 1000);
}
jQuery.fn.disableTextSelect = function () {
    return this.each(function () {
        $(this).css({
            'MozUserSelect': 'none',
            'webkitUserSelect': 'none'
        }).attr('unselectable', 'on').bind('selectstart', function () {
            return false;
        });
    });
};
jQuery.fn.enableTextSelect = function () {
    return this.each(function () {
        $(this).css({
            'MozUserSelect': '',
            'webkitUserSelect': ''
        }).attr('unselectable', 'off').unbind('selectstart');
    });
};
function drawChart() {
    var chart = new CanvasJS.Chart("pie",
		{

		    title: {
		        text: "Tháng 3 Năm 2010",
		        fontSize: 30
		    },
		    animationEnabled: true,
		    axisX: {

		        gridColor: "Silver",
		        tickColor: "silver",
		        valueFormatString: "DD"

		    },
		    toolTip: {
		        shared: true
		    },
		    theme: "theme4",
		    axisY: {
		        gridColor: "Silver",
		        tickColor: "silver"
		    },
		    legend: {
		        verticalAlign: "center",
		        horizontalAlign: "right"
		    },
		    data: [
			{
			    type: "line",
			    showInLegend: true,
			    lineThickness: 2,
			    name: "Câu đã làm",
			    markerType: "square",
			    color: "#F08080",
			    dataPoints: [
				{ x: new Date(2010, 0, 1), y: 60 },
				{ x: new Date(2010, 0, 2), y: 70 },
				{ x: new Date(2010, 0, 3), y: 210 },
				{ x: new Date(2010, 0, 4), y: 65 },
				{ x: new Date(2010, 0, 5), y: 73 },
				{ x: new Date(2010, 0, 6), y: 0 },
				{ x: new Date(2010, 0, 7), y: 0 },
				{ x: new Date(2010, 0, 8), y: 85 },
				{ x: new Date(2010, 0, 9), y: 89 },
				{ x: new Date(2010, 0, 10), y: 43 },
				{ x: new Date(2010, 0, 11), y: 0 },
                { x: new Date(2010, 0, 11), y: 0 },
                { x: new Date(2010, 0, 12), y: 0 },
                { x: new Date(2010, 0, 13), y: 0 },
                { x: new Date(2010, 0, 14), y: 0 },
                { x: new Date(2010, 0, 15), y: 0 },
                { x: new Date(2010, 0, 16), y: 20 },
                { x: new Date(2010, 0, 17), y: 30 },
                { x: new Date(2010, 0, 18), y: 40 },
                { x: new Date(2010, 0, 19), y: 100 },
                { x: new Date(2010, 0, 20), y: 0 },
                { x: new Date(2010, 0, 21), y: 0 },
                { x: new Date(2010, 0, 22), y: 40 },
                { x: new Date(2010, 0, 23), y: 50 },
                { x: new Date(2010, 0, 24), y: 60 },
                { x: new Date(2010, 0, 25), y: 70 },
                { x: new Date(2010, 0, 26), y: 30 },
                { x: new Date(2010, 0, 27), y: 20 },
                { x: new Date(2010, 0, 28), y: 10 },
                { x: new Date(2010, 0, 29), y: 20 },

			    ]
			},
		    ],
		    legend: {
		        cursor: "pointer",
		        itemclick: function (e) {
		            if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		                e.dataSeries.visible = false;
		            }
		            else {
		                e.dataSeries.visible = true;
		            }
		            chart.render();
		        }
		    }
		});

    chart.render();
}
function Profile_active()
{
    var url = window.location.href;
    var arr = url.split('=');
    group = $("ul[class='nav-tab']").find("li").find("a");
    i = 1;
    group.removeClass("active");
    group.each(function () {
        if (arr[2] == i) {
            $(this).addClass("active");
        }
        i++;
    });
}
function Gradebook(start)
{
    var page = 10;
    var url = window.location.href;
    var arr = url.split('=');
    var category = arr[2];
    var page = { "start": start, "category": category };
    $.post("/Ajax/ProfileStudy.ashx"
        , JSON.stringify(page)
        , function (text) {
            $("#front-gradebook").html(text);
    });
}
function changpage(e) {
    $("div[class='paging']").find("a").removeClass("active");
    $(e).addClass("active");
    var i = parseInt($(e).text());
    i = (i - 1) * 10 + 1;
    Gradebook(i);
}
function nextpage() {
    var now = $("div[class='paging']").find("a[class='active']").parent();
    if (now.next().is("li:last-child")) {
        var node = $("div[class='paging']").find("li:first-child").next();
        changpage(node.find("a"));
        return false;
    }
    changpage(now.next().find("a"));
}
function prevpage() {
    var now = $("div[class='paging']").find("a[class='active']").parent();
    if (now.prev().is("li:first-child")) {
        var node = $("div[class='paging']").find("li:last-child").prev();
        changpage(node.find("a"));
        return false;
    }
    changpage(now.prev().find("a"));
}
function postLog(e) {
    localStorage['logNameU'] = $(e).text();
    group = $(e).parent().parent().find("div[class='cel']");
    localStorage['logStart'] = group.eq(6).text();
    localStorage['logLast'] = group.eq(7).text();
    localStorage['logPass'] = group.eq(2).text();
    localStorage['logUnPass'] = group.eq(3).text();
    localStorage['logGrade'] = group.eq(5).text();
    localStorage['logUrl'] = window.location.href;
}
function LogHeader() {
    $("#logNameU").html(localStorage['logNameU']);
    $("#logStart").html(localStorage['logStart']);
    $("#logLast").html(localStorage['logLast']);
    $("#logPass").html(localStorage['logPass']);
    $("#logUnPass").html(localStorage['logUnPass']);
    $("#logGrade").html(localStorage['logGrade']);
}
function LoadLog() {
    var x = parseInt(localStorage['No']);
    x++;
    localStorage['No'] = x;
    var value = $("input[id='ListId']").val();
    var arr = value.split('-');
    var id = arr[0];
    var json = { "LogId": id };
    if (id == "") { return;}
    $.post("/Ajax/ProfileStudy.ashx"
        , JSON.stringify(json)
        , function (data) {
            var arr = data.split('{##}');
            var i = parseInt(arr[1]);
            switch (i) {
                case 0:
                    var varHTML = '<div class="quizzes">'
                                + '<div class="head">'
                                + '<div class="order">' + arr[2] + '</div>'
                                + '<div class="order">câu số ' + x + '</div>'
                                + '<div class="wrong"><i class="fa fa-times"></i>SAI</div>'
                                + '</div>'
                                + '<div class="content" style="max-width:100%;overflow: auto;">'
                                + arr[0]
                                + '</div>'
                                + '</div>';
                    break;
                case 1:
                    var varHTML = '<div class="quizzes">'
                                + '<div class="head">'
                                + '<div class="order">' + arr[2] + '</div>'
                                + '<div class="order">câu số ' + x + '</div>'
                                + '<div class="right"><i class="fa fa-check"></i>ĐÚNG</div>'
                                + '</div>'
                                + '<div class="content" style="max-width:100%;overflow: auto;">'
                                + arr[0]
                                + '</div>'
                                + '</div>';
                    break;
            }
            $("#LogQ").append(varHTML);
        });
    value = "";
    for (i = 0; i < arr.length; i++) {
        if (i == 1) {
            value = arr[1];
        }
        else if(i>1)
            value += "-" + arr[i];
    }
    $("input[id='ListId']").val(value);
}
function loginG() {
    var j = { "type": "login" };
    $.post("LoginG.aspx"
        , JSON.stringify(j)
        , function (data) {
            alert(data);
        });
}
function RegisterG(){
    $("[name='email']").attr('readonly', true);
    $("#boxLoginG").hide();
}
function voice_speak(e) {
    var voice = $(e).next("span[class='font-voice']").text();
    responsiveVoice.speak(voice);
}
function voice_speakvn(e) {
    var voice = $(e).next("span[class='font-voice']").text();
    responsiveVoice.speak(voice, "Vietnamese Male");
}