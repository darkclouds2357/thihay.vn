﻿function shuffle(array) {
    var m = array.length, t, i;
    while (m) {
        i = Math.floor(Math.random() * m--);
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}
function Mark(i,ID)
{
    var answer;
    if (!valMar(i))
    {
        AlertNoRedirect("Bạn chưa hoàn thành bài thi.");
        return;
    }
    switch (i)
    {
        case 1:
            answer = "";
            ans1 = "";
            ans2 = "";
            group = $("tr[class*='ThRowA']");
            group.each(function () {
                ans1 += $(this).find("td:last").attr('name');
                var e = $(this).attr('class');
                if (e == 'ThRowA select') {
                    ans2 += '1';
                }
                else ans2 += '0';
            });
            answer = ans1 +';'+ ans2;
            break;
        case 2:
            answer = "";
            group = $("table[class='subAns']");
            group.each(function () {
                node = $(this).find("tr");
                ans1 = ""; ans2 = "";
                node.each(function () {
                    ans1 += $(this).find("td:last").attr('name');
                    var e = $(this).attr('class');
                    if (e == 'ThRowA select') {
                        ans2 += '1';
                    }
                    else ans2 += '0';
                });
                answer += ans1 + ";" + ans2 + "-";
            });
            break;
        case 3:
        case 4:
            answer = "";
            group = $("input[class='Thblank']");
            group.each(function () {
                var p = $(this).parent();
                //alert(p.prop("tagName"));
                if (p.prop("tagName") != 'annotation-xml')
                {
                    ans = $(this).val().trim();
                    answer += ans + "{div}";
                }
            });
            break;
        case 5:
            answer = "";
            group = $("select[class='ThSelect']");
            group.each(function () {
                answer += $(this).val()+";";
            });
            break;
        case 6:
            groupL = $("div[class*='LeftDD']");
            groupM = $("div[class*='MiddleDD']");
            groupR = $("div[class*='RightDD']");
            var valL =[], valM=[], valR=[];
            groupL.each(function () {
                valL.push($(this).attr('id'));
            });
            groupM.each(function () {
                valM.push($(this).attr('id'));
            });
            groupR.each(function () {
                valR.push($(this).attr('id'));
            });
            corr = true;
            i = 0;
            groupL.each(function () {
                var eq1 = $("div[id='"+valL[i]+"']").find("p").attr('name');
                var eq2 = $("div[id='"+valM[i]+"']").find("p").attr('name');
                if (eq1 != eq2) {
                    $("div[id='" + valM[i] + "']").css("border-color", "#fa2012");
                    eqc = getMathML($(".MiddleDD p[name='" + eq1 + "']"));
                    $("div[id='" + valR[i] + "']").html(eqc);
                    corr = false;
                }
                i++;
            });
            if (corr) randomEff(1);
            else randomEff(0);
            group = $("div[class*='RightDD']");
            group.each(function () {
                $(this).draggable('disable');
                $(this).awesomeCursor('none');
            });
            group = $("div[class*='MiddleDD']");
            group.each(function () {
                $(this).draggable('disable');
            });
            break;
        case 7:
            corr = true;
            varHTML = "<div id=\"DesOrder\">";
            group = $("div[class*='rowOrder']");
            i = 0;
            group.each(function () {
                //text = getMathML($(this));
                //$(this).html(text);
                var val = $(this).attr('name');
                var eq1 = $("div[name='" + i + "']").text();
                var eq2 = $(this).text();
                if (i != val && eq1 != eq2) {
                    corr = false;
                    fCorr = getMathML($("div[name='" + i + "']"));
                    varHTML += "<div class=\"rowOrder\">" + fCorr + "</div>";
                    $(this).addClass("OrderIncorrect");
                }
                else {
                    varHTML += "<div class=\"rowOrder\">" + getMathML($(this)) + "</div>";
                }
                i++;
            });
            varHTML += "</div>";
            //varHTML = varHTML.replace(/fa fa-arrows-v/g, "fa fa-thumb-tack");
            $("#ThOrder").after(varHTML);
            if (corr) randomEff(1);
            else randomEff(0);
            $("#ThOrder").sortable('disable');
            break;
        case 8:
            corr = true;
            LCorrect = $("a[class*='btnEn']").attr('name').split(';');
            //group = $("input[class*='blank8']");
            group = $("[class*='blank8']");
            i = 0;
            group.each(function () {
                answer = $(this).attr('name');
                if (LCorrect[i] != answer) {
                    corr = false;
                    temp = $(this).html();
                    Acorr = $("div[name='"+LCorrect[i]+"']").html();
                    span = '<span class="tt"><strong><i class="fa fa-check" style="color:blue" ></i></strong>&nbsp&nbsp' + Acorr + '</span>';
                    temp = '<u class="tooltip">&nbsp&nbsp' + temp + '&nbsp&nbsp' + span + '</u><i class="fa fa-times" style="color:red"></i>';
                    $(this).replaceWith(temp);
                }
                else {
                    temp = $(this).html();
                    temp = '<u>&nbsp&nbsp' + temp + '&nbsp&nbsp</u><i class="fa fa-check" style="color:blue"></i>';
                    $(this).replaceWith(temp);
                }
                i++;
            });
            if (corr) randomEff(1);
            else randomEff(0);
            StopType8();
            break;
        case 9:
            corr = true;
            mark = $("a[class*='btnEn']").attr('name');
            Lmark = mark.split(';');
            group = $("td[class*='Td-Drog-9']");
            i = 0;
            Eq2 = 0; value = "<tr>";
            group.each(function () {
                Eq1 = parseInt(Eq2);
                Eq2 = parseInt(Lmark[i]);
                group1 = $(this).find("p");
                group1.each(function () {
                    val = parseInt($(this).attr('name'));
                    if (!(Eq1 <= val && val  < Eq2)){
                        corr = false;
                        $(this).parent().addClass("incorrect");
                    }
                });
                value += "<td>";
                group2 = $("div[class*='Th-Row-9']");
                group2.each(function () {
                    val = parseInt($(this).find("p").attr('name'));
                    if (Eq1 <= val && val < Eq2) {
                        value += '<div class="Th-Row-9">'+$(this).html()+'</div>';
                    }
                });
                value += "</td>";
                i++;
            });
            value += "</tr>";
            $(".Th-Target-9").append(value);
            if (corr) randomEff(1);
            else randomEff(0);
            $("div[class*='Th-Row-9']").draggable({ disabled: true });
            break;
        case 10:
            corr = true;
            group = $("[class*='p-type-10']");
            i = 1;
            var mark = $("[name='mark10']").val();
            //alert(mark);
            group.each(function () {
                $(this).attr("onclick", "");
                if(mark.indexOf(i) >= 0)
                {
                    $(this).addClass("checked-10");
                    if ($(this).attr("class").indexOf("check-10") < 0) {
                        $(this).removeClass("checked-10");
                        $(this).addClass("corr-10");
                        corr = false;
                    }
                }
                if ($(this).attr("class").indexOf("check-10") >= 0)
                {
                    if(mark.indexOf(i) < 0)
                    {
                        $(this).addClass("uncorr-10");
                        corr = false;
                    }
                }
                i++;
            });
            if (corr) {
                randomEff(1);
            }
            else randomEff(0);
            break;
    }
    $.ajax({
        type: "POST",
        url: "/Ajax/Play.ashx",
        data: '{"ID":"' + ID + '","ANS":"' + answer + '"}',
        dataType: "text",
        success: function (text) {
            count = parseInt($("#count-180").text());
            count = count + 1;
            $("#count-180").html(count);
            $("#getmark").html(text);
            var i = localStorage['randomEff'];
            saveProfile(i);
            $("#dialog").dialog({
                dialogClass: "no-close",
                height: 127,
                width: 500,
                open: function (event, ui) {
                    setTimeout("$('#dialog').dialog('close')", 2200);
                    $("[class='overlay']").show();
                },
                close: function () {
                    $("[class='overlay']").hide();
                }
            });
            // MathJax.Hub.Queue(["Typeset", MathJax.Hub, "step1"]);

            $("[class='MathJax_Preview']").remove();
            $("span[id*='MathJax']").remove();
            var scr = $("script[type='math/mml']");
            scr.each(function () {
                $(this).replaceWith($(this).text());
            });
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, "step1"]);

            //scr.replaceWith(scr.text());
            if ($("[name='BoolCorrect']").val() == 1) {
                setTimeout(function () {
                    NextQuestion();
                }, 2200);
            }
        }
    });
}
function tick(e)
{
    if ($(e).hasClass("select"))
    {
        $(e).removeClass("select");
        $(e).find("input").removeClass("ticked");
        $(e).children("td:first").html("<i class=\"fa fa-times\"></i>");
    }
    else
    {
        $(e).addClass("select");
        $(e).find("input").addClass("ticked");
        $(e).children("td:first").html("<i class=\"fa fa-check\"></i>");
    }
        
}
function tick1(e) {
    var group = $(e).parent().find("tr");
    group.each(function () {
        $(this).removeClass("select");
        $(this).find("input").removeClass("ticked");
        $(this).children("td:first").html("<i class=\"fa fa-times\"></i>");
    });
    $(e).addClass("select");
    $(e).find("input").addClass("ticked");
    $(e).children("td:first").html("<i class=\"fa fa-check\"></i>");
}
function autolen(e) {
    var value = $(e).val();
    $(e).width((value.length+1)*8+3);
}
function ThResult(text,correct)
{
    text = $("[name='result3']").val();
    correct = $("[name='correct3']").val();
    group = $("input[class='Thblank']");
    i = 0;
    var nSplit = '{div}';
    var ListA = correct.split(nSplit);
    group.each(function () {
        var p = $(this).parent();
        if (p.prop("tagName") != 'annotation-xml')
        {
            val = text.charAt(i);
            if (val == '0') {
                span = '<span class="tt"><strong><i class="fa fa-check" style="color:blue" ></i></strong>&nbsp&nbsp' + ListA[i] + '</span>';
                temp = '<u class="tooltip">&nbsp&nbsp' + $(this).val() + '&nbsp&nbsp' + span + '</u><i class="fa fa-times" style="color:red"></i>';
                $(this).replaceWith(temp);
            }
            else {
                temp = '<u>&nbsp&nbsp' + $(this).val() + '&nbsp&nbsp</u><i class="fa fa-check" style="color:blue" ></i>';
                $(this).replaceWith(temp);
            }
            i++;
        }
        else {

        }
    });
}
function ThResult5()
{
    text = $("[name='result5']").val();
    correct = $("[name='correct5']").val();
    group = $("select[class='ThSelect']");
    i = 0;
    var nSplit = '{div}';
    var ListA = correct.split(nSplit);
    j = 0;
    group.each(function () {
        var val = text.charAt(i);
        if (val == '0')
        {
            temp = $(this).find('option:selected').text();
            span = '<span  class="tt"><strong><i class="fa fa-check" style="color:blue" ></i></strong>&nbsp&nbsp' + ListA[j] + '</span>';
            temp = '<u class="tooltip">&nbsp&nbsp' + temp + '&nbsp&nbsp'+span+'</u><i class="fa fa-times" style="color:red"></i>';
            $(this).replaceWith(temp);
            j++;
        }
        else
        {
            temp = $(this).find('option:selected').text();
            temp = '<u>&nbsp&nbsp' + temp + '&nbsp&nbsp</u><i class="fa fa-check" style="color:blue"></i>';
            $(this).replaceWith(temp);
        }
        i++;
    });
}
function ThResult1(mark)
{
    group = $("tr[class*='ThRowA']");
    i = 0;
    group.each(function () {
        var val = mark.charAt(i);
        var text = getMathML($(this).find("td"));
        var varHTML;
        if (val == '3') {
            varHTML = '<tr class="ThRowA none"><td class="checkA"><i class="fa fa-check"></i></td><td class="rowA">' + text + '</td></tr>';
        }
        if (val == '0') {
            varHTML = '<tr class="ThRowA ThRed"><td class="checkA"><i class="fa fa-times"></i></td><td class="rowA">' + text + '</td></tr>';
        }
        if (val == '1')
            varHTML = '<tr class="ThRowA select"><td class="checkA"><i class="fa fa-check"></i></td><td class="rowA">' + text + '</td></tr>';
        if (val == '2')
            varHTML = '<tr class="ThRowA none"><td class="checkA"><i class="fa fa-times"></i></td><td class="rowA">' + text + '</td></tr>';
        $(this).replaceWith(varHTML);
        i++;
    });
}
function valMar(i) {
    var flag = false;
    switch (i) {
        case 1:
            if ($("tr[class*='select']").length)
            {
                flag = true;
            }
            break;
        case 2:
            flag = true;
            group = $("table[class='subAns']");
            group.each(function () {
                rflag = false;
                if ($(this).find("tr[class*='select']").length) {
                    return true;
                }
                else
                {
                    flag = false;
                    return false;
                } 
            });
            break;
        case 3:
        case 4:
            flag = true;
            group = $("input[class='Thblank']");
            group.each(function () {
                var p = $(this).parent();
                if (p.prop("tagName") != 'annotation-xml')
                {
                    if ($(this).val().trim() == '') {
                        flag = false;
                        return false;
                    }
                }
            });
            break;
        case 5:
            flag = true;
            break;
        case 6:
            flag = true;
            group = $("div[class*='RightDD']");
            group.each(function () {
                if($(this).html()!="")
                {
                    flag = false;
                    return false;
                }
            });
            break;
        case 7:
        case 10:
            flag = true;
            break;
        case 8:
            flag = true;
            group = $("input[class*='blank8']");
            group.each(function () {
                if ($(this).val() == '') {
                    flag = false;
                    return false;
                }
            });
            break;
        case 9:
            flag = true;
            if ($(".Th-Source-9").find("div[class*='Th-Row-9']").length)
            {
                flag = false;
            }
            break;
    }
    return flag;
}
function randomEff(i)
{
    localStorage['randomEff'] = i;
    var Effect = "rubberBand, bounce, flash, shake, swing, tada, wobble, jello, bounceIn, bounceInDown, bounceInLeft, bounceInRight,"
    + "bounceInUp, fadeInLeft,"
    + "fadeInLeftBig,fadeInRight,fadeInRightBig,fadeInUp,fadeInUpBig,"
    + "flip,flipInX,flipInY,lightSpeedIn,rotateIn,rotateInDownLeft,rotateInDownRight,rotateInUpLeft,rotateInUpRight,"
    + "slideInUp,slideInDown,slideInLeft,zoomIn,zoomInDown,zoomInLeft,zoomInRight,zoomInUp,rollIn";
    var nSplit = ',';
    var ListE = Effect.split(nSplit);
    var eff = RandomList(ListE);
    $("#dialog").find("p").addClass(eff);
    if (i == 0)
    {
        var Label = "Sorry, incorrect...;";
        var Icon = '<i class="fa fa-tint"></i>;<i class="fa fa-frown-o"></i>;<i class="fa fa-thumbs-down"></i>;<i class="fa fa-wheelchair"></i>;<i class="fa fa-exclamation-triangle"></i>;<i class="fa fa-modx"></i>;<i class="fa fa-stop-circle"></i>;<i class=\"fa fa-times\"></i>;<i class="fa fa-bomb"></i>;<i class="fa fa-battery-empty"></i>;'
        + '<i class="fa fa-bolt"></i>';
        var List = Label.split(';');
        var val = RandomList(List);
        $("#dialog").find("p").html(val);
        List = Icon.split(';');
        val = RandomList(List);
        $("#icon").html(val);
        $("#submit").html("Tiếp tục làm bài");
        $("#submit").attr("onclick", "NextQuestion()");
        $("[name='BoolCorrect']").val(i);
        $("#explanation").show();
        value = $("[name='Remember']").val();
        $("#result").html(value);
        // cau nao sai lam  lai 
        listid = $("[name='listID']").val();
        listid = listid + localStorage['idQuestion']+';';
        $("[name='listID']").val(listid);
    }
    if (i == 1) {
        var Label = "Great Job !;Terrific !;Keep it up !;Good !!!;You got it!;Good work !;";
        var Icon = '<i class="fa fa-check-circle"></i>;<i class="fa fa-trophy"></i>;<i class="fa fa-check"></i>;<i class="fa fa-star"></i>;<i class="fa fa-star" style="color:#FFDC00"></i>;<i class="fa fa-gratipay"></i>;<i class="fa fa-smile-o"></i>;<i class="fa fa-commenting-o"></i>;'
        + '<i class="fa fa-hand-peace-o"></i>;<i class="fa fa-plus"></i>;<i class="fa fa-paper-plane"></i>;<i class="fa fa-space-shuttle"></i>;<i class="fa fa-cloud-upload"></i>;<i class="fa fa-flag-o"></i>;<i class="fa fa-graduation-cap"></i>;'
        + '<i class="fa fa-paper-plane-o"></i>;';
        var List = Label.split(';');
        var val = RandomList(List);
        $("#dialog").find("p").html(val);
        List = Icon.split(';');
        val = RandomList(List);
        $("#icon").html(val);
        $("[name='BoolCorrect']").val(i);
    }

}
function RandomList(x) {
    var count = x.length-1;
    var rd = Math.floor((Math.random() * count));
    return x[rd];
}
function StartDD() {
    var val;
    group = $("div[class*='RightDD']");
    group.each(function () {
        val = $(this).width();
        if ($(this).html() != "") {
            $(this).awesomeCursor('space-shuttle', { color: 'blue', size: 50, flip: 'horizontal' });
        }
        $(this).draggable({
            revert: true
        });
        $(this).droppable({
            drop: function (event, ui) {
                if ($(this).html() == "" && ui.draggable.html()!="") {
                    $(this).html(ui.draggable.html());
                    $(this).addClass("RExist");
                    $(this).removeClass("noneDD");
                    if (ui.draggable.attr('class').indexOf("MiddleDD") >= 0) {
                        ui.draggable.empty();
                        ui.draggable.removeClass("Exist");
                        ui.draggable.addClass("noneDD");
                    }
                    else{
                        ui.draggable.empty();
                        ui.draggable.removeClass("RExist");
                        ui.draggable.addClass("noneDD");
                    }
                    $(this).effect("pulsate", { times: 2 }, "slow");
                }
            }
        });
        $(this).sortable({
            helper: 'clone',
            appendTo: document.body
        });
    });
    group = $("div[class*='MiddleDD']");
    group.each(function () {
        if ($(this).html() != "")
        {
            $(this).awesomeCursor('arrows', { color: 'blue', size: 40, flip: 'horizontal' });
        }
        $(this).width(val);
        $(this).draggable({
            revert: true
        });
        $(this).droppable({
            drop: function (event, ui) {
                if ($(this).html() == "" && ui.draggable.html() != "") {
                    $(this).html(ui.draggable.html());
                    $(this).addClass("Exist");
                    $(this).removeClass("noneDD");
                    if (ui.draggable.attr('class').indexOf("MiddleDD") >= 0)
                    {
                        ui.draggable.empty();
                        ui.draggable.removeClass("Exist");
                        ui.draggable.addClass("noneDD");
                    }
                    else {
                        ui.draggable.empty();
                        ui.draggable.removeClass("RExist");
                        ui.draggable.addClass("noneDD");
                    }
                    $(this).effect("pulsate", { times: 2 }, "slow");
                }
            }

        });
        $(this).sortable({
            helper: 'clone',
            appendTo: document.body
        });
    });
}
function StartOrder() {
    $(function () {
        //$("#ThOrder").sortable({
        //    appendTo: document.body,
        //    //revert: true
        //});
        $('#ThOrder').sortable({
            helper: 'clone',
            appendTo: document.body,
            placeholder: "ui-state-highlight",
            forcePlaceholderSize: true,
            cursor: 'move'
        });
        $("#ThOrder").disableSelection();
    });
}
function StartType8() {
    group = $("div[class*='ThRow8']");
    group.each(function () {
        $(this).draggable({
            appendTo: "body",
            helper: "clone"
        });
    });
    group = $("[class*='blank8']");
    group.each(function () {
        $(this).droppable({
            drop: function (event, ui) {
                //if ($(this).html() == "") {
                    $(this).html(ui.draggable.html());
                    //$(this).text(ui.draggable.html());
                    $(this).effect("pulsate", { times: 2 }, "slow");
                    $(this).attr('name', ui.draggable.attr('name'));
                    //$("[class='MathJax_Preview']").remove();
                    //$("span[id*='MathJax']").remove();
                    //var scr = $("script[type='math/mml']");
                    //scr.each(function () {
                    //    $(this).replaceWith($(this).text());
                    //});
                    //MathJax.Hub.Queue(["Typeset", MathJax.Hub, "step1"]);
                //}
            }
        });
    });
}
function StopType8() {
    group = $("div[class*='ThRow8']");
    group.each(function () {
        $(this).draggable('disable');
    });
}
function StartType9() {
    count = $("td[class*='Td-Drog-9']").length;
    len = (100 / count) - 1;
    $("td[class*='Td-Drog-9']").css("width", len + "%");
    group = $("div[class*='Th-Row-9']");
    group.each(function () {
        $(this).draggable({
            helper: "clone",
        });
    });
    $("td[class*='Td-Drog-9']")
    .droppable({
            drop: function (event, ui) {
                if (ui.draggable.html() != "") {
                    val = '<div class="Th-Row-9">' + ui.draggable.html() + '</div>';
                    $(this).append(val);
                    ui.draggable.remove();
                    $("div[class*='Th-Row-9']").draggable({
                        helper: "clone",
                    });
                }
            }
    });
}
function StartType10() {
    $("[class*='p-type-10']").awesomeCursor('pencil', { color: 'blue', size: 50, flip: 'vertical' });
}
function StartMark(type,id) {
    $("#submit").attr('onclick', 'Mark(' + type + ',' + id + ')');
}
function play_10(e) {
    var att = $(e).attr("class");
    if (att.indexOf('check-10') > 0)
    {
        $(e).removeClass('check-10');
    }
    else
    {
        $(e).addClass('check-10');
    }
}
function saveProfile(i)
{
    var sessionValue = $("#hdnSession").data('value');
    if (sessionValue != '1') {
        return false;
    }
    var historyHTML = $("#exams").html();
    if (historyHTML == "") {
        return false;
    }
    var pass = parseInt(i);
    var historyinfo = { "pass": pass, "HTML": historyHTML, "sunit": localStorage['unit'], "idQuestion": localStorage['idQuestion'] };
    $.post("/Ajax/ProfileStudy.ashx", JSON.stringify(historyinfo));
}
function getMathML(e) {
    var temp = $(e);
    temp.find("[class='MathJax_Preview']").remove();
    temp.find("span[id*='MathJax']").remove();
    //var scr = $("script[type='math/mml']");
    //scr.replaceWith(scr.text());
    text = temp.text();
    return (text);
}